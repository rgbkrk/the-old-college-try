#!/bin/bash
#Wrote to test the lexer and parser in a system wide fashion

#Just to be sure...
javac tester/OutAndRunComparer.java

FILES=../TestCases/*.svg
for f in $FILES
do
  echo "*** Processing $f "
  java tester.OutAndRunComparer $f $f.out
  echo ""
done
