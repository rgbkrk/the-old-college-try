package tester;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PushbackReader;
import java.io.IOException;

import java.util.Iterator;

import lexer.Lexer;
import parser.Parser;
import lexer.Token;

public class OutAndRunComparer {

  private static String input_file_name = "";
  private static String expected_output_file_name = "";

  private static void process_args(String args[]) {
      if (args.length < 2)
          OutAndRunComparer.usage();
      input_file_name = args[0];
      expected_output_file_name = args[1];
  }

  /**
   * Prints a usage message and exits with error code 1.
   */
  private static void usage() {
      System.out.println("Usage: OutAndRunComparer input.svg expectedOutput.txt");
      System.exit(1);
  }
  public static void main(String[] args)
  {
    process_args(args);
    // Create PushbackReader/BufferedReader/FileReader to read in the
    // file
    PushbackReader infile = null;
    try{
        infile = new PushbackReader(new BufferedReader(
                new FileReader(input_file_name)), 1024);
    }
    catch(java.io.FileNotFoundException fnfe)
    {
      System.out.println("FATAL: Failed to open the input file.");
      System.exit(1);
    }
    BufferedReader expectedOutputFile = null;
    try {

    expectedOutputFile = new BufferedReader(new FileReader(expected_output_file_name));
    }
    catch(IOException ioe)
    {
      System.out.println("FATAL: Failed to open the expected output file.");
      System.exit(1);
    }

    // Create the lexer to provide tokens to the parser
    Lexer lexer = new Lexer(infile);
    
    JustReporter report = new JustReporter();

    Parser parser = new Parser(lexer, report, report);
    try{
        parser.parse_svg();
    }
    catch(Exception e)
    {
    	report.report_exception(e);
    }

    boolean passed = true;
    int failures = 0;

    Iterator<String> ittr = report.iterator();
    String line = "";	

    while(ittr.hasNext()){
      String output = ittr.next();
      try {
   	   line = expectedOutputFile.readLine();
      }
      catch(IOException ioe)
      {
        System.out.println("FATAL: Failed to read line from the expected output file.");
        System.exit(1);
      }
      
      if( output.trim().equals(line.trim()))
      {
        System.out.println("TEST PASSED for ");
        System.out.println(output);
        System.out.println("with expected output");
        System.out.println(line);
        System.out.println();
      }
      else{
        passed = false;
        failures++;
        System.out.println("TEST FAILED for ");
        System.out.println(output);
        System.out.println("with expected output");
        System.out.println(line);
        System.out.println();
      }
    }

    // Make sure we reached the end
    try{
      line = expectedOutputFile.readLine();
      if(line != null){
        passed = false; // Should not have reached this
        failures++;
      }
    }
    catch(IOException ioe)
    {
      //File should be finished
    }

    if(passed)
    {
      System.out.println("All tests PASSED!");
    }
    else
    {
      System.out.println(failures + " tests FAILED!");
    }

  }
}
