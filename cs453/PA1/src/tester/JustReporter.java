package tester;

import java.awt.Color;
import java.awt.Graphics;
import exceptions.InternalException;
import util.SVGColors.SVGColor;
import util.IDrawSVG;
import util.IReportSVG;
import java.util.LinkedList;
import java.lang.Iterable;
import java.util.Iterator;


/**
 * Drawer/Reporter class to be used while parsing an SVG file. This will
 * print/draw various statements based on the element being parsed.
 */
public class JustReporter implements IDrawSVG, IReportSVG, Iterable<String> {

    private LinkedList<String> report;

	
    public JustReporter() {
        report = new LinkedList<String>();
    }

    private static Color translate_color(SVGColor c) {

        switch (c) {
        case RED:
            return Color.RED;
        case GREEN:
            return Color.GREEN;
        case BLUE:
            return Color.BLUE;
        case BLACK:
            return Color.BLACK;
        case WHITE:
            return Color.WHITE;
        default:
            throw new InternalException("Unsupported SVGColor value.");
        }
    }

    /**
     * Draws info about a rectangle element.
     * 
     * @param x
     *            Horizontal location.
     * @param y
     *            Vertical location.
     * @param width
     *            Width of the rectangle.
     * @param height
     *            Height of the rectangle.
     * @param c
     *            Color of the rectangle.
     */
    public void draw_rect(int x, int y, int width, int height, SVGColor c) {
    }

    /**
     * Draws info about a circle element.
     * 
     * @param x
     *            Horizontal location.
     * @param y
     *            Vertical location.
     * @param radius
     *            Radius of the circle.
     * @param c
     *            Color of the circle.
     */
    public void draw_circle(int x, int y, int radius, SVGColor c) {
    }

    /**
     * Draws info about a line element.
     * 
     * @param x1
     *            Horizontal location of the first end-point.
     * @param y1
     *            Vertical location of the second end-point.
     * @param x2
     *            Horizontal location of the first end-point.
     * @param y2
     *            Vertical location of the second end-point.
     * @param c
     *            Color of the line.
     */
    public void draw_line(int x1, int y1, int x2, int y2, SVGColor c) {
    }

    /**
     * Reports info about a rectangle element.
     * 
     * @param x
     *            Horizontal location.
     * @param y
     *            Vertical location.
     * @param width
     *            Width of the rectangle.
     * @param height
     *            Height of the rectangle.
     * @param c
     *            Color of the rectangle.
     */
    public void report_rect(int x, int y, int width, int height, SVGColor c) {
        String reportText = "Rectangle: (" + x + "," + y + ") " + width + "x"
                + height + " color: " + c.toString();
	report.add(reportText);
    }

    /**
     * Reports info about a circle element.
     * 
     * @param x
     *            Horizontal location.
     * @param y
     *            Vertical location.
     * @param radius
     *            Radius of the circle.
     * @param c
     *            Color of the circle.
     */
    public void report_circle(int x, int y, int radius, SVGColor c) {
        String reportText = "Circle: (" + x + "," + y + ") radius:" + radius
                + " color: " + c.toString();
	report.add(reportText);
    }

    /**
     * Reports info about a line element.
     * 
     * @param x1
     *            Horizontal location of the first end-point.
     * @param y1
     *            Vertical location of the second end-point.
     * @param x2
     *            Horizontal location of the first end-point.
     * @param y2
     *            Vertical location of the second end-point.
     * @param c
     *            Color of the line.
     */
    public void report_line(int x1, int y1, int x2, int y2, SVGColor c) {
        String reportText = "Line: (" + x1 + "," + y1 + ") (" + x2 + "," + y2
                + ") color: " + c.toString();
	report.add(reportText);
    }
    
    public void report_exception(Exception e) {
    	report.add(e.getMessage());
    }

    public Iterator<String> iterator(){
    	return report.iterator();
    }
}

