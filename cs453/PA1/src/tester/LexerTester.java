package tester;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PushbackReader;

import lexer.Lexer;
import lexer.Token;

public class LexerTester {

  private static String input_file_name = "";
  private static String expected_output_file_name = "";

  private static void process_args(String args[]) {
      if (args.length < 2)
          LexerTester.usage();
      input_file_name = args[0];
      expected_output_file_name = args[1];
  }

  /**
   * Prints a usage message and exits with error code 1.
   */
  private static void usage() {
      System.out.println("Usage: LexerTester input.svg expectedOutput.txt");
      System.exit(1);
  }
  public static void main(String[] args)
  {
    process_args(args);
    // Attempt to parse the given file
        try {

            // Create PushbackReader/BufferedReader/FileReader to read in the
            // file
            PushbackReader infile = new PushbackReader(new BufferedReader(
                    new FileReader(input_file_name)), 1024);

            BufferedReader expectedOutputFile = new BufferedReader(new FileReader(expected_output_file_name));

            // Create the lexer to provide tokens to the parser
            Lexer lexer = new Lexer(infile);
            
            Token tok;
            
            boolean passed = true;
            int failures = 0;

            do{
              String output = "";
              try {
                tok = lexer.scan();
                output = tok.toString();
              } catch (exceptions.ParseException e) {
                output = e.toString();
                tok = null;
              }

              String line = expectedOutputFile.readLine();
              if( output.trim().equals(line.trim()))
              {
                System.out.println("TEST PASSED for ");
                System.out.println(output);
                System.out.println("with expected output");
                System.out.println(line);
                System.out.println();
              }
              else{
                passed = false;
                failures++;
                System.err.println("TEST FAILED for ");
                System.err.println(output);
                System.err.println("with expected output");
                System.err.println(line);
                System.err.println();
              }
            }while(tok != null && tok.tag != Token.Tag.EOF);

            if(passed)
            {
              System.out.println("All tests PASSED!");
            }
            else
            {
              System.out.println(failures + " tests FAILED!");
            }
        

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }


  }
}
