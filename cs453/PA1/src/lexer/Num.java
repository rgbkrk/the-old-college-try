package lexer;

/* Num is a Token that has an integer number value associated with it.
 * The Token.Tag name will always be NUM.
 */
public class Num extends Token {
    public final int value;

    public Num(int line, int pos, int v) {
        super(Token.Tag.NUM, line, pos);
        value = v;
    }

    @Override
    public String toString() {
        return "< " + tag.toString() + ", " + value
                + " >";
    }
}
