package lexer;
/**
 * @author dvnewman
 * @author kelley
 *
 * PA1 for CS453 Spring 2010
 *
 */

import java.util.*;
import java.io.*;

import exceptions.*;

/**
 * The Lexer implements a typical interface where scan() returns the next token
 * in the stream. The Lexer needs to be constructed by being passed a
 * PushbackReader. This Lexer keeps track of the current line and position in
 * the file while scanning.
 */
public class Lexer {

	/**
	 * An internal class to handle errors having to do with unexpected characters.
	 */
	private class CharacterException extends ParseException {

		public CharacterException(String actual, char expected, int line, int pos) {
			super ("Expected character '" +  expected
					+ "' and got '" + actual + "'.", line, pos);
		}

		public CharacterException(char actual, int line, int pos) {
			super ("Unexpected character '" + actual + "'.", line, pos);
		}

		public CharacterException(char actual, char expected, int line, int pos) {
			super ("Expected character '" +  expected
					+ "' and got '" + actual + "'.", line, pos);
		}
	}

	private PushbackReader filebuf;

	/**
	 * The Lexer constructor reserves all of the key words.
	 */
	public Lexer(PushbackReader fb) {
		filebuf = fb;
		// reserve all of the keywords
		this.reserve("x", Token.Tag.KW_X);
		this.reserve("y", Token.Tag.KW_Y);
		this.reserve("width", Token.Tag.KW_WIDTH);
		this.reserve("height", Token.Tag.KW_HEIGHT);
		this.reserve("fill", Token.Tag.KW_FILL);
		this.reserve("cx", Token.Tag.KW_CX);
		this.reserve("cy", Token.Tag.KW_CY);
		this.reserve("r",  Token.Tag.KW_R);
		this.reserve("x1", Token.Tag.KW_X1);
		this.reserve("y1", Token.Tag.KW_Y1);
		this.reserve("x2", Token.Tag.KW_X2);
		this.reserve("y2", Token.Tag.KW_Y2);
		this.reserve("stroke", Token.Tag.KW_STROKE);

		this.reserve("<svg",Token.Tag.SVG_START);
		this.reserve("</svg>",Token.Tag.SVG_END);
		this.reserve("<rect",Token.Tag.RECT_START);
		this.reserve("<circle",Token.Tag.CIRCLE_START);
		this.reserve("<line",Token.Tag.LINE_START);
		this.reserve("/>",Token.Tag.ELEM_END);
		this.reserve("=",Token.Tag.EQ);

	}

	// current line and position in the file
	public int line = 1;
	public int pos = 0;

	// Mapping between lexemes and token tag types for
	// handling reserved words. The map will be
	// initialized with tokens for all of the reserved words
	// including Color tokens.
	private Map<String, Token.Tag> words = new HashMap<String, Token.Tag>();

	private void reserve(String s, Token.Tag t) {
		words.put(s, t);
	}

	private Token.Tag lookup(String s) {
		return words.get(s);
	}

	private final StringBuffer lexeme = new StringBuffer();

	/**
	 * Returns the next character from the file buffer and maintains a lexeme
	 * that will be used when certain tokens are found. Also maintains the position
	 * of the character in the current line.
	 * 
	 * @throws IOException
	 */
	private int nextChar() throws IOException {
		int retval = filebuf.read();

		pos++;
		lexeme.append((char) retval);
		return retval;
	}

	/**
	 * Returns the next character from the file buffer after any
	 * white space and maintains a lexeme
	 * that will be used when certain tokens are found. Also maintains the line
	 * and position.
	 * 
	 * @throws IOException
	 */
	private int skipWhiteSpace() throws IOException {
		int retval = filebuf.read();

		// look for all end of line possibilities
		// linefeed = '\n', carriage return='\r', or carriage return followed by
		// linefeed '\r\n'
		// this takes care of Mac, Unix, and Windows
		while (retval == '\n' || retval == '\r' || retval == ' '
			|| retval == '\t') {
			if (retval == ' ' || retval == '\t') {
				pos++;
				retval = filebuf.read();

				// check for carriage return followed by linefeed
			} else if (retval == '\r') {
				line++;
				pos = 0; // below will be incrementing position for retval
				// char
				// will be returning the next character instead
				retval = filebuf.read();
				// carriage return followed by linefeed
				if (retval == '\n') {
					retval = filebuf.read();
				}
			} else {
				line++;
				pos = 0; // below will be incrementing position for retval
				// char
				// will be returning the next character instead
				retval = filebuf.read();
			}
		}

		pos++;
		lexeme.append((char) retval);
		return retval;
	}

	/**
	 * Throw an exception if the next character does not expect the only
	 * character the DFA can accept at this point.
	 */
	private void match(int c) throws ParseException, IOException {
		int next = nextChar();
		if (c != next) {
			throw new CharacterException((char) next, (char) c, line, pos);
		}
	}


	/**
	 * match a string of characters, throwing an exception if any does not match.
	 */
	private void matchString(String s) throws ParseException, IOException {
		for (int i = 0; i < s.length(); i++) {
			match(s.charAt(i));
		}
	}

	private void restartLexeme() {
		lexeme.setLength(0);
	}

	private String getLexeme() {
		return this.lexeme.toString();
	}

	/**
	 * scan a number, or throw an exception.
	 * @return a token representing the scanned number
	 * @throws IOException
	 */
	private Token scanNum() throws IOException{
		int c;
		do{
			c = nextChar();
		}while( Character.isDigit(c) );
		this.pushBack(c); // push back the one we don't need.
		int i = Integer.parseInt(this.getLexeme());
		return new Num(line, pos, i); 
	}

	/**
	 * scan a string representing a color, or throw an exception
	 * @return a token representing the scanned color
	 * @throws IOException
	 */
	private Token scanColor() throws IOException{
		int c;
		do{
			c = nextChar();
		}while( Character.isLetter(c) );
		this.pushBack(c); // push back the char we don't need.
		return new Color(line, pos, this.getLexeme()); 
	}

	/**
	 * scan a string representing a comment or throw an exception.
	 * if the string is successfully scanned, we scan the following
	 * token and return that effectively ignoring the contents of
	 * the comment.
	 * @return a token representing the item following the comment.
	 * @throws IOException
	 * @throws ParseException
	 */
	private Token scanComment() throws IOException, ParseException{
		int c = skipWhiteSpace();
		
		// look for initial two hyphens
		if (c == '-') {
			match('-'); //Match throws an exception if this one isn't found
		} else {
			// we didn't find even the first hyphen
			throw new CharacterException((char) c, '-', line, pos);
		}
		
		// look for final two hyphens
		int last_c;
		c = nextChar();
		do {
			last_c = c;
			c = nextChar();
		} while(!(c == '-' && last_c == '-') && c != -1); // ignore stuff until you see consecutive dashes
		if(c == -1) // unexpected EOF!
			throw new CharacterException((char)0xffff, '-', this.line, this.pos+1);
		c = skipWhiteSpace();
		if (c != '>') {
			throw new CharacterException((char) c, '>', line, pos);
		}
		
		return scan(); // we ignore a comment by scanning for the next token and returning that.
	}
	
	private Token scanQuotedString() throws IOException, ParseException{
			this.restartLexeme();
			int c = this.nextChar();
			int currPos = this.pos;
			Token tok;
			try {
				if(Character.isLetter(c))
				{
					tok = scanColor();
				}
				else if(Character.isDigit(c))
				{
					tok = scanNum();
				}
				else
				{
					//throw new CharacterException((char)c, this.line, this.pos);
					throw new ParseException("Expect only numbers or colors in quotes.", this.line, currPos);
				}
			}
			catch (CharacterException ce)
			{
				throw ce;
			}
			catch (InternalException i) {
				//	throw new CharacterException((char)c, this.line, this.pos);
				throw new ParseException("Unexpected word '" + this.getLexeme() + "'.",this.line, currPos);
				//throw new ParseException("Expect only numbers or colors in quotes.", this.line, currPos);
			}
			match('"');
			return tok;
	}
	
	
	public Token scanSVGElement() throws IOException, ParseException{
		//Here is the rough one.
		//For now, we'll look at the first character then decide
		int currPos = this.pos;
		int c = nextChar();
		if (c == 'r') {
			matchString("ect");
			return new Token(Token.Tag.RECT_START, this.line, currPos);
		} else if (c == 'c') {
			matchString("ircle");
			return new Token(Token.Tag.CIRCLE_START, this.line, currPos);	
		} else if (c == 'l') {
			matchString("ine");
			return new Token(Token.Tag.LINE_START, this.line, currPos);	
		} else if (c == 's') {
			matchString("vg");
			do {
				c = nextChar();
			} while(c != '>' && c != -1); // ignore stuff until you see a > or the buffer is at the end
			if(c == -1) // Unexpected EOF!
				throw new CharacterException((char)0xffff, '>', line, pos+1);
			return new Token(Token.Tag.SVG_START, this.line, currPos);	
		} else if (c == '/') {
			matchString("svg>");
			return new Token(Token.Tag.SVG_END, this.line, currPos);
		} else if (c == '!') {
			return scanComment();
		}
		else {
			throw new ParseException("Unexpected word '" + this.getLexeme() + "'.", line, pos);
		}

	}
	
	private Token scanWord() throws IOException, ParseException{
		int currPos = this.pos;
		int c;
		do {
			c = nextChar();
		} while(Character.isLetterOrDigit(c));

		// Put back the character that wasn't needed
		this.pushBack(c);

		Token.Tag t = this.lookup(this.getLexeme());
		if(t == null) {
			//lexeme not in our table
			//For SVG, we don't add the new word to the table,
			//as we would while doing lexical analysis of 
			// a programming language
			throw new ParseException("Unexpected word '" +
					this.getLexeme() + "'.",this.line, currPos);
		}
		return new Word(this.lookup(this.getLexeme()), this.line, currPos, this.getLexeme());
	}
	

	private void pushBack(int c) throws IOException{
		filebuf.unread(c);
		lexeme.setLength(lexeme.length()-1);
		pos--; // added this to correct pos when a pushback has occurred.
	}

	/**
	 * Returns the next MiniSVG token in the file buffer sent to the Lexer upon
	 * construction.
	 */
	public Token scan() throws IOException, ParseException, InternalException {
		// Most changes made here, per Dr. Strout's instructions.
		this.restartLexeme();
		int c = skipWhiteSpace();
		if(Character.isLetter(c)){
			return scanWord();
		}
		else if(c == '<'){
			return scanSVGElement();
		}
		else if(c == '/'){
			match('>');
			return new Token(Token.Tag.ELEM_END, this.line, this.pos - 1);
		}
		else if(c == '='){
			return new Token(Token.Tag.EQ, this.line, this.pos);
		}
		else if(c == '"'){
			return scanQuotedString();
		}
		else if (c == -1) {
			//Check for actual EOF
			return new Token(Token.Tag.EOF,this.line,this.pos);
		}
		else {
			throw new CharacterException((char)c, this.line, this.pos);
		}
	}
}
