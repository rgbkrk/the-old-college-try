/*
 * A Token consists of a token symbol, which is kept in the public tag field.
 * Tokens that require extra information, such as the lexeme for the token, 
 * should subclass from Token.  This design is almost the same as in 
 * Chapter 2 in the purple dragon book.
 */

package lexer;

public class Token {
    public enum Tag {
        SVG_START, SVG_END, RECT_START, CIRCLE_START, LINE_START, TEXT_START, ELEM_END, TEXT_END, COMMENT, EQ, NUM, COLOR, TEXT_STRING, KW_X, KW_Y, KW_WIDTH, KW_HEIGHT, KW_FILL, KW_CX, KW_CY, KW_R, KW_X1, KW_Y1, KW_X2, KW_Y2, KW_STROKE, EOF
    }

    // the tag is the terminal symbol
    public final Tag tag;
    // line and pos indicate the location in the file for the first char in
    // token
    public final int line;
    public final int pos;

    public Token(Tag t, int l, int p) {
        tag = t;
        line = l;
        pos = p;
    }

    @Override
    public String toString() {
        return "< " + tag.toString() + " >";
    }

}
