package lexer;

/* 
 * Word is a Token with a lexeme/string associated with it.
 * It is used to represent keywords and identifiers.
 */
public class Word extends Token {
    public final String lexeme;

    public Word(Tag t, int line, int pos, String s) {
        super(t, line, pos);
        lexeme = new String(s);
    }

    @Override
    public String toString() {
        return "< " + tag.toString() + ", " + lexeme
                + " >";
    }

}
