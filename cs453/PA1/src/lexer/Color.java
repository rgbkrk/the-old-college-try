package lexer;

import util.SVGColors.SVGColor;
import exceptions.*;

/* 
 * The Color token has a field that indicates which specific color
 * the token represents.  This was done instead of making the colors
 * separate keywords, so that the parser will not have to be modified
 * if a new color is added.  Extends Word so that all colors can be put
 * into a table with the rest of the keywords.
 */
public class Color extends Word {

    public final SVGColor value;

    public Color(int line, int pos, String color) throws InternalException {
        super(Token.Tag.COLOR, line, pos, color);
        if (color.equals("blue")) {
            value = SVGColor.BLUE;
        } else if (color.equals("red")) {
            value = SVGColor.RED;
        } else if (color.equals("green")) {
            value = SVGColor.GREEN;
        } else if (color.equals("black")) {
            value = SVGColor.BLACK;
        } else if (color.equals("white")) {
            value = SVGColor.WHITE;
        } else {
            throw new InternalException("Unknown color");
        }

    }

}
