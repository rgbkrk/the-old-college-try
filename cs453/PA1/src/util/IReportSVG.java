package util;

import util.SVGColors.SVGColor;

public interface IReportSVG {

    public void report_rect(int x, int y, int width, int height, SVGColor c);

    public void report_circle(int x, int y, int radius, SVGColor c);

    public void report_line(int x1, int y1, int x2, int y2, SVGColor c);
}
