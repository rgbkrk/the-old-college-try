package util;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 * Canvas subclass that will do the drawing in our JFrame
 */
public class MiniSVGCanvas extends Canvas {
    private static final long serialVersionUID = -134850353730173207L;

    /**
     * Image this canvas will draw
     */
    private BufferedImage m_background;

    /**
     * Single constructor takes an image that it will draw
     */
    public MiniSVGCanvas(BufferedImage background) {
        this.m_background = background;
    }

    /**
     * Draw the image
     */
    @Override
    public void paint(Graphics g) {
        super.paint(g);
        this.getGraphics().drawImage(this.m_background, 0, 0, this);
    }
}