package util;

import java.awt.Color;
import java.awt.Graphics;
import exceptions.InternalException;
import util.SVGColors.SVGColor;

/**
 * Drawer/Reporter class to be used while parsing an SVG file. This will
 * print/draw various statements based on the element being parsed.
 */
public class SVGDrawReport implements IDrawSVG, IReportSVG {

    /**
     * @param g
     *            The graphics object to draw to.
     */
    public SVGDrawReport(Graphics g) {
        this.m_g = g;
    }

    private static Color translate_color(SVGColor c) {

        switch (c) {
        case RED:
            return Color.RED;
        case GREEN:
            return Color.GREEN;
        case BLUE:
            return Color.BLUE;
        case BLACK:
            return Color.BLACK;
        case WHITE:
            return Color.WHITE;
        default:
            throw new InternalException("Unsupported SVGColor value.");
        }
    }

    private Graphics m_g;

    /**
     * Draws info about a rectangle element.
     * 
     * @param x
     *            Horizontal location.
     * @param y
     *            Vertical location.
     * @param width
     *            Width of the rectangle.
     * @param height
     *            Height of the rectangle.
     * @param c
     *            Color of the rectangle.
     */
    public void draw_rect(int x, int y, int width, int height, SVGColor c) {
        this.m_g.setColor(SVGDrawReport.translate_color(c));
        this.m_g.fillRect(x, y, width, height);
    }

    /**
     * Draws info about a circle element.
     * 
     * @param x
     *            Horizontal location.
     * @param y
     *            Vertical location.
     * @param radius
     *            Radius of the circle.
     * @param c
     *            Color of the circle.
     */
    public void draw_circle(int x, int y, int radius, SVGColor c) {
        this.m_g.setColor(SVGDrawReport.translate_color(c));
        this.m_g.fillOval(x, y, radius, radius);
    }

    /**
     * Draws info about a line element.
     * 
     * @param x1
     *            Horizontal location of the first end-point.
     * @param y1
     *            Vertical location of the second end-point.
     * @param x2
     *            Horizontal location of the first end-point.
     * @param y2
     *            Vertical location of the second end-point.
     * @param c
     *            Color of the line.
     */
    public void draw_line(int x1, int y1, int x2, int y2, SVGColor c) {
        this.m_g.setColor(SVGDrawReport.translate_color(c));
        this.m_g.drawLine(x1, y1, x2, y2);
    }

    /**
     * Reports info about a rectangle element.
     * 
     * @param x
     *            Horizontal location.
     * @param y
     *            Vertical location.
     * @param width
     *            Width of the rectangle.
     * @param height
     *            Height of the rectangle.
     * @param c
     *            Color of the rectangle.
     */
    public void report_rect(int x, int y, int width, int height, SVGColor c) {
        System.out.println("Rectangle: (" + x + "," + y + ") " + width + "x"
                + height + " color: " + c.toString());
    }

    /**
     * Reports info about a circle element.
     * 
     * @param x
     *            Horizontal location.
     * @param y
     *            Vertical location.
     * @param radius
     *            Radius of the circle.
     * @param c
     *            Color of the circle.
     */
    public void report_circle(int x, int y, int radius, SVGColor c) {
        System.out.println("Circle: (" + x + "," + y + ") radius:" + radius
                + " color: " + c.toString());
    }

    /**
     * Reports info about a line element.
     * 
     * @param x1
     *            Horizontal location of the first end-point.
     * @param y1
     *            Vertical location of the second end-point.
     * @param x2
     *            Horizontal location of the first end-point.
     * @param y2
     *            Vertical location of the second end-point.
     * @param c
     *            Color of the line.
     */
    public void report_line(int x1, int y1, int x2, int y2, SVGColor c) {
        System.out.println("Line: (" + x1 + "," + y1 + ") (" + x2 + "," + y2
                + ") color: " + c.toString());
    }
}