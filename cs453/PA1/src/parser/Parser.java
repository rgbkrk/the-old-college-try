package parser;

import java.io.IOException;

import util.IDrawSVG;
import util.IReportSVG;

import exceptions.InternalException;
import exceptions.ParseException;
import lexer.Color;
import lexer.Lexer;
import lexer.Num;
import lexer.Token;

/**
 * A top-down, predictive parser.
 */
public class Parser {

    /**
     * Parser constructor taking a lexer to use to obtain a stream of tokens.
     * 
     * @param lexer
     *            The lexer to use.
     */
    public Parser(Lexer lexer, IDrawSVG drawer, IReportSVG reporter) {
        this.m_lexer = lexer;
        this.m_drawer = drawer;
        this.m_reporter = reporter;
    }

    private Lexer m_lexer;
    private IDrawSVG m_drawer;
    private IReportSVG m_reporter;

    // The next token from the scanner.
    private Token m_lookahead;

    /**
     * If the current lookahead token has a token type equal to the one given,
     * then the lexer is called to set the lookahead to the next token.
     * Otherwise, an exception is thrown. The lookahead token just matched is
     * returned.
     */
    private Token match(Token.Tag tag) throws IOException, ParseException {
        Token retval;
        if (this.m_lookahead.tag != tag) {
            throw new ParseException("Expected token " +
                    tag + " got "
                    + this.m_lookahead.toString() + ".", this.m_lookahead.line,
                    this.m_lookahead.pos);
        } else {
            retval = this.m_lookahead;
            this.m_lookahead = this.m_lexer.scan();
        }
        return retval;
    }

    private void svg() throws IOException, ParseException {
      match(Token.Tag.SVG_START);
      elem_list();
      match(Token.Tag.SVG_END);
    }

    private void elem_list() throws IOException, ParseException {
      if(this.m_lookahead.tag == Token.Tag.SVG_END)
        return;
      elem();
      elem_list();
    }

    private void elem() throws IOException, ParseException {
      switch(this.m_lookahead.tag){
        case RECT_START:
          rect();
          break;
        case CIRCLE_START:
          circle();
          break;
        case LINE_START:
          line();
          break;
      }
    }

    private void rect() throws IOException, ParseException {

      match(Token.Tag.RECT_START);

      match(Token.Tag.KW_X);
      match(Token.Tag.EQ);
      Num xNum = (Num) match(Token.Tag.NUM);

      match(Token.Tag.KW_Y);
      match(Token.Tag.EQ);
      Num yNum = (Num) match(Token.Tag.NUM);

      match(Token.Tag.KW_WIDTH);
      match(Token.Tag.EQ);
      Num widthNum = (Num) match(Token.Tag.NUM);

      match(Token.Tag.KW_HEIGHT);
      match(Token.Tag.EQ );
      Num heightNum = (Num) match(Token.Tag.NUM);

      match(Token.Tag.KW_FILL);
      match(Token.Tag.EQ);
      Color fillColor = (Color) match(Token.Tag.COLOR);

      match(Token.Tag.ELEM_END);
      
      this.m_drawer.draw_rect(xNum.value, yNum.value, widthNum.value, heightNum.value, fillColor.value);
      this.m_reporter.report_rect(xNum.value, yNum.value, widthNum.value, heightNum.value, fillColor.value);

    }

    private void circle() throws IOException, ParseException {
      match(Token.Tag.CIRCLE_START);
      
      match(Token.Tag.KW_CX);
      match(Token.Tag.EQ);
      Num xNum = (Num) match(Token.Tag.NUM);

      match(Token.Tag.KW_CY);
      match(Token.Tag.EQ);
      Num yNum = (Num) match(Token.Tag.NUM);
      
      match(Token.Tag.KW_R);
      match(Token.Tag.EQ);
      Num radius = (Num) match(Token.Tag.NUM);
      
      match(Token.Tag.KW_FILL);
      match(Token.Tag.EQ);
      Color fill = (Color) match(Token.Tag.COLOR);
      
      match(Token.Tag.ELEM_END);

      this.m_drawer.draw_circle(xNum.value - radius.value/2, yNum.value - radius.value/2, radius.value, fill.value);
      this.m_reporter.report_circle(xNum.value, yNum.value, radius.value, fill.value);

    }

    private void line() throws IOException, ParseException {
      match(Token.Tag.LINE_START);
      
      match(Token.Tag.KW_X1 );
      match(Token.Tag.EQ);
      Num x1 = (Num) match(Token.Tag.NUM);
      
      match(Token.Tag.KW_Y1 );
      match(Token.Tag.EQ);
      Num y1 = (Num) match(Token.Tag.NUM );
      
      match(Token.Tag.KW_X2);
      match(Token.Tag.EQ);
      Num x2 = (Num) match(Token.Tag.NUM);
      
      match(Token.Tag.KW_Y2);
      match(Token.Tag.EQ);
      Num y2 = (Num) match(Token.Tag.NUM );

      match(Token.Tag.KW_STROKE);
      match(Token.Tag.EQ);
      Color stroke = (Color) match(Token.Tag.COLOR);

      match(Token.Tag.ELEM_END);
      
      this.m_drawer.draw_line(x1.value, y1.value, x2.value, y2.value, stroke.value);
      this.m_reporter.report_line(x1.value, y1.value, x2.value, y2.value, stroke.value);
    }

    /**
     * Routine that starts the parsing and rendering of SVG.
     * Should use the Lexer.scan routine to get the next token.
     */
    public void parse_svg() throws IOException, ParseException {
        //Initialize our lookahead
        this.m_lookahead = this.m_lexer.scan();
        svg();
        //Check to see that file is a proper file
        Token tok = match(Token.Tag.EOF);


        //Old test:
        //Token tok;
        //do{
        //  tok = this.m_lexer.scan();
        //  System.out.println(tok);
        //}while(tok.tag != Token.Tag.EOF);
    }

}
