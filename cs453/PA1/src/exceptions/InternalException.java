package exceptions;

/*
 * A ParseException includes an error message with the line and position of
 * error.
 */

public class InternalException extends RuntimeException {

    private static final long serialVersionUID = -1922819070348317175L;

    public InternalException(String msg) {
        super(msg);
    }
}
