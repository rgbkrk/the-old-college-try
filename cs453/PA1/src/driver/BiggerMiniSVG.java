package driver;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PushbackReader;
import javax.swing.JFrame;
import parser.Parser;
import util.MiniSVGCanvas;
import util.SVGDrawReport;
import lexer.Lexer;

/**
 * usage: java BiggerMiniSVG infile.SVG [--batch]
 * 
 * 1/6/08 MMS and AL
 */
public class BiggerMiniSVG extends JFrame {
    private static final long serialVersionUID = -8595369681863723650L;

    /* Hardcoded width and height for the image/window that may be shown. */
    private static final int image_width = 1000;
    private static final int image_height = 1000;
    private static final String batch_flag = "--batch";
    private static String file_name = "";
    private static boolean show_window = true;

    /**
     * Constructor taking an image to draw in it's window
     */
    public BiggerMiniSVG(BufferedImage background) {
        super();
        /* Add a new canvas that will draw the given image */
        this.add(new MiniSVGCanvas(background));
    }

    /**
     * BiggerMiniSVG entry point.
     * 
     * @param args
     *            Commandline arguments.
     */
    public static void main(String args[]) {

        /* Process the arguments given to the program */
        BiggerMiniSVG.process_args(args);

        /*
         * Create a BufferedImage. This will be the in-memory buffer we draw to
         * while parsing the SVG file
         */
        BufferedImage bg = new BufferedImage(BiggerMiniSVG.image_width,
                BiggerMiniSVG.image_height, BufferedImage.TYPE_INT_RGB);

        /* Draw to the image */
        BiggerMiniSVG.render(bg);

        /* Now create a window that will display the image if we need to */
        if (BiggerMiniSVG.show_window) {
            BiggerMiniSVG shell = new BiggerMiniSVG(bg);
            shell.setSize(BiggerMiniSVG.image_width, BiggerMiniSVG.image_height);
            shell.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            shell.setVisible(true);
        }
    }

    /**
     * Processes the given arguments. If errors are encountered, the program
     * exits with error code 1. If all goes well, {@link BiggerMiniSVG}.file_name is
     * set to the file name to process and {@link BiggerMiniSVG}.show_window is set
     * to true if a window should be shown and false if one should not be shown.
     * 
     * @param args
     *            Arguments to process
     */
    private static void process_args(String args[]) {
        if (args.length == 0 || args.length > 2)
            BiggerMiniSVG.usage();
        BiggerMiniSVG.file_name = args[0];
        if (2 == args.length) {
            if (!BiggerMiniSVG.batch_flag.equals(args[1]))
                BiggerMiniSVG.usage();
            BiggerMiniSVG.show_window = false;
        }
    }

    /**
     * Prints a usage message and exits with error code 1.
     */
    private static void usage() {
        System.out.println("Usage: BiggerMiniSVGShell SVGFile.svg ["
                + BiggerMiniSVG.batch_flag + "]");
        System.exit(1);
    }

    /**
     * Draws a sample set of elements to the given image object.
     * 
     * @param image
     *            The image to draw to.
     */
    private static void render(BufferedImage image) {

        //First fill the image with white
        image.getGraphics().setColor(Color.WHITE);
        image.getGraphics().fillRect(0, 0, BiggerMiniSVG.image_width,
                BiggerMiniSVG.image_height);

        // Attempt to parse the given file
        try {

            // Create PushbackReader/BufferedReader/FileReader to read in the
            // file
            PushbackReader infile = new PushbackReader(new BufferedReader(
                    new FileReader(BiggerMiniSVG.file_name)), 1024);

            // Create the lexer to provide tokens to the parser
            Lexer lexer = new Lexer(infile);

            // Create the Drawer/Reporter object that the parser uses
            SVGDrawReport drawreport = new SVGDrawReport(image.getGraphics());

            // Create the parser to obtain tokens from the lexer and do the
            // drawing.
            // This is a Predictive Parser that interprets the SVG file
            // When match rules in grammar, render SVG element to screen.
            Parser parser = new Parser(lexer, drawreport, drawreport);
            parser.parse_svg();

        } catch (exceptions.ParseException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
    }
}
