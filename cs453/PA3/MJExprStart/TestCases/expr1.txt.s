	.text
main:

	# IntegerExp
	li $t0, 4
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# IntegerExp
	li $t0, 5
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# IntegerExp
	li $t0, 6
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# IntegerExp
	li $t0, 2
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# PlusExp
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	add $t0, $t0, $t1
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# MulExp
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	mul $t0, $t0, $t1
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# IntegerExp
	li $t0, 2
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# MulExp
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	mul $t0, $t0, $t1
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# PlusExp
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	add $t0, $t0, $t1
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	jal _printint #Print out the last result
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	li $v0, 10
	syscall

## __printint.s
    .text

_printint:

    addi $sp, $sp, -4	# push return addr
    sw   $ra, 0($sp)

    addi $sp, $sp, -4   # push frame pointer
    sw   $fp, 0($sp)

    addi $fp, $sp, -4	# set up frame pointer

    lw	$a0, 12($fp)	# load the int
			# will it work with just $sp?

    li	$v0, 1		# print the int
    syscall

    la $a0, lf
    li $v0, 4
    syscall

    lw  $fp, 0($sp) 	# pop frame pointer
    addi $sp, $sp, 4

    lw  $ra, 0($sp)	# pop return addr
    addi $sp, $sp, 4

    jr   $ra		# return to caller


.data:
  lf: .asciiz "\n"

