#!/bin/bash


#We must retain sanity
rm TestCases/*.txt.*
make clean

#Ahhhhh
make

#Wonderful

FILES=./TestCases/*.txt
for f in $FILES
do
  echo "Testing $f"


  #### One-pass ####
  java -jar OracleEval.jar --one-pass-eval $f > $f.eout
  java -jar ExprEval.jar --one-pass-eval $f > $f.onepass
  diff $f.onepass $f.eout

  #### Two-pass-eval ####
  echo "Two-pass-eval test $f"
  java -jar OracleEval.jar --two-pass-eval $f > $f.eout.out
  mv $f.ast.dot $f.eout.ast.dot

  java -jar ExprEval.jar --two-pass-eval $f > $f.twopass.out

  diff $f.twopass.out $f.eout.out
  diff $f.ast.dot $f.eout.ast.dot

  #### Two-pass MIPS testing ####
  echo "Two-pass MIPS test $f"
  java -jar OracleEval.jar --two-pass-mips $f > /dev/null
  java -jar Mars.jar nc $f.s > $f.s.eout

  java -jar ExprEval.jar --two-pass-mips $f > /dev/null #Creates $f.s
  java -jar Mars.jar nc $f.s > $f.s.out

  diff -B $f.s.out $f.s.eout #-b ignores extra blank lines

done

