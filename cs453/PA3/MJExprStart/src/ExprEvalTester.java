import java.io.IOException;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;

public class ExprEvalTester extends AbstractCompilerTester {

  public ExprEvalTester(String input_file)
  {
    super(input_file);
  }

  /**
   * process conducts specific testing
   * @return The file name associated with the final output
   */
  public String process()
  {

    String args[] = new String[2];
    args[0] = "--two-pass-mips";
    args[1] = input_file;

    // Run ExprEvalDriver with input_file --> creates input_file.s
    try{
      Process p = Runtime.getRuntime().exec("java -jar ../ExprEval.jar " + args[0] + " " + args[1]);
      BufferedInputStream streaming = new BufferedInputStream(p.getInputStream());
      BufferedInputStream errStream = new BufferedInputStream(p.getErrorStream());
      p.waitFor();
      while(streaming.available() > 0)
      {
        System.out.print((char)streaming.read());
      }

      while(errStream.available() > 0)
      {
        System.err.print((char) errStream.read());
      }

    }
    catch(IOException ioe)
    {
      System.out.println("Aw crap.");
      System.out.println(ioe);
    }
    catch(InterruptedException ie)
    {
      System.out.println("Aw crap.");
      System.out.println(ie);
    }



    // Run Mars.jar with input_file, pipe output to input_file.marsout
    String marsargs[] = new String[2];
    // Trim off the Mars crap at the top
    marsargs[0] = "nc"; // No copyright
    marsargs[1] = input_file + ".s";
    String finalFile = input_file + ".s" + ".out";
    try{

      String finalOutput = "";
      Process p = Runtime.getRuntime().exec(
          "java -jar ../Mars.jar " + marsargs[0] + " " + marsargs[1] );
      BufferedInputStream streaming = new BufferedInputStream(p.getInputStream());
      BufferedInputStream errStream = new BufferedInputStream(p.getErrorStream());
      p.waitFor();

      File f = new File(finalFile);
      if(!f.exists())
      {
        System.out.println("Deleting the old " + finalFile);
        f.delete();
      }

      FileWriter fstream = new FileWriter(finalFile);
      BufferedWriter out = new BufferedWriter(fstream);

      while(streaming.available() > 0)
      {
        out.write(streaming.read());
        //finalOutput += (char)streaming.read();
      }

      out.close();

      while(errStream.available() > 0)
      {
        System.err.print((char) errStream.read());
      }

    }
    catch(IOException ioe)
    {
      System.out.println("Aw crap.");
      System.out.println(ioe);
    }
    catch(InterruptedException ie)
    {
      System.out.println("Aw crap.");
      System.out.println(ie);
    }

    // Create a file called input_file.compilerOut
    // return input_file.compilerOut
    return input_file + ".s" + ".out";
  }

  public static void main(String[] args)
  {
    ExprEvalTester eet = new ExprEvalTester("../TestCases/expr1.txt");
    eet.process();
  }

}
