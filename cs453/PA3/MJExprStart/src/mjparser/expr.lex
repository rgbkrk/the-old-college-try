/**
 * CS 453h
 * Kyle Kelley, Dave Newman, Paul Gagliardi
 */
package mjparser;

import java_cup.runtime.Symbol;
%%
%cup
%char
%line
%public
%%
"+" { return new Symbol(sym.PLUS); }
"-" { return new Symbol(sym.MINUS); }
"*" { return new Symbol(sym.TIMES); }
"(" { return new Symbol(sym.LPAREN); }
")" { return new Symbol(sym.RPAREN); }
[0-9]+ {return new Symbol(sym.NUMBER, new TokenValue(yytext(), yyline, yychar));}
[ \t\r\n\f] { /* ignore white space. */ }
. { System.err.println("Illegal character: "+yytext()); }
