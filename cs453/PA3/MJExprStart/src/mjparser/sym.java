
//----------------------------------------------------
// The following code was generated by CUP v0.11a beta 20060608
// Wed Mar 03 15:11:56 MST 2010
//----------------------------------------------------

package mjparser;

/** CUP generated class containing symbol constants. */
public class sym {
  /* terminals */
  public static final int RPAREN = 6;
  public static final int error = 1;
  public static final int PLUS = 2;
  public static final int NUMBER = 7;
  public static final int MINUS = 3;
  public static final int LPAREN = 5;
  public static final int TIMES = 4;
  public static final int EOF = 0;
}

