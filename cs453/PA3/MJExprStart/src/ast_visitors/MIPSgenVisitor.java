/**
 * This code prints ASTs in a post-order traversal.
 *
 * 2/23/2010 - Modified from our own EvaVisitor.java
 * 2/23/2010 - Modified from Michelle Strout's DotVisitor.java
 * 			   Kyle Kelley, Dave Newman, Paul Gagliardi 
 * 6/06 - Modified from Brian Richard's ParserTest.Java.
 *        Michelle Strout
 */
package ast_visitors;

// These imports are all for SableCC-generated packages.
import java.io.PrintWriter;
import java.util.Stack;

import ast.analysis.DepthFirstAdapter;
import ast.node.*;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


/**
 * We extend the DepthFirstAdapter.  
 * Visitors invoke a defaultCase method on each node they visit.  
 * We evaulate the tree and print result
 */
public class MIPSgenVisitor extends DepthFirstAdapter {
   private PrintWriter out;
   private Stack<Node> nodeStack;

   public MIPSgenVisitor(PrintWriter out) {
      this.out = out;
      this.nodeStack = new Stack<Node>();
   }

   public void defaultIn(Node node) {
     if(nodeStack.empty()) {
       prologue();
     }
    this.nodeStack.push(node);
    out.flush();
   }

   public void defaultOut(Node node) {
       this.nodeStack.pop();
       if(nodeStack.empty())
       {
         epilogue();
       }
       out.flush();
   }
   
   public void outPlusExp(PlusExp node){
     out.println("\t# PlusExp");
     mipsPop("$t1");
     mipsPop("$t0");
     out.println("\tadd $t0, $t0, $t1");
     mipsPush("$t0");
     out.println();
     out.flush();
     defaultOut(node);
   }

   public void outIntegerExp(IntegerExp node){
     out.println("\t# IntegerExp");
     out.println("\tli $t0, " + node.getLiteral().getText());
     mipsPush("$t0");
     out.println();
     out.flush();
     defaultOut(node);
   }
   
   public void outMulExp(MulExp node){
	   out.println("\t# MulExp");
     mipsPop("$t1");
     mipsPop("$t0");
     out.println("\tmul $t0, $t0, $t1");
     mipsPush("$t0");
     out.println();
     out.flush();
     defaultOut(node);

   }
   
   public void outMinusExp(MinusExp node){
     out.println("\t# MinusExp");
     mipsPop("$t1");
     mipsPop("$t0");
     out.println("\tsub $t0, $t0, $t1");
     mipsPush("$t0");
     out.println();
     out.flush();
     defaultOut(node);
   }


   /** Helper routines to push a value onto the stack
    * and pop it off */
   private void mipsPush(String register)
   {
     out.println("\taddi $sp, $sp, -4");
     out.println("\tsw " + register + ", 0($sp)");
   }

   private void mipsPop(String register)
   {
     out.println("\tlw " + register + ", 0($sp)");
     out.println("\taddi $sp, $sp, 4");
   }

   private void prologue()
   {
     out.println("\t.text");
     out.println("main:");
     out.println();
   }

   private void epilogue()
   {
         //Last result should be the value to print
         out.println("\tjal _printint #Print out the last result");
         mipsPop("$t0"); //Pop the argument even though we don't need it
         out.println("\tli $v0, 10");
         out.println("\tsyscall");
         out.println();

         //Necessary utility routines
         _printint();
   }

   private void _printint()
   {
     try{
       String source = suckSource("src/MIPSutils/printint.s");
       out.println(source);
     }
     catch(IOException ioe)
     {
       // Backup plan, in case of extreme file systems!
       out.println("\t.text");
       out.println("_printint:");

       out.println("\taddi $sp, $sp, -4	# push return addr");
       out.println("\tsw   $ra, 0($sp)");

       out.println("\taddi $sp, $sp, -4   # push frame pointer");
       out.println("\tsw   $fp, 0($sp)");

       out.println("\taddi $fp, $sp, -4	# set up frame pointer");

       out.println("\tlw	$a0, 12($fp)	# load the int");
		   out.println("\t	# will it work with just $sp?");

       out.println("\tli	$v0, 1		# print the int");
       out.println("\tsyscall");

       out.println("\tla $a0, lf");
       out.println("\tli $v0, 4");
       out.println("\tsyscall");

       out.println("\tlw  $fp, 0($sp) 	# pop frame pointer");
       out.println("\taddi $sp, $sp, 4");

       out.println("\tlw  $ra, 0($sp)	# pop return addr");
       out.println("\taddi $sp, $sp, 4");

       out.println("\tjr   $ra		# return to caller");
       out.println(".data:");
       out.println("\tlf: .asciiz \"\\n\"");
     }
   }

   private String suckSource(String filename) throws IOException
   {
     BufferedReader input =  new BufferedReader(new FileReader(filename));
     String line;
     String source = "";
     String nl = System.getProperty("line.separator");
     while((line = input.readLine())!= null)
     {
       source = source + line + nl;
     }
     input.close();

     return source;

   }
   
}
