/**
 * This evaluates an ast. 
 *
 * 2/23/2010 - Modified from Michelle Strout's DotVisitor.java
 * 			   Kyle Kelley, Dave Newman, Paul Gagliardi 
 * 6/06 - Modified from Brian Richard's ParserTest.Java.
 *        Michelle Strout
 */
package ast_visitors;

// These imports are all for SableCC-generated packages.
import java.io.PrintStream;
import java.util.Stack;

import ast.analysis.DepthFirstAdapter;
import ast.node.*;

/**
 * We extend the DepthFirstAdapter.  
 * Visitors invoke a defaultCase method on each node they visit.  
 * We evaulate the tree and print result
 */
public class EvalVisitor extends DepthFirstAdapter {
   private int nodeCount = 0;
   private PrintStream out;
   private Stack<Integer> evalStack;
   
   private Node ast_root;

   public EvalVisitor(Node ast_root) {
      this.out = System.out;
      this.ast_root = ast_root;
      this.evalStack = new Stack<Integer>();
   }

   /** Checks to see if node is root, if so prints top of stack */
   public void defaultOut(Node node) {
       if(node.equals(ast_root)) {
		   int answer = evalStack.pop();
		   if(evalStack.isEmpty()) out.println(answer);
		   else out.println("Exception in EvalVisitor! =(");
       }
       out.flush();
   }
   
   public void outPlusExp(PlusExp node){
       int temp1 = evalStack.pop();
       int temp2 = evalStack.pop();
       evalStack.push(temp1 + temp2);
       defaultOut(node);
   }
   
   public void outIntegerExp(IntegerExp node){
	   evalStack.push(Integer.parseInt(node.getLiteral().getText()));
	   defaultOut(node);
   }
   
   public void outMulExp(MulExp node){
	   int temp1 = evalStack.pop();
       int temp2 = evalStack.pop();
       evalStack.push(temp1 * temp2);
	   defaultOut(node);
   }
   
   public void outMinusExp(MinusExp node){
	   int temp1 = evalStack.pop();
       int temp2 = evalStack.pop();
       evalStack.push(temp2 - temp1); //second pop minus first pop b/c subtraction is not communtative
	   defaultOut(node);
   }
   
}
