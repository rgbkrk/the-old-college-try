public abstract class AbstractExpectedTester{

  protected String input_file;
  
  public AbstractExpectedTester(String input_file) {	
    this.input_file = input_file;
  }

  /*Return filename of output */
  public abstract String process();

}