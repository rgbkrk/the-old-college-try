/*
 * HW3Driver.java
 * cs453 Spring 2010
 * cs453h group: David Newman, Kyle Kelley, Paul Gagliardi
 *
 */

import java.io.FileReader;
import ast_visitors.*;
import mjparser.*;

public class HW3Driver {
	 
	  public static void main(String args[]) {
	    
	    // filename should be the last command line option
	    String filename = args[args.length-1];
	    
	    try {

	      // construct the lexer, 
	      // the lexer will be the same for all of the parsers
	      Yylex lexer = new Yylex(new FileReader(filename));
	      
	        // create the AST
	        expr_ast parser = new expr_ast(lexer);
	        ast.node.Node ast_root =         
	            (ast.node.Node)parser.parse().value;
	            
		System.out.println("Depth first post-order output:");	      
	        ast_root.apply(new PrintPostVisitor(ast_root));
		System.out.println();	      
		System.out.println("Depth-first pre-order output:");	      
	        ast_root.apply(new PrintPrefVisitor(ast_root));
	      
	    } catch (Exception e) {
	        e.printStackTrace();
	        System.exit(1);
    }  
  }
}
