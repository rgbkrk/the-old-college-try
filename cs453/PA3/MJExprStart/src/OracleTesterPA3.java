import java.io.BufferedInputStream;
import java.io.FileWriter;
import java.io.FileInputStream;
import java.io.BufferedWriter;
import java.io.File;

public class ExpectedTesterPA3 extends AbstractExpectedTester {

  public ExpectedTesterPA3(String input_file) {
    super(input_file);
  }

  public String process() {
      createCompileJavaFile();
      return this.input_file + ".expected.out";
  }

  public void createCompileJavaFile(){
    String expr = "";
    File readIn = new File(input_file);
    try { 
      FileInputStream fis = new FileInputStream(readIn);
      BufferedInputStream bis = new BufferedInputStream(fis);
      while(bis.available() > 0)
        expr += (char) bis.read();

      File etpaFile = new File("etpa.java");
      if(!etpaFile.exists()) etpaFile.delete();
    	  
      FileWriter fstream = new FileWriter("etpa.java");
      BufferedWriter out = new BufferedWriter(fstream);
      out.write("import java.io.*;\n\n");
      out.write("public class etpa {\n");
      out.write("	public static void main(String [] args){ \n");
      out.write("	int x = " + expr + ";\n");
      out.write(" try { FileWriter fstream = new FileWriter(\"" + this.input_file + ".expected.out\");\n");
      out.write(" BufferedWriter out = new BufferedWriter(fstream);\n");
      out.write(" out.write(new Integer(x).toString());\n");
      out.write(" out.close(); } catch (Exception e) {}\n");
      out.write(" }\n");
      out.write("}");
      out.close();
      Process p = Runtime.getRuntime().exec("javac etpa.java");
      BufferedInputStream estreaming = new BufferedInputStream(p.getErrorStream());
      p.waitFor();
      while(estreaming.available() > 0) System.out.println(estreaming.read());
      Process p2 = Runtime.getRuntime().exec("java etpa");
      p2.waitFor();
    } catch (Exception e) {System.out.println("error!");}
  }

}
