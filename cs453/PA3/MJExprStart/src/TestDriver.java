public class TestDriver {
	private AbstractExpectedTester aet;
	private AbstractCompilerTester act;

	private String aetFileName;
	private String actFileName;

	public TestDriver(String file_name) {
		aet = new ExpectedTesterPA3(file_name);
		act = new ExprEvalTester(file_name);
		doProcessing();
		
	}

	private void doProcessing() {
		aetFileName = aet.process();
		actFileName = act.process();
	}


	public static void main (String [] args) {
		new TestDriver(args[0]);
	}

}
