public abstract class AbstractCompilerTester {
  protected String input_file;

  public AbstractCompilerTester(String input_file)
  {
    this.input_file = input_file;
  }

  /**
   * process conducts specific testing
   * @return The file name associated with the final output
   */
  public abstract String process();

}
