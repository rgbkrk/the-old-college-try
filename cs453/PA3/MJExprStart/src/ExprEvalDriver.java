/*
 * ExprEvalDriver.java
 *
 * usage: 
 *   java ExprEval [--one-pass-eval | --two-pass-eval | --two-pass-mips ] infile
 *
 * This driver calls one of three possible expression evaluation parsers.
 * The default parser is the one-pass-eval.
 *
 */
import java.io.FileReader;
import java.io.PrintWriter;

import mjparser.*;
import ast_visitors.*;

public class ExprEvalDriver {

	  private static void usage() {
	      System.err.println(
	        "ExprEval: Specify input file in program arguments\n" +
	        "java ExprEval [--one-pass-eval | --two-pass-eval | --two-pass-mips ] infile");
	  }
	 
	  public static void main(String args[]) 
	  {
	    
	    if(args.length < 1)
	    {         
	        usage();
	        System.exit(1);
	    }

	    // filename should be the last command line option
	    String filename = args[args.length-1];
	    
	    try {

	      // construct the lexer, 
	      // the lexer will be the same for all of the parsers
	      Yylex lexer = new Yylex(new FileReader(filename));
	      
	      // determine which parser to execute
	      if (args.length < 2 || args[0].equals("--one-pass-eval")) {

	        //System.out.println("Implement Me!");
	     
	        // evaluate the expression while parsing
	        expr_eval parser = new expr_eval(lexer);
	        System.out.println(parser.parse().value);
	        
	      } else if ( args[0].equals("--two-pass-eval") ) {

	        // create the AST
	        expr_ast parser = new expr_ast(lexer);
	        ast.node.Node ast_root =         
	            (ast.node.Node)parser.parse().value;
	            
	        // print ast to file
	        java.io.PrintStream astout =
	          new java.io.PrintStream(
	            new java.io.FileOutputStream(filename + ".ast.dot"));
	        ast_root.apply(new DotVisitor(new PrintWriter(astout)));
	        System.out.println("Printing dot file to " + filename + ".ast.dot");
	      
	        // evaluate the value of expr, also prints value to stdout
	        ast_root.apply(new EvalVisitor(ast_root));
	      
	      } else if ( args[0].equals("--two-pass-mips") ) {

	        //System.out.println("Implement Me!");

	        // create the AST
	        expr_ast parser = new expr_ast(lexer);
	        ast.node.Node ast_root =         
	            (ast.node.Node)parser.parse().value;
	            
	        // print ast to file
	        java.io.PrintStream astout =
	          new java.io.PrintStream(
	            new java.io.FileOutputStream(filename + ".ast.dot"));
	        ast_root.apply(new DotVisitor(new PrintWriter(astout)));
	        System.out.println("Printing dot file to " + filename + ".ast.dot");
	      
	        // generate MIPS code that evaluates the expression
	        java.io.PrintStream mipsout =
	          new java.io.PrintStream(
	            new java.io.FileOutputStream(filename + ".s"));
	        System.out.println("Printing MARS MIPS file to " + filename + ".s");
	        ast_root.apply(new MIPSgenVisitor(new PrintWriter(mipsout)));

	      

          } else {
            System.out.print("Unknown command-line option ");
            System.out.println(args[0]);
          }

	    } catch (Exception e) {
	        e.printStackTrace();
	        System.exit(1);
	    }  
	  }

}
