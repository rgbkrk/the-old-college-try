1: That the lexeme does not include the last symbol that brought us to the final state. More generally, if there are n many asterisks, then the last n symbols should not be included in the lexeme. That is, our forward pointer has to be recalled by n symbols.


