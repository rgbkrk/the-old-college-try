package lines;

/*
 * The Lines data structure maps nodes in the ast to the line and position in
 * line where they were located in the source file.  Such information is
 * needed for useful error messages.
 */

import ast.node.*;
import exceptions.*;
import java.util.*;


public class Lines
{

  // lines map (key: node, value: line)
  private HashMap<Node,Integer> mLines = new HashMap<Node,Integer>();

  // positions map (key: node, value: pos)
  private HashMap<Node,Integer> mPositions = new HashMap<Node,Integer>();

  public int getLine(Node node)
  {
    if(node == null)
    {
      throw new InternalException("unexpected null argument");
    }

    return ((Integer) mLines.get(node)).intValue();
  }

  public int getPos(Node node)
  {
    if(node == null)
    {
      throw new InternalException("unexpected null argument");
    }

    return ((Integer) mPositions.get(node)).intValue();
  }

  public void setLine(Node node, int line)
  {
    if(node == null)
    {
      throw new InternalException("unexpected null argument");
    }

    mLines.put(node, new Integer(line));
  }

  public void setPos(Node node, int pos)
  {
    if(node == null)
    {
      throw new InternalException("unexpected null argument");
    }

    mPositions.put(node, new Integer(pos));
  }
}
