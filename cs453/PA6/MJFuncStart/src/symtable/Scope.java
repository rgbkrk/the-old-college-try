package symtable;

import java.util.HashMap;

public class Scope {
  private HashMap<String,SymbolTableEntry> tableData = new HashMap();
  private Scope mEnclosing; //What is this? The owner?

  public Scope(Scope mEnclosing){
    this.mEnclosing = mEnclosing;
  }

  public SymbolTableEntry lookup(String key) {
    SymbolTableEntry ste = tableData.get(key);
    if(ste == null && mEnclosing != null)
      ste = mEnclosing.lookup(key);
    return ste;
  }

  public int size(){
    return tableData.size();
  }

  public void insert(SymbolTableEntry ste) {
    tableData.put(ste.getName().getText(), ste);
  }

  public String genDOT(boolean global) {
    String dot = "";
    int myNum = SymbolTable.nextID();
    if(!global)
      dot += myNum + ":<f0>;\n";
    dot += "  " + myNum + " [label=\" <f0> Scope ";
    //Loop through SymbolTableEntries for fields
    int field = 1;

    //Print the dictionary of the symbol table
    for(SymbolTableEntry ste : tableData.values()) {
      dot += "| <f" + field + "> mDict\\[" + ste.getName().getText().trim() + "\\] ";
      field++;
    }
    dot += "\"];\n"; //End declaration of scope

    field = 1;

    //Label
    for(SymbolTableEntry ste : tableData.values()) {
      dot += "  " + myNum + ":<f"+field+"> -> "+ ste.genDOT();
      field++; 
    }
  
    return dot;
  }


}
