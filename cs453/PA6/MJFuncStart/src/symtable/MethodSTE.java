package symtable;

public class MethodSTE extends NamedScopeSTE {

  protected Signature signature;

  public String genDOT() {
      int uid = SymbolTable.nextID();
      String dot = uid + ":<f0>;\n";
      dot += "  " + uid + " ";
      dot += "[label=\" <f0> MethodSTE | <f1> mName = " + name +
             " | <f2> " + signature.genDOT() + 
             " | <f3> mScope\"];\n";

      dot += "  " + uid + ":<f3> -> " + mScope.genDOT(false);
      return dot;

  }

  public String toString()
  {
    return "name " + name + " signature " + signature;
  }

  public Signature getSignature() {return signature;}
  public void setSignature(Signature signature) {this.signature = signature;}

}
