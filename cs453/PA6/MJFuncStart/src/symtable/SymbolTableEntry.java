package symtable;

import ast.node.Token;
import ast.node.BoolType;
import ast.node.IntType;
import ast.node.IType;

public abstract class SymbolTableEntry {
   public enum TYPE {
     BOOL, INT, INVALID
   }

   protected Token name;

   public Token getName(){return name;}
   public void setName(Token name){this.name = name;}

   public abstract String genDOT();


   public static TYPE getType(IType it){
     if(it instanceof BoolType)
       return TYPE.BOOL;
     else if(it instanceof IntType)
       return TYPE.INT;
     return TYPE.INVALID;
   }
}
