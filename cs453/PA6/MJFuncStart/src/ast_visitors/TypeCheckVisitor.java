/**
 * This  visitor checks the type of our AST nodes and reports errors.
 *
 * 4/1/2010  - Modified from our own MIPSgenVisitor.java
 * 2/23/2010 - Modified from our own EvaVisitor.java
 * 2/23/2010 - Modified from Michelle Strout's DotVisitor.java
 * 			   Kyle Kelley, Dave Newman, Paul Gagliardi 
 * 6/06 - Modified from Brian Richard's ParserTest.Java.
 *        Michelle Strout
 */
package ast_visitors;

// These imports are all for SableCC-generated packages.
import java.util.Stack;
import java.util.EmptyStackException;

import ast.analysis.DepthFirstAdapter;
import ast.node.*;
import java.util.List;

import symtable.*;

/**
 * We extend the DepthFirstAdapter.  
 * Visitors invoke a defaultCase method on each node they visit.  
 * We evaulate the tree and print result
 */
public class TypeCheckVisitor extends DepthFirstAdapter {

  private class TypeAndToken{
    SymbolTableEntry.TYPE type;
    Token token;
    public TypeAndToken(SymbolTableEntry.TYPE type, Token token){
      this.type = type;
      this.token = token;
    }

    public String toString() {
      return "type: " + type + " token " + token.getText() + " " + token.getLine() + " " + token.getPos();
    }
  }

   public class TypeCheckException extends RuntimeException {}

   private SymbolTable symTable;

   private Stack<TypeAndToken> typeAndTokenStack;

   public TypeCheckVisitor(SymbolTable symTable) {
      this.symTable = symTable;
      this.typeAndTokenStack = new Stack<TypeAndToken>();
   }

   private void compilationError(Token tok, String message) {
      System.err.println( "[" + tok.getLine() + "," + tok.getPos() + 
                             "] " + message);
      throw new TypeCheckException();
   }

   public void outAssignStatement(AssignStatement node) {

      VariableSTE localVar = (VariableSTE)this.symTable.lookup(node.getId().getText());
      
      if (localVar == null) {
        // Freak out here for undeclared vars.
        compilationError(node.getId(), "Undeclared variable " + node.getId().getText());
      }

      TypeAndToken tat = typeAndTokenStack.pop();

      if ( tat.type != localVar.getType() ){
        // Freak out because the variable and the expression
        // being assigned to it are not of the same type.
        compilationError(tat.token, "Invalid expression type assigned to variable " + node.getId());
      }
   }

  public void outIdExp(IdExp node){
      // push the type onto the typeAndTokenStack
      VariableSTE localVar = (VariableSTE)this.symTable.lookup(node.getId().getText());
      if (localVar != null)
         typeAndTokenStack.push(new TypeAndToken(localVar.getType(),node.getId()));
      else
         compilationError(node.getId(), "Undeclared variable " + node.getId().getText());
   }

  private void opTypeCheck(SymbolTableEntry.TYPE type, String op) {
    TypeAndToken tat = typeAndTokenStack.pop();

    if (tat.type != type)
      compilationError(tat.token, "Invalid operand type for operator " + op);
    //Push the token info back on
    typeAndTokenStack.push(tat);
  }

  private void argTypeCheck(SymbolTableEntry.TYPE type, String methodName) {
    TypeAndToken tat = typeAndTokenStack.pop();

    if (tat.type != type)
      compilationError(tat.token, "Invalid argument type for method " + methodName);
    // No push back, as the print statement is done.
  }

  private void binOpTypeCheck(SymbolTableEntry.TYPE type, String op) throws TypeCheckException {
    TypeAndToken right, left;
    try {
      right = typeAndTokenStack.pop();
      left = typeAndTokenStack.pop();
    }
    catch (EmptyStackException e) {
      System.err.println("WHOA, Empty Stack Exception. Why ME!?");
      throw new TypeCheckException();
    }
    if (left.type != type)
      compilationError(left.token, "Invalid left operand type for operator " + op);
    else if (right.type != type){
      compilationError(right.token, "Invalid right operand type for operator " + op);
    }
    //Push the token info back on
    typeAndTokenStack.push(left);
  }

  public void outIfStatement(IfStatement node)
  {
    //Check for boolean type
    TypeAndToken tat = typeAndTokenStack.pop();
    if(tat.type != SymbolTableEntry.TYPE.BOOL)
      compilationError(tat.token, "Invalid condition type for if statement");
    //Since we got the expression type, it is done. No need to push
  }

  /*
   * For debugging purposes only
   */
  private void printStack() {
    for(TypeAndToken t: typeAndTokenStack) {
      System.out.println(t.toString());
    }
  }
   
  public void outWhileStatement(WhileStatement node) {
    //Check for boolean type
    TypeAndToken tat = typeAndTokenStack.pop();
    if(tat.type != SymbolTableEntry.TYPE.BOOL)
      compilationError(tat.token, "Invalid condition type for while statement");
    //Since we got the expression type, it is done. No need to push
  }

   public void outTrueExp(TrueExp node){
     // push the type onto the typeAndTokenStack
      typeAndTokenStack.push(new TypeAndToken(SymbolTableEntry.TYPE.BOOL, node.getLiteral()));
   }
   
   public void outFalseExp(FalseExp node){
     // push the type onto the typeAndTokenStack
      typeAndTokenStack.push(new TypeAndToken(SymbolTableEntry.TYPE.BOOL, node.getLiteral()));
   }
   
   public void outIntegerExp(IntegerExp node){
      typeAndTokenStack.push(new TypeAndToken(SymbolTableEntry.TYPE.INT, node.getLiteral()));
   }

   public void outNotExp(NotExp node){
     opTypeCheck(SymbolTableEntry.TYPE.BOOL, "!");
   }

   public void outPrintStatement(PrintStatement node){
     argTypeCheck(SymbolTableEntry.TYPE.INT, "println");
   }

   public void outAndExp(AndExp node){
     binOpTypeCheck(SymbolTableEntry.TYPE.BOOL, "&&");
   }

   public void outLtExp(LtExp node){
    binOpTypeCheck(SymbolTableEntry.TYPE.INT, "<");
    TypeAndToken left = typeAndTokenStack.pop();
    //Push the token info back on, changing one thing... It's now a boolean!
    TypeAndToken newLeft = new TypeAndToken(SymbolTableEntry.TYPE.BOOL, left.token);
    typeAndTokenStack.push(newLeft);
   }

   public void outPlusExp(PlusExp node){
     binOpTypeCheck(SymbolTableEntry.TYPE.INT, "+");
   }

   public void outMulExp(MulExp node){
     binOpTypeCheck(SymbolTableEntry.TYPE.INT, "*");
   }
   
   public void outMinusExp(MinusExp node){
     binOpTypeCheck(SymbolTableEntry.TYPE.INT, "-");
   }

   /** PA6 Additions */
   /**
    * Need to add the following errors:

      [LINENUM,POSNUM] Invalid type returned from method METHODNAME
      // any method declaration

      [LINENUM,POSNUM] Method METHODNAME does not exist

      [LINENUM,POSNUM] Method METHODNAME requires exactly NUM arguments

      [LINENUM,POSNUM] Invalid argument type for method METHODNAME
        // any call to a method (note that println is a method)
    * 
    *
    * ...also need to think about scoping...
    */
   public void outCallExp(CallExp node){
     /*
      * Check for these:

      [LINENUM,POSNUM] Method METHODNAME does not exist

      [LINENUM,POSNUM] Method METHODNAME requires exactly NUM arguments

      [LINENUM,POSNUM] Invalid argument type for method METHODNAME
        // any call to a method (note that println is a method)
      */

      SymbolTableEntry ste = symTable.lookup(node.getId().getText());
      if(ste == null || ! (ste instanceof MethodSTE)) compilationError(node.getId(), "Method " + node.getId().getText() + " does not exist");
      MethodSTE mste = (MethodSTE) ste;
      Signature sig = mste.getSignature();
      List<SymbolTableEntry.TYPE> formals = sig.getFormals();

      if(formals.size() != node.getArgs().size()) compilationError(node.getId(), "Method " + node.getId().getText() + " requires exactly " + formals.size() + " arguments");
      for(SymbolTableEntry.TYPE s : formals) {
        TypeAndToken tat = typeAndTokenStack.pop();
        if (tat.type != s) compilationError(tat.token, "Invalid argument type for method " + node.getId().getText());
      }
      TypeAndToken temp = new TypeAndToken(sig.getReturnType(), node.getId());
      typeAndTokenStack.push(temp);

   }

   public void inMethodDecl(MethodDecl node) {
      symTable.pushScope(node.getName().getText()); 
   }

   public void outMethodDecl(MethodDecl node) {
     //Scope needs pushing too!!
     
      //[LINENUM,POSNUM] Invalid type returned from method METHODNAME
      // any method declaration
    //Check for same type
    TypeAndToken tat = typeAndTokenStack.pop();
    if(tat.type != SymbolTableEntry.getType(node.getType()))
      compilationError(tat.token, "Invalid type returned from method " + node.getName().getText());
    //Since we got the expression type, it is done. No need to push
    symTable.popScope();
     //Last expression should be the return statement
   }

}
