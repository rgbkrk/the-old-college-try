package ast_visitors;

/**
 * This ast walker generates dot output for the AST.  
 *
 * 6/06 - Modified from Brian Richard's ParserTest.Java.
 *        Michelle Strout
 */

import ast.analysis.*;
import ast.node.*;
import java.io.PrintWriter;
import java.util.Stack;

/**
 * We extend the DepthFirstAdapter.  
 * Visitors invoke a defaultCase method on each node they visit.  
 * We override this method so that it
 * prints info about a node.
 */
public class DotLinesVisitor extends DepthFirstAdapter {
   private int nodeCount = 0;
   private PrintWriter out;
   private Stack<Integer> nodeStack;
   private lines.Lines mLines;
   
   /** Constructor takes a PrintWriter, and stores in instance var. */
   public DotLinesVisitor(PrintWriter out,lines.Lines lines) {
      this.out = out;
      this.nodeStack = new Stack<Integer>();
      mLines = lines;
   }

   
   /** Override the default action taken at each visited node -- print
    * out dot node with parse tree node's text as a label.
    * Also print out edge from parent to this node. */
   public void defaultCase(Node node) {
       nodeDotOutput(node);
   }
   
   /** When descending into a new node, increment the indentation level.
    * Decrement the level when leaving a node. */
   public void defaultIn(Node node) {
       if (nodeStack.empty()) {
           out.println("digraph ParseGraph {");
       }

       nodeDotOutput(node);
       
       // store this node id on the nodeStack
       // the call to nodeDotOutput increments for
       // the next guy so we have to decrement here
       nodeStack.push(nodeCount-1);    
   }
   
   public void defaultOut(Node node) {
       nodeStack.pop();
       if (nodeStack.empty()) {
           out.println("}");
       }
       out.flush();
   }
   
   /* A helper output routine that generates the
    * dot node and the dot edges from the parent
    * to the node.
    */
   private void nodeDotOutput(Node node)
   {
       // dot node
       out.print(nodeCount);
       out.print(" [ label=\"");
       printNodeName(node);
       if (node instanceof Token) {
           out.print("\\n");
           out.print(((Token)node).getText());
       }
       out.print("\\nline = ");
       out.print(mLines.getLine(node));
       out.print("\\npos = ");
       out.print(mLines.getPos(node));
       out.println("\" ];");
       
       // print dot edge to parent
       if (!nodeStack.empty()) {
           out.print(nodeStack.peek());
           out.print(" -> ");
           out.println(nodeCount);
       }
       
       // increment for ourseves
       nodeCount++;
   }

   /** A helper that trims a node's class name before printing it.
    * (E.g., "node.TAsmt" --> "TAsmt".) 
    */
   private void printNodeName(Node node) {
      String fullName = node.getClass().getName();
      String name = fullName.substring(fullName.lastIndexOf('.')+1);
      
      out.print(name);
   }
   
}
