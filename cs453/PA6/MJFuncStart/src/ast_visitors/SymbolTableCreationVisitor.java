/**
 *
 * 			   Kyle Kelley, Dave Newman, Paul Gagliardi 
 */
package ast_visitors;

// These imports are all for SableCC-generated packages.
import java.io.PrintWriter;
import java.util.Stack;
import java.util.HashMap;

import ast.analysis.DepthFirstAdapter;
import ast.node.*;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


import symtable.*;


/**
 * We extend the DepthFirstAdapter.  
 * Visitors invoke a defaultCase method on each node they visit.  
 * We evaulate the tree and print result
 */
public class SymbolTableCreationVisitor extends DepthFirstAdapter {

   // Instance variables.
   private SymbolTable symTable;
   private boolean compilationFails;
   private static int SIZEOFINT = 4;
   private int localVariableOffset;
   private int argumentOffset;

   private static int START_ARGUMENTS = 16;
   private static int START_LOCALS = 0;

   // Creator.
   public SymbolTableCreationVisitor() {
      this.symTable = new SymbolTable();
      this.localVariableOffset = START_LOCALS;
      this.argumentOffset = START_ARGUMENTS;
      this.compilationFails = false;
   }

   public boolean getCompilationFailed() {
     return compilationFails;
   }

   public SymbolTable getSymbolTable() {
     return this.symTable;
   }

   private void compilationError(Token tok, String message) {
      System.err.println( "[" + tok.getLine() + "," + tok.getPos() + 
                             "] " + message + " " + tok.getText());
   }

   public void outVarDecl(VarDecl node)
   {
     if(addToSymbolTable(node.getType(), node.getName(), localVariableOffset))
        localVariableOffset -= SIZEOFINT; // assumes booleans and ints are of same size.
   }

   public void outFormal(Formal node)
   {
     if(addToSymbolTable(node.getType(), node.getName(), argumentOffset));
       argumentOffset += SIZEOFINT;
   }

   public boolean addToSymbolTable(IType type, Token token, int offset)
   {
     if (this.symTable.lookup(token.getText()) != null) {
        // Freak out, then recover because we're required to continue
        // The var has already been declared so we don't add anything
        // to the symbol table.
        System.err.println( "[" + token.getLine() + "," + token.getPos() + 
                             "] Redeclared symbol " + token.getText());
        compilationFails = true;
        return false;
     }
     
     // We have an undeclared variable, so put it in the symbol table.
     VariableSTE ste = new VariableSTE();
     ste.setOffset(offset);
     ste.setBase("$fp");
     ste.setName(token);
     ste.setType(SymbolTableEntry.getType(type));
     this.symTable.insert(ste);

     return true;
     

   }

   public void inMethodDecl(MethodDecl node)
   {
     //Think about methods with same name error
     MethodSTE ste = new MethodSTE();
     ste.setName(node.getName());
     Signature signature = new Signature(node.getType(), node.getFormals());
     ste.setSignature(signature);

     //Scope will be set within the SymbolTable
     this.symTable.insertAndPushScope(ste);
     //Need to reset the frame pointer to 0
     this.localVariableOffset = START_LOCALS;
     this.argumentOffset = START_ARGUMENTS;

   }

   public void outMethodDecl(MethodDecl node)
   {
     this.symTable.popScope();
   }

   
}
