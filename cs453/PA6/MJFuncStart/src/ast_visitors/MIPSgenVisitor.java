/**
 * This code generates MIPS code from an ast tree.
 *
 * 2/23/2010 - Modified from our own EvaVisitor.java
 * 2/23/2010 - Modified from Michelle Strout's DotVisitor.java
 *          Kyle Kelley, Dave Newman, Paul Gagliardi 
 * 6/06 - Modified from Brian Richard's ParserTest.Java.
 *        Michelle Strout
 */
package ast_visitors;

// These imports are all for SableCC-generated packages.
import java.io.PrintWriter;
import java.util.Stack;
import java.util.HashMap;

import ast.analysis.DepthFirstAdapter;
import ast.node.*;
import label.Label;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


import symtable.*;


/**
 * We extend the DepthFirstAdapter.
 * Visitors invoke a defaultCase method on each node they visit.
 * We evaulate the tree and print result
 */
public class MIPSgenVisitor extends DepthFirstAdapter {

  protected SymbolTable symTable;
  protected static final String TRUE = "1";
  protected static final String FALSE = "0";

  protected PrintWriter out; // The stream to which we will write generated code.

  protected static int SIZEOFINT = 4;

  protected boolean willNeed_printint;

  public MIPSgenVisitor(PrintWriter out, SymbolTable symTable) {
    this.symTable = symTable;
    this.willNeed_printint = false;
    this.out = out;
  }

  public void defaultIn(Node node) {
    out.flush();
  }

  public void defaultOut(Node node) {
    out.flush();
  }


  public void outAssignStatement(AssignStatement node){
    int offset = 0;
    VariableSTE localVar = (VariableSTE)this.symTable.lookup(node.getId().getText());
    offset = localVar.getOffset();
    String base = localVar.getBase();
    out.println("\t# Making assignment to variable " + node.getId().getText());
    out.println("\t# from the stack.");
    mipsPop("$t0");
    out.println("\tsw $t0, " + offset + "(" + base + ")");
    out.println();
    defaultOut(node);
  }

  public void outIdExp(IdExp node){
    int offset = 0;
    VariableSTE localVar = (VariableSTE)this.symTable.lookup(node.getId().getText());
    offset = localVar.getOffset();
    String base = localVar.getBase();
    //Put the rvalue into $t0
    out.println("\t# Get the rvalue of variable " + node.getId().getText());
    out.println("\tlw $t0, " + offset + "(" + base + ")");
    //Push it
    mipsPush("$t0");
    out.println();
    defaultOut(node);
  }

  public void outPrintStatement(PrintStatement node){
    willNeed_printint = true;
    //Assumes that the last item on the stack is to be printed
    out.println("\tjal _printint #System.out.println from the top of the stack");
    mipsPop("$t0"); //Pop the argument that we "put on there"
    out.println();
    defaultOut(node);
  }
  
  public void outPlusExp(PlusExp node){
    out.println("\t# PlusExp");
    opGen("add");
    defaultOut(node);
  }

  public void outMulExp(MulExp node){
    out.println("\t# MulExp");
    opGen("mul");
    defaultOut(node);
  }
  
  public void outMinusExp(MinusExp node){
    out.println("\t# MinusExp");
    opGen("sub");
    defaultOut(node);
  }

  public void opGen(String op) {
    mipsPop("$t1");
    mipsPop("$t0");
    out.println("\t"+op+" $t0, $t0, $t1");
    mipsPush("$t0");
    out.println();
  }

  public void outIntegerExp(IntegerExp node){
    out.println("\t# IntegerExp");
    out.println("\tli $t0, " + node.getLiteral().getText());
    mipsPush("$t0");
    out.println();
    defaultOut(node);
  }

  public void caseIfStatement(IfStatement node) {
    defaultIn(node);
    out.println("\t# IfStatement");
    IStatement elseStatement = node.getElseStatement();
    IStatement thenStatement = node.getThenStatement();
    IExp boolExp = node.getExp();
    if(boolExp!= null) boolExp.apply(this);
    Label elseLabel = new Label();
    Label endLabel = new Label();
    mipsPop("$t0");
    out.println("\tbeq $t0, $0, " + elseLabel);
    if(thenStatement != null) thenStatement.apply(this);
    out.println("\tj " + endLabel);
    genLabel(elseLabel);
    if(elseStatement != null) elseStatement.apply(this);
    genLabel(endLabel);
    defaultOut(node);
  }

  public void caseWhileStatement(WhileStatement node) {
    defaultIn(node);
    out.println("\t# WhileStatement");
    Label startLabel = new Label();
    Label conditionLabel = new Label();
    out.println("\t j " + conditionLabel);
    genLabel(startLabel);
    IStatement body = node.getStatement();
    body.apply(this);
    genLabel(conditionLabel);
    IExp boolExp = node.getExp();
    boolExp.apply(this);
    mipsPop("$t0");
    out.println("\tbne $t0, $0 " + startLabel);
    defaultOut(node);
  }

   /** Logical operators and related expressions. */

  public void outFalseExp(FalseExp node) {
    out.println("\t# FalseExp");
    out.println("\tli $t0," + FALSE);
    mipsPush("$t0");
    out.println();
    defaultOut(node);
  }

  public void outTrueExp(TrueExp node) {
    out.println("\t# TrueExp");
    out.println("\tli $t0, " + TRUE);
    mipsPush("$t0");
    out.println();
    defaultOut(node);
  }

  public void outNotExp(NotExp node) {
    out.println("\t# NotExp");
    mipsPop("$t0");
    out.println("\txor $t0, $t0, " + TRUE);
    mipsPush("$t0");
    out.println();
    defaultOut(node);
  }

  public void caseAndExp(AndExp node) {
    defaultIn(node);
    out.println("\t# CaseAndExp");
    IExp left = node.getLExp();
    left.apply(this);
    mipsPeek("$t0");
    Label endLabel = new Label();
    out.println("\t# Using register 0 for false!");
    out.println("\tbeq $t0, $0, " + endLabel + " # Was left operand false?"); 
    out.println("\taddi $sp, $sp, 4 # Get rid of \"true\" result from left operand (&&)");
    IExp right = node.getRExp();
    right.apply(this);
    genLabel(endLabel);
    defaultOut(node);
  }

  public void outLtExp(LtExp node) {
    out.println("\t# LtExp");
    mipsPop("$t1");
    mipsPop("$t0");
    out.println("\tslt $t0, $t0, $t1");
    mipsPush("$t0");
    defaultOut(node);
  }

   /** Helper routines to push a value onto the stack
    * and pop it off */
  protected void mipsPush(String register, String comment) {
    out.println("\taddi $sp, $sp, -4 # mipsPush");
    out.println("\tsw " + register + ", 0($sp)"+"\t# "+comment);
  }

  protected void mipsPush(String register) {
    out.println("\taddi $sp, $sp, -4 # mipsPush");
    out.println("\tsw " + register + ", 0($sp)");
  }

  protected void mipsPop(String register) {
    out.println("\tlw " + register + ", 0($sp) # mipsPop");
    out.println("\taddi $sp, $sp, 4");
  }

  protected void mipsPeek(String register) {
    out.println("\tlw " + register + ", 0($sp) # mipsPeek");
  }

  protected void genLabel(Label label) {
    out.println(label + ":");
  }

  public void outMainClass(MainClass node) {
    //We're all done
    out.println("\t# That's all folks!");
    // Put stack pointer at frame pointer
    out.println("\taddi   $sp, $fp, 0  # \"clean up\" the stack");
    out.println("\tli $v0, 10");
    out.println("\tsyscall");
    out.println();

    defaultOut(node);
   }

  public void outProgram(Program node) {
    //Put in code for necessary utility functions
    //if needed
    if(this.willNeed_printint)
      _printint();
    defaultOut(node);
  }

  public void inMainClass(MainClass node) {
    //main prologue
    out.println("\t.text");
    out.println("main:");
    //out.println();
    // Put frame pointer at stack pointer
    out.println("\taddi $fp, $sp, 0  # init frame pointer");
   //main has no local vars
    defaultIn(node);
  }

  public void inMethodDecl(MethodDecl node) {
    symTable.pushScope(node.getName().getText());
    out.println();
    out.println("\t.text");
    out.println("\t# in method declaration");
    out.println("__" + node.getName().getText() + ":");
    out.println("\taddi $sp, $sp, -"+ SIZEOFINT + "\t#set up space for return value"); 
    mipsPush("$ra", "Save return address.");
    mipsPush("$fp", "Save old frame pointer.");
    out.println("\taddi $fp, $sp, -4\t#set up new frame pointer");

    int allocation = node.getVarDecls().size() * SIZEOFINT;
      if(allocation > 0){
        //out.println("\t# Allocating " + allocation);
        out.println("\taddi  $sp, $sp, -"+ allocation + " # allocate space for local variables.");
    }
    defaultIn(node);
  }

  public void outMethodDecl(MethodDecl node) {
    out.println("\t# out method declaration");
    mipsPop("$t0"); // pop return val
    out.println("\tsw $t0, 12($fp)\t# put return value +12 off fp"); 
    int allocation = node.getVarDecls().size() * SIZEOFINT;
    if(allocation > 0) {
      out.println("\t# Deallocating " + allocation);
      out.println("\t# deallocate space for local variables");
      out.println("\taddi $sp, $sp, " + allocation); 
    }
    mipsPop("$fp");
    mipsPop("$ra");
    out.println("\tjr $ra");
    out.println();
    symTable.popScope();
    defaultOut(node);
  }

  public void caseCallExp(CallExp node) {
    out.println("\t# case call Exp");
    defaultIn(node);
    int allocationSize = SIZEOFINT * node.getArgs().size();
    if (allocationSize>0) {
      out.println("\taddi $sp, $sp, -" + allocationSize + " # Allocate space for the call");
      // note: here there's a new TYPE() that we need to think about?
      int currOffset = 0; 
      for (IExp param : node.getArgs()) {
        out.println("\t#Applying parameter \""+param+"\" for call");
        param.apply(this);
        mipsPop("$t0");
        out.println("\tsw $t0, " + currOffset + "($sp)");
        currOffset += SIZEOFINT;
      }
    }
    out.println("\tjal __" + node.getId().getText());
 
    mipsPop("$t0");
    if (allocationSize>0) {
      out.println("\taddi $sp, $sp, " + allocationSize);
    }
    mipsPush("$t0");
    out.println();
    defaultOut(node);
  }

  protected void _printint() {

    out.println("\t.text");
    out.println("_printint:");

    out.println("\taddi $sp, $sp, -4  # push return addr");
    out.println("\tsw   $ra, 0($sp)");

    out.println("\taddi $sp, $sp, -4   # push frame pointer");
    out.println("\tsw   $fp, 0($sp)");

    out.println("\taddi $fp, $sp, -4  # set up frame pointer");

    out.println("\tlw  $a0, 12($fp)  # load the int");
    out.println("\t  # will it work with just $sp?");

    out.println("\tli  $v0, 1    # print the int");
    out.println("\tsyscall");

    out.println("\tla $a0, lf");
    out.println("\tli $v0, 4");
    out.println("\tsyscall");

    out.println("\tlw  $fp, 0($sp)   # pop frame pointer");
    out.println("\taddi $sp, $sp, 4");

    out.println("\tlw  $ra, 0($sp)  # pop return addr");
    out.println("\taddi $sp, $sp, 4");

    out.println("\tjr   $ra    # return to caller");
    out.println(".data:");
    out.println("\tlf: .asciiz \"\\n\"");
    //}
  }

}
