/**
 * This code generates MIPS code from an ast tree.
 *
 * 2/23/2010 - Modified from our own EvaVisitor.java
 * 2/23/2010 - Modified from Michelle Strout's DotVisitor.java
 * 			   Kyle Kelley, Dave Newman, Paul Gagliardi 
 * 6/06 - Modified from Brian Richard's ParserTest.Java.
 *        Michelle Strout
 */
package ast_visitors;

// These imports are all for SableCC-generated packages.
import java.io.PrintWriter;
import java.util.Stack;
import java.util.HashMap;
import java.util.ArrayList;

import ast.analysis.DepthFirstAdapter;
import ast_visitors.MIPSgenVisitor;
import ast.node.*;
import label.Label;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import symtable.*;



/**
 * We extend the DepthFirstAdapter.  
 * Visitors invoke a defaultCase method on each node they visit.  
 * We evaulate the tree and print result
 */
public class MIPSgenECVisitor extends MIPSgenVisitor {

  // The following are specific to the register allocation visitor.
  private final static String ON_STACK = "ON_STACK"; 
  private final static String LEFT_REG = "$t0";
  private final static String RIGHT_REG = "$t1";
  private final static String SAVE_FROM_STACK_REG = LEFT_REG;

  private Stack<String> regStack;
  private ArrayList<RegObject> registers;

  public MIPSgenECVisitor(PrintWriter out, SymbolTable symTable) {
    super(out, symTable);
    this.regStack = new Stack<String>();
    this.registers = new ArrayList<RegObject>();
    //Create the ts!
    for(int i = 2; i <10; i++) {
      registers.add(new RegObject("$t"+i, true));
    } 
    registers.add(new RegObject("$a0",true));
    registers.add(new RegObject("$a1",true));
    registers.add(new RegObject("$a2",true));
    registers.add(new RegObject("$a3",true));
    registers.add(new RegObject("$v0",true));
    registers.add(new RegObject("$v1",true));
  }


   public void outMethodDecl(MethodDecl node) {
    out.println("\t# out method declaration");
    String location = getLocation(true);
    // we got the result of the last computation, so we store it to the stack frame.
    out.println("\tsw "+ location + ", 12($fp)\t# put return value +12 off fp"); 
    int allocation = node.getVarDecls().size() * SIZEOFINT;
    if(allocation > 0) {
      out.println("\t# Deallocating " + allocation);
      out.println("\taddi $sp, $sp, " + allocation + "\t# deallocate space for local variables"); 
    }
    mipsPop("$fp");
    mipsPop("$ra");
    out.println("\tjr $ra");
    out.println();
    symTable.popScope();
    defaultOut(node);
  }


  public void saveRegisters() {
    for ( int i=0; i<registers.size(); i++) {
      RegObject reg = registers.get(i);
      if (!reg.isFree)
        mipsPush(reg.regName);
    }
  }


  public void loadRegisters() {
    for ( int i=registers.size()-1; i>=0;  i--) {
      RegObject reg = registers.get(i);
      if (!reg.isFree)
        mipsPop(reg.regName);
    }
  }

  public void caseCallExp(CallExp node) {
    out.println("\t# case call Exp");
    defaultIn(node);
    saveRegisters();
    int allocationSize = SIZEOFINT * node.getArgs().size();
    if (allocationSize>0) {
      out.println("\taddi $sp, $sp, -" + allocationSize + " # Allocate space for the call");
      // note: here there's a new TYPE() that we need to think about?
      int currOffset = 0;
      for (IExp param : node.getArgs()) {
        out.println("\t#Applying parameter \""+param+"\" for call");
        param.apply(this);
        String loc = getLocation(true);
        out.println("\tsw " + loc + ", " + currOffset + "($sp)");
        currOffset += SIZEOFINT;
      }
    }
    out.println("\tjal __" + node.getId().getText());
 
    // in the end, we save this to a register or to the stack.
    regStack.push(ON_STACK);
    //String saveLocation = getSaveLocation();
    mipsPop(SAVE_FROM_STACK_REG);
    if (allocationSize>0) {
      out.println("\t# deallocating space for parameters");
      out.println("\taddi $sp, $sp, " + allocationSize);
    }
    // This actually does the store.
    loadRegisters();
    store(SAVE_FROM_STACK_REG);
    out.println();
    defaultOut(node);
  }


  public void outAssignStatement(AssignStatement node){
    VariableSTE localVar = (VariableSTE)this.symTable.lookup(node.getId().getText());
    int offset = localVar.getOffset();
    String base = localVar.getBase();
    out.println("\t# Making assignment to variable " + node.getId().getText());
    String reg = getLocation(true);
    out.println("\tsw " + reg + ", " + offset + "(" + base + ")");
    out.println();
    defaultOut(node);
  }

  // Handle identifiers.
  public void outIdExp(IdExp node){
    VariableSTE localVar = (VariableSTE)this.symTable.lookup(node.getId().getText());
    int offset = localVar.getOffset();
    String base = localVar.getBase();      
    //Put the rvalue into $t0
    out.println("\t# Get the rvalue of variable " + node.getId().getText());
    String reg = getSaveLocation();
    out.println("\tlw " + reg + ", " + offset + "(" + base + ")");
    //Push it
    store(reg);
    out.println();
    defaultOut(node);
  }

  // Handle the special case of System.out.println
  public void outPrintStatement(PrintStatement node){
    willNeed_printint = true;
    //Assumes that the last item on the stack is to be printed

    if(! ON_STACK.equals(regStack.peek())){
      String reg = getLocation(true);
      mipsPush(reg);
    }
    else {
      // This will never happen unless there are only 2 registers...
      // If it does happen, we assume the result is on the stack
    }
    out.println("\tjal _printint #System.out.println from the top of the stack");

    //Don't need what we put on the stack
    out.println("\taddi $sp, $sp, 4");

    out.println();
    defaultOut(node);
  }

  //
  // Handle all arithmetic operations
  //
  // Handle addition
  public void outPlusExp(PlusExp node){
    out.println("\t# PlusExp");
    opGen("add");
    defaultOut(node);
  }

  // handle multiplication
  public void outMulExp(MulExp node){
    out.println("\t# MulExp");
    opGen("mul");
    defaultOut(node);
  }

  // Handle subtraction
  public void outMinusExp(MinusExp node){
    out.println("\t# MinusExp");
    opGen("sub");
    defaultOut(node);
  }

  // Generalized operator generation.
  public void opGen(String op) {
    String regRight = getLocation(false);
    String regLeft = getLocation(true);
    String storeRegister = getSaveLocation();
    out.println("\t" + op + " " + storeRegister + ", " + regLeft + ", " + regRight);
    store(storeRegister);
    out.println();
  }

  
  //
  // Handle all literals.
  //
  public void outIntegerExp(IntegerExp node){
    out.println("\t# IntegerExp");
    genLoad(node.getLiteral().getText());
    defaultOut(node);
  }

  public void outFalseExp(FalseExp node) {
    out.println("\t# FalseExp");
    genLoad(FALSE);
    defaultOut(node);
  }

  public void outTrueExp(TrueExp node) {
    out.println("\t# TrueExp");
    genLoad(TRUE);
    defaultOut(node);
  }

  // Generalized literal handler.
  public void genLoad(String val) {
    String reg = getSaveLocation();
    out.println("\tli " + reg + ", " + val);
    store(reg);
    out.println();
  }


   //
   // Handle control statements.
   //
   public void caseIfStatement(IfStatement node) {
     IStatement elseStatement = node.getElseStatement();
     IStatement thenStatement = node.getThenStatement();
     IExp boolExp = node.getExp();
     Label elseLabel = new Label();
     Label endLabel = new Label();

     defaultIn(node);
     out.println("\t# IfStatement");
     if(boolExp!= null) boolExp.apply(this);
     String boolLoc = getLocation(true); // assume left for unary operators
     out.println("\t# Using register 0 for false!");
     out.println("\tbeq " + boolLoc + " $0, " + elseLabel);
     if(thenStatement != null) thenStatement.apply(this);
     out.println("\tj " + endLabel);
     genLabel(elseLabel);
     if(elseStatement != null) elseStatement.apply(this);
     genLabel(endLabel);
     defaultOut(node);
   }

   public void caseWhileStatement(WhileStatement node) {
     IStatement body = node.getStatement();
     IExp boolExp = node.getExp();
     Label startLabel =  new Label();
     Label conditionLabel = new Label();

     defaultIn(node);
     out.println("\t# WhileStatement");
     out.println("\t j " + conditionLabel);
     genLabel(startLabel);
     body.apply(this);
     genLabel(conditionLabel);
     boolExp.apply(this);
     String boolLoc = getLocation(true); // assume left for unary operators?
     out.println("\t# Using register 0 for false!");
     out.println("\tbne " + boolLoc + " $0 " + startLabel);
     defaultOut(node);
   }

   /** Logical operators and related expressions. */

   public void outNotExp(NotExp node) {
     out.println("\t# NotExp uses xor with 1 for NOT");
     String arg = getLocation(true);
     String dest = getSaveLocation();
     out.println("\txor " + dest + ", " + arg + " , " + TRUE);
     store(dest);
     out.println();
     defaultOut(node);
   }

   public void caseAndExp(AndExp node) {
      IExp left = node.getLExp();
      IExp right = node.getRExp();
      Label endLabel = new Label();

      defaultIn(node);
      // Create a temporary to hold the result.
      // Note to self: more writeup for final report!
      String destLoc = getSaveLocation();
      out.println("\t# CaseAndExp");
      left.apply(this);
      String leftLoc = getLocation(true);
      out.println("\t# Using register 0 for false!");
      out.println("\tmove "+ destLoc + " " + leftLoc );
      out.println("\tbeq "+ leftLoc + " $0, " + endLabel);
      right.apply(this);
      String rightLoc = getLocation(true);
      out.println("\tmove "+ destLoc + " " + rightLoc );
      genLabel(endLabel);
      store(destLoc);
      out.println();
      defaultOut(node);
   }

   public void outLtExp(LtExp node) {
      out.println("\t# LtExp");
      opGen("slt");
      defaultOut(node);
   }


  //
  // Utility methods.
  //
  private String getLocation(boolean left) {
    // get a location (stack or register) to use for an operation.
    // left tells us whether we're looking at a left argument or not.
    String tempReg = regStack.pop();
    if(tempReg == ON_STACK) {
      if(left) tempReg = LEFT_REG;
      else tempReg = RIGHT_REG;
      mipsPop(tempReg);
    }
    else{
      free(tempReg);
    }
    return tempReg;
  }

  private String getSaveLocation() {
    // get a location in which we can store something.
    for(RegObject temp: registers) {
      if (temp.isFree) {
        temp.isFree = false;
        regStack.push(temp.regName);
        return temp.regName;
      }	  	
    }
    regStack.push(ON_STACK);
    return SAVE_FROM_STACK_REG;

  }

  private void free(String reg) {
    // note that we're no longer using a location.
    for(RegObject temp: registers) {
      if(temp.regName.equals(reg)){
        temp.isFree = true;
        return;
      }
    }
  }

  private void store(String reg) {
    // generate code to store into a location.
    if(reg.equals(SAVE_FROM_STACK_REG))
      mipsPush(reg);
    //else we are done
  }

  private class RegObject {
    // a private class that keeps track of registers allocated.
    public String regName;
    public boolean isFree;
    public RegObject(String regName, boolean isFree) {
      this.regName = regName;	
      this.isFree = isFree;
    }
  }


}
