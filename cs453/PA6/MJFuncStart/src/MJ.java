/*
 * MJ.java
 * cs 453 Spring 2010 group h
 * David Newman, Kyle Kelley, Paul Gagliardi
 *
 * usage: 
 *   java MJ [--two-pass-interpret | --two-pass-mips | --two-pass-mips-O ] infile
 *
 * This driver calls one of two possible expression evaluation parsers.
 *
 */
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;

import mjparser.*;
import ast_visitors.*;
import symtable.*;

public class MJ {

	private static void usage() {
		System.err.println("MJ: Specify input file in program arguments\n" +
		"java MJ [ --two-pass-interpret | --two-pass-mips | --two-pass-mips-O ] infile");
	}

	private static void print_ast(String filename, ast.node.Node ast_root) throws FileNotFoundException {
		// I added this in an attempt to simplify the code below. DVN 
		// print ast to file
		java.io.PrintStream astout =
			new java.io.PrintStream(
					new java.io.FileOutputStream(filename + ".ast.dot"));
		ast_root.apply(new DotVisitor(new PrintWriter(astout)));
		System.out.println("Printing dot file to " + filename + ".ast.dot");
	}

	public static void main(String args[]) 
	{

		if(args.length < 1)
		{         
			usage();
			System.exit(1);
		}

		// filename should be the last command line option
		String filename = args[args.length-1];
          
		try {

			// First, the code that's common to both parsers.
          
			// construct the lexer, 
			// the lexer will be the same for all of the parsers
			Yylex lexer = new Yylex(new FileReader(filename));

			// create the AST
			stmt_ast parser = new stmt_ast(lexer);
			ast.node.Node ast_root =  (ast.node.Node)parser.parse().value;

			// print ast to file
			print_ast(filename, ast_root);

      SymbolTableCreationVisitor symTableCreateVisitor = new SymbolTableCreationVisitor();
			ast_root.apply(symTableCreateVisitor);

      SymbolTable symTable = null;

			if(!symTableCreateVisitor.getCompilationFailed()){
				symTable = symTableCreateVisitor.getSymbolTable();
        String symTableDotFileName = filename + ".ST.dot";
        System.out.println("Printing symbol table to " + symTableDotFileName);
        symTable.outputDOT(symTableDotFileName);
        //Line pos stuff here
        System.out.println("Printing AST with line and position info to " + filename + ".astlines.dot");
        LinesPositionsVisitor lpVisitor = new LinesPositionsVisitor();
        ast_root.apply(lpVisitor);
		    java.io.PrintStream astout = new java.io.PrintStream( new java.io.FileOutputStream(filename + ".astlines.dot"));
		    ast_root.apply(new DotLinesVisitor(new PrintWriter(astout), lpVisitor.getLines()));

				TypeCheckVisitor typeCheckVisitor = new TypeCheckVisitor(symTable);

				try {
				  ast_root.apply(typeCheckVisitor);


        }
        catch(TypeCheckVisitor.TypeCheckException tce){
					System.exit(2);
        }
      }
      else
      {
          System.err.println("Errors found while building symbol table");
				  System.exit(3);
      }
			
       // For now, taking out the large majority of the code to just verify the correctness of the AST.
			// determine which parser to execute

			if ( args[0].equals("--two-pass-mips") || args[0].equals("--two-pass-mips-O")) {

						// generate MIPS code that evaluates the expression
						java.io.PrintStream mipsout =
						  new java.io.PrintStream(
						    new java.io.FileOutputStream(filename + ".s"));
						System.out.println("Printing MARS MIPS file to " + filename + ".s");
						PrintWriter pw = new PrintWriter(mipsout);
            MIPSgenVisitor mgv = null;
            if (args[0].equals("--two-pass-mips-O")) {
               //System.err.println("EC not implemented for this release");
               //System.exit(4);
						   System.out.println("Register allocation in use.");
						   System.out.println("Thrusters Engaged!");
				       mgv = new MIPSgenECVisitor(pw, symTable);
             } else {
						   mgv = new MIPSgenVisitor(pw, symTable);
             } 
						ast_root.apply(mgv);
						pw.close();     

			} else {
				System.out.print("Unknown command-line option ");
				System.out.println(args[0]);
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.exit(1);
		}  
	}

}
