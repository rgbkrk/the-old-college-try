/* This file was generated by SableCC (http://www.sablecc.org/). 
 * Then modified.
 */

package ast.node;

import ast.analysis.*;
import java.util.*;

@SuppressWarnings("nls")
public final class SpecialMain extends Node
{
    private final LinkedList<VarDecl> _varDecls_ = new LinkedList<VarDecl>();
    private final LinkedList<IStatement> _statements_ = new LinkedList<IStatement>();

    public SpecialMain()
    {
        // Constructor
    }

    public SpecialMain(
        @SuppressWarnings("hiding") List<VarDecl> _varDecls_,
        @SuppressWarnings("hiding") List<IStatement> _statements_)
     {
        // Constructor
        setVarDecls(_varDecls_);

        setStatements(_statements_);

    }

    @Override
    public Object clone()
    {
        return new SpecialMain(
             cloneList(this._varDecls_),
            cloneList(this._statements_));
    }

    public void apply(ISwitch sw)
    {
        ((IAnalysis) sw).caseSpecialMain(this);
    }

    public LinkedList<VarDecl> getVarDecls()
    {
        return this._varDecls_;
    }

    public void setVarDecls(List<VarDecl> list)
    {
        this._varDecls_.clear();
        this._varDecls_.addAll(list);
        for(VarDecl e : list)
        {
            if(e.parent() != null)
            {
                e.parent().removeChild(e);
            }

            e.parent(this);
        }
    }

    public LinkedList<IStatement> getStatements()
    {
        return this._statements_;
    }

    public void setStatements(List<IStatement> list)
    {
        this._statements_.clear();
        this._statements_.addAll(list);
        for(IStatement e : list)
        {
            if(e.parent() != null)
            {
                e.parent().removeChild(e);
            }

            e.parent(this);
        }
    }


    @Override
    public String toString()
    {
        return ""
            + toString(this._varDecls_)
            + toString(this._statements_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        if(this._varDecls_.remove(child))
        {
            return;
        }

        if(this._statements_.remove(child))
        {
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child

        for(ListIterator<VarDecl> i = this._varDecls_.listIterator(); i.hasNext();)
        {
            if(i.next() == oldChild)
            {
                if(newChild != null)
                {
                    i.set((VarDecl) newChild);
                    newChild.parent(this);
                    oldChild.parent(null);
                    return;
                }

                i.remove();
                oldChild.parent(null);
                return;
            }
        }

        for(ListIterator<IStatement> i = this._statements_.listIterator(); i.hasNext();)
        {
            if(i.next() == oldChild)
            {
                if(newChild != null)
                {
                    i.set((IStatement) newChild);
                    newChild.parent(this);
                    oldChild.parent(null);
                    return;
                }

                i.remove();
                oldChild.parent(null);
                return;
            }
        }

        throw new RuntimeException("Not a child.");
    }
}
