#!/bin/bash

rm TestCases/*.class
rm TestCases/*.java.*
java -jar MJ.jar --two-pass-mips-O $1
java -jar Mars.jar $1.s
javac $1
java -classpath TestCases/ `basename $1 .java`

