#!/bin/bash

if [ $# -lt 1 ];
then
  echo "Usage: $0 <java_file>"
  exit 1
fi

if [ "$1" == "--help" ];
then
  echo "Attempts to run the currently built MJ.jar on a java file, then create a .png file"
  echo "Usage: $0 <java_file>"
  exit 2
fi

java -jar MJ.jar --two-pass-mips $1
if [ -e "$1.ast.dot" ];
then
  echo "Attempting to create png file from $1.ast.dot"
  dot -Tpng $1.ast.dot -o $1.ast.dot.png
  test=`which eog`
  if [ "$?" -eq "0" ];
  then
    echo "RUN: "
    echo "eog $1.ast.dot.png"
  else
    echo "You don't have eog, so you'll have to open the file yourself..."
  fi
else
  echo "Oh noes! The dot file wasn't created"
fi


