class NoMethod {
  public static void main(String[] args) {
    System.out.println(new Internal().run());
  }
}

class Internal {
  public int noRun() {
    return 1;
  }
}
