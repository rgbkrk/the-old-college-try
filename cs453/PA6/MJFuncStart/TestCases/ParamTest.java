class ParamTest {
    public static void main(String[] whatever){
        System.out.println( new Foo().testing() );
    }
}

class Foo {

    public int testing() {
        boolean b;
        b = new Baz().lotsaParams(10, 20, 30);
        return 42;
    }
}


class Baz {
    public boolean lotsaParams(int x, int y, int z) {
        System.out.println(x);
        System.out.println(y);
        System.out.println(z);
        return true;
    }
}
       

