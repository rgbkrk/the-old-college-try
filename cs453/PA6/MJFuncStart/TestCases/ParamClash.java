class ParamClash {
    public static void main(String[] whatever){
        System.out.println( new Foo().testing() );
    }
}

class Foo {

    public int testing() {
    
        int b;
        int a;
        
        a = 56;
        b = 20;
        System.out.println(a + b * 2);
        System.out.println(2 * (6 - 1) + 2);
        System.out.println(new Bar().another(100));
        if (new Baz().lotsaParams(10, 20, 30)) {
            System.out.println(1);    
        } else {
            System.out.println(0);
        }
        System.out.println(b);
        return 42;
    }
}

class Bar {
    public int another(int x) {
        return 2 + x;
    }

}

class Baz {
    public boolean lotsaParams(int x, int q, int r) {
        int b;
        b = 3;
        System.out.println(x-q-r+b);
        return true;
    }
}
       

