class TooFewArgs {
  public static void main (String[] args) {
    System.out.println(new Internal().run(1));
  }
}
  class Internal {
    public int run(int i, int j) {
      return 1;
    }
  }
