#!/bin/bash

FILES=*.txt
for f in $FILES
do
  BASE=`basename $f .txt`
  BASE="OLD$BASE"
  NEWFILE="$BASE.java"
  echo "class $BASE {" >> $NEWFILE
  echo "  public static void main(String[] whatev){" >> $NEWFILE
  echo "    System.out.println(new Foo$BASE().oldTest() );" >> $NEWFILE
  echo "  }" >> $NEWFILE
  echo "}" >> $NEWFILE
  echo "" >> $NEWFILE
  echo "class Foo$BASE {" >> $NEWFILE
  echo "" >> $NEWFILE
  echo "  public int oldTest() {" >> $NEWFILE
  sed 's/special\s*main/ /' $f >> $NEWFILE
  echo "    return 1;" >> $NEWFILE
  echo "  }" >> $NEWFILE
  echo "}" >> $NEWFILE
done



