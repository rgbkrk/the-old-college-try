class InvalidConditionWhile {
  public static void main (String [] args){
    System.out.println(new Internal().run());
  }
}

class Internal{
  public int run() {
    int i;
    while(i == true) {
      i++;
    }
    return i;
  }
}
