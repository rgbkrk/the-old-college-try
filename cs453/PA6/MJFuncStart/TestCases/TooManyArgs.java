class TooManyArgs {
  public static void main (String[] args) {
    System.out.println(new Internal().run(1, 2));
  }
}
  class Internal {
    public int run(int i) {
      return 1;
    }
  }
