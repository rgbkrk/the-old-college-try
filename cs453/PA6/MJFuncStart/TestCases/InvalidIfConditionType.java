class InvalidReturn {
  public static void main (String[] args) {
    System.out.println(new Internal().run());
  }
}
  class Internal {
    public int run() {
      int i;
      if (3) i = 1;
      else i= 2;
      return i;
    }
  }
