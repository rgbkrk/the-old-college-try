#!/bin/bash
## Kyle Kelley, Paul Gagliardi, David Newman

function testFile()
{
  OPTION=$1;
  f=$2;
  RUNNER=PA6_cs453h.jar
  #echo ""
  echo "Testing $f"

  #### Two-pass MIPS testing ####
  #echo "Two-pass MIPS test $f"
  #echo "javac $f > /dev/null 2> $f.javerr"
  javac $f > /dev/null 2> $f.expec.err
  failCheckOracle=$?
  if [ $failCheckOracle == 0 ]
  then
    name=`basename $f .java`
    #echo "java -classpath TestCases/ ${name%.java} > $f.s.eout"
    java -classpath TestCases/ ${name%.java} > $f.s.eout
  fi

  java -jar $RUNNER $OPTION $f > $f.ours.out 2> $f.ours.err
  failCheckRunner=$?
  if [ $failCheckRunner == 0 ]
  then
    dot -Tpng $f.ast.dot -o $f.ast.dot.png
    dot -Tpng $f.ST.dot -o $f.ST.dot.png
    java -jar Mars.jar nc $f.s > $f.s.out
  else
    echo $f.ours.err
    cat $f.ours.err

    errors="`cat $f.ours.err | grep "^\\["`"

    echo $f >> logfile
    echo "" >> logfile
    cat $f.ours.err >> logfile
    echo "" >> logfile

    lines=`cat $f.ours.err | grep "^\\[" | cut -d"[" -f2 | cut -d"," -f1`
    for line in $lines
    do
      OFFENDER=`sed -n -e ${line}'p' $f`
      echo ">>> $OFFENDER" >> logfile
    done

    echo "" >> logfile
    cat $f >> logfile
    echo "" >> logfile
    echo "************============************" >> logfile
    echo "" >> logfile
    
  fi



  if [ $failCheckOracle -eq 0 -a $failCheckRunner -eq 0 ]
  then
    echo ""
  else
    if [ $failCheckOracle -ne 0 -a $failCheckRunner -ne 0 ]
    then
      echo ""
    else
      echo ">> Return codes are not both of either zero or non-zero fashion"
      echo $failCheckOracle
      echo $failCheckRunner
      return 42
    fi
  fi

  #echo "diff -b $f.ours.err $f.expec.err"
  #diff -b $f.ours.err $f.expec.err

  #echo "diff -b $f.ours.ast.dot $f.expec.ast.dot"
  if [ $failCheckOracle -eq 0 -o $failCheckRunner -eq 0 ]
  then
    diff -B $f.s.out $f.s.eout #-B ignores extra blank lines
    ret=$?
    if [ $ret -ne 0 ]
    then
      return $ret
    fi
  fi
  return 0
}

###
### Real testing begins here.
###

#We must retain sanity
#make clean

#Ahhhhh
#make

#Wonderful

FILES=./TestCases/*.java
OPTION=--two-pass-mips
if [ $# -ne "0" ]
then
  if [ $1 = "-O" ]
  then
    OPTION=--two-pass-mips-O
    shift 1
    if [ $# -ge "1" ]
    then
      FILES=$@
    fi
  else
      FILES=$@
  fi
fi

let good=0
let bad=0
failures=""

rm logfile

for f in $FILES
do
  testFile $OPTION $f
  let val="$?"
  if [ $val -eq 0 ]
  then
    let good++
  else
    let bad++
    failures="$f $failures"
  fi
done

echo "$good passed"
echo "$bad failed"

echo ""

if [ $bad -ne 0 ]
then
  echo "The failures: "
  echo $failures
fi
