#############
# main_sum5
#
# Example main routine that calls sum5.
#############

    .text
    .globl main
main:
    # There is no need to store $ra and $fp
    # in main for MARS because execution always
    # starts at the top of the input .s file
    # and $ra and $fp always have initial 
    # values of 0.


    addi    $sp, $sp, -4    # PUSH param 5 onto stack
    li      $t0, 5          #
    sw      $t0, 0($sp)     #
    
    addi    $sp, $sp, -4    # PUSH param 4 onto stack
    li      $t0, 4          #
    sw      $t0, 0($sp)     #

    addi    $sp, $sp, -4    # PUSH param 3 onto stack
    li      $t0, 3          #
    sw      $t0, 0($sp)     #

    addi    $sp, $sp, -4    # PUSH param 2 onto stack
    li      $t0, 2          #
    sw      $t0, 0($sp)     #

    addi    $sp, $sp, -4    # PUSH param 1 onto stack
    li      $t0, 1          #
    sw      $t0, 0($sp)     #
    

    jal     _sum5 
    lw      $t0, 0($sp)     # $t0 = sum return value
    addi    $sp, $sp, 4     # pop sum return value
                       
    # There are some obvious optimizations possible
    # here, however, we are treating all function calls
    # as if they are occuring all by themselves.
    addi    $sp, $sp, -4    # PUSH parameter 1
    sw      $t0, 0($sp)     #

    jal     _printint       # print return value from sum

exit_main:
    # HALT the MARS simulation.
    li      $v0, 10
    syscall


########
# sum5.s
#
# int sum5(int p1,int p2,int p3,int p4,int p5)
#
# Sums 5 input parameters and returns the result on
# the top of the stack.
#
# Expects the parameters to be passed in reverse
# order on the run-time stack (RTS).
# Upon entry to this function, the RTS should look like
# the following:
#       |     |
# $sp ->|  p1 |
#       |  p2 |
#       |  p3 |
#       |  p4 |
#       |  p5 |
#       | ... |
#
# The stack pointer should be pointing at the top value in
# the stack and that value should be the first parameter to sum5.
#
# The sum5 prologue will set up the RTS as follows:
# $fp ->|         |
# $sp ->| old $fp |
#       | old $ra |
#       | ret     |
#       |  p1     |
#       |  p2     |
#       |  p3     |
#       |  p4     |
#       |  p5     |
#       | ...     |
#
# where ret is the slot for placing
# the return value.
# sum5 does not need any RTS storage for local variables, 
# so the frame pointer does not end up pointing at anything
# useful, but it is as always pointing one slot above the old
# frame pointer.
########

    .text
    .globl _sum5
_sum5:
    #### prologue
    addi    $sp, $sp, -4    # make space for retval

    addi    $sp, $sp, -4    # push return address
    sw      $ra, 0($sp)     #
            
    addi    $sp, $sp, -4    # push frame pointer
    sw      $fp, 0($sp)     #    
    
    addi    $fp, $sp, -4    # set up new frame pointer

    #### body
    lw      $t0, 16($fp)    # p1
    lw      $t1, 20($fp)    # p2
    add     $t0,$t0,$t1     # $t0 = p1 + p2
    lw      $t1, 24($fp)    # p3
    add     $t0,$t0,$t1     # $t0 = $t0 + p3
    lw      $t1, 28($fp)    # p4
    add     $t0,$t0,$t1     # $t0 = $t0 + p4
    lw      $t1, 32($fp)    # p5
    add     $t0,$t0,$t1     # $t0 = $t0 + p5

    sw      $t0, 12($fp)    # return result
    
    #### epilogue
exit_sum5:
    lw      $fp, 0($sp)     # pop frame pointer
    addi    $sp, $sp, 4     #
            
    lw      $ra, 0($sp)     # pop return address
    addi    $sp, $sp, 4     #
            
    jr      $ra



