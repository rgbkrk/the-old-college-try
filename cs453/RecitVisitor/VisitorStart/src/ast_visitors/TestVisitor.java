/*
 * ExprEvalDriver.java
 *
 * usage: 
 *   java ExprEval [--one-pass-eval | --two-pass-eval | --two-pass-mips ] infile
 *
 * This driver calls one of three possible expression evaluation parsers.
 * The default parser is the one-pass-eval.
 *
 */

package ast_visitors;

import ast.analysis.DepthFirstAdapter;
import ast.node.IntegerExp;
import ast.node.MinusExp;
import ast.node.MulExp;
import ast.node.Node;
import ast.node.PlusExp;


public class TestVisitor extends DepthFirstAdapter {
	private Node root;
	private String pn;
	private String rpn;

	public TestVisitor(Node r){
		this.root=r;
		this.pn = "";
		this.rpn = "";
	}

	/**
	 * Override the default action taken at each visited node -- print out dot
	 * node with parse tree node's text as a label. Also print out edge from
	 * parent to this node.
	 */
	public void defaultCase(Node node) {
		
	}
	public void defaultIn(Node node) {
		System.out.println("Get in a node.");
	}

	public void defaultOut(Node node) {
		System.out.println("Get out of a node.");
		if(node == root)
		{
			System.out.println("PN: " + pn);
			System.out.println("RPN: " + rpn);
		}
	}
	
	public void inIntegerExp(IntegerExp node){
		pn = pn + " " + node.getLiteral();
		defaultIn(node);
	}

	public void outIntegerExp(IntegerExp node){
		rpn = rpn + " " + node.getLiteral();
		defaultOut(node);
	}

	public void outPlusExp(PlusExp node){
		rpn = rpn + " +";
		defaultOut(node);
	}
	
	public void outMinusExp(MinusExp node){
		rpn = rpn + " -";
		defaultOut(node);
	}
	
	public void outMulExp(MulExp node){
		rpn = rpn + " *";
		defaultOut(node);
	}
	public void inPlusExp(PlusExp node){
		pn = pn + " +";
		defaultIn(node);
	}
	
	public void inMinusExp(MinusExp node){
		pn = pn + " -";
		defaultIn(node);
	}
	
	public void inMulExp(MulExp node){
		pn = pn + " *";
		defaultIn(node);
	}
}
