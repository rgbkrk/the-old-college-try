class ClassRef {
    public static void main(String[] a){
        System.out.println(new Foo().testing());
    }
}

class Foo {
    Bar b;
    public int testing() {  
        int a;
        b = new Bar();
        a = b.changeY(7);
        return b.getY() + a;
    }
}

class Bar {
    boolean x;
    int y;
    public int changeY(int p) {
        y = p;
        return this.getY();
    }
    
    public int getY() {
        return y;
    }
}
