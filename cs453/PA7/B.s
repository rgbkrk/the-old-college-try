    .text

main: .text
      move $fp, $sp
B_makeB:
    # space for retval
    addi    $sp, $sp, -4

    addi    $sp, $sp, -4
    sw      $ra, 0($sp)
    addi    $sp, $sp, -4
    sw      $fp, 0($sp)

    # set up frame pointer
    addi    $fp, $sp, -4
    # space for local variables
    addi    $sp, $sp, 0

    # IdExp
    # load value for p 
    lw      $t0, 20($fp)
    addi    $sp, $sp, -4
    sw      $t0, 0($sp)

    # IdExp
    # member load
    # implicit this
    lw $t0, 16($fp)
    # get this.y
    lw $t1, 0($t0)
    addi $sp, $sp, -4
    sw $t1, 0($sp)

    # PlusExp
    lw $t1, 0($sp)
    addi $sp, $sp, 4
    lw $t0, 0($sp)
    addi $sp, $sp, 4
    add $t2, $t0, $t1
    addi $sp, $sp, -4
    sw $t2, 0($sp)

    # AssignExp
    lw $t1, 0($sp)
    addi $sp, $sp, 4
    # load implicit this
    lw $t0, 16($fp)
    # store to member y
    sw $t1, 0($t0)

    lw $t0, 16($fp)
    addi $sp, $sp, -4
    sw $t0, 0($sp)
    lw $t0, 0($sp)
    addi $sp, $sp, 4

    # deallocating space for local vars
    addi    $sp, $sp, 0
    lw      $fp, 0($sp)
    addi    $sp, $sp, 4
    lw      $ra, 0($sp)
    addi    $sp, $sp, 4
    # set the return value
    sw    $t0, 0($sp)
    jr      $ra



