class Recursion {
  public static void main(String[] whatever) {
    System.out.println(new Foo().recurseMe(10));
  }
}

class Foo {

  public int recurseMe(int i) {
    int r;
    System.out.println(i);
    if((i-1) < 1) { r = 0; }
    else {r = this.recurseMe(i-1);}
    return r;
  }
}
