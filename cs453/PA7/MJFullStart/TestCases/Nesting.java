class Nesting {
  public static void main(String[] args){
    if((new A().setNum(3)).t(3).b(new A().setNum(11))){
    }
    else{
    }
  }
}

class A{
  int num;
  public A t(int i){
    A a;
    System.out.println(i*num);
    a = new A();
    a = a.setNum(i*5);
    return a;
  }

  public A setNum(int i){
    System.out.println(i);
    num = i;
    System.out.println(num);
    return this;
  }

  public int getNum(){
    return num;
  }

  public boolean b(A a){
    int i;
    i = a.getNum();
    System.out.println(i);
    System.out.println(num);

    return i <3 && num <3 ;
  }

}


