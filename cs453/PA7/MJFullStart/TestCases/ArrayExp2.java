class ArrayExp2 {
    public static void main(String[] a){
        System.out.println(new MyClass().testing());
    }
}

class MyClass {
    public int testing() {
        int [] x;
        x = new int [7];
        x[0] = 42;
        x[1] = 6;
        x[6] = 21;
        return x[0] + x[x[1]];
    }

}
