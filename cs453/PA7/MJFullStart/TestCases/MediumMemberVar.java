class MediumMemberVar {
    public static void main(String[] deNada){
        System.out.println( new Other().joe() );
    }
}

class Inner {
  int a;

    public int testing(int fred) {
      a = fred;
      return a;
    }
}

class Other {
  public int joe() {
    Inner a;
    int b;
    a = new Inner();
    b = a.testing(42);
    System.out.println(a.testing(42));
    return b;
  }
}
