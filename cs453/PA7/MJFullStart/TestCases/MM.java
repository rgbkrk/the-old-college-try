class MM{
  public static void main(String[] args)
  {
    if((new Driver()).test()){}
    else{}
  }
}

class Driver{
  public boolean test(){
    Matrix Q;
    Matrix R;
    Matrix QR;
    PRNG random;
    int junk;
    int i;
    
    Q = new Matrix();
    Q = Q.init(5,3);

    R = new Matrix();
    R = R.init(3,7);

    //junk = Q.printLinearly();
    //junk = R.printLinearly();
    random = new PRNG();
    junk = random.seedIt(42);
    i = 0;
    while(i < 30){
      junk = random.next();
      System.out.println(junk);
      i = i + 1;
    }

    Q = Q.randomize(random);
    R = R.randomize(random);
    
    QR = Q.multiply(Q,R);
    
    junk = QR.printLinearly();


    return true;
  }
}

class Matrix{
  int[] M;
  int m;
  int n;

  public Matrix init(int numRows, int numCols){
    m = numRows;
    n = numCols;
    M = new int[m*n];
    return this;
  }

  public int get(int i, int j){
    return M[i*n + j];
  }

  public int set(int i, int j, int value){
    M[i*n + j] = value;
    return M[i*n + j];
  }

  public int getNumRows(){
    return m;
  }

  public int getNumCols(){
    return n;
  }

  public int printLinearly(){
    int i;
    i = 0;
    while(i < n*m){
      System.out.println(M[i]);
      i = i + 1;
    }
    return i;
  }

  public Matrix multiply(Matrix M1, Matrix M2){
    Matrix Mm;
    int myM;
    int myN;
    int myP;
    int i;
    int j;
    int r;
    int sum;
    int junk;
    Mm = new Matrix();
    Mm = Mm.init(M1.getNumRows(), M2.getNumCols());

    myM = M1.getNumRows();
    myN = M1.getNumCols();
    myP = M2.getNumCols();

    i = 0;
    j = 0;
    sum = 0;
    //Think m, n, p
    while(i < myM){
      j = 0;
      while(j < myP){
        r = 0;
        while(r < myN){
          sum = sum + M1.get(i,r)*M2.get(r,j);
          r = r + 1;
        }
        junk = Mm.set(i,j, sum);
        j = j + 1;
        sum = 0;
      }
      i = i + 1;
    }

    return Mm;
  }

  public Matrix randomize(PRNG rando){
    int i;
    i = 0;
    while(i < n*m){
      M[i] = rando.next();
      i = i + 1;
    }
    return this;
  }
}

class PRNG{
  int current;
  Divisor math;
  boolean initialized;

  public int seedIt(int seed){
    current = seed;
    math = new Divisor();
    initialized = true;
    return current;
  }

  public int next(){
    int retval;
    if(!initialized){
      current = 3;
      math = new Divisor();
      initialized = true;
    }
    else{}

    retval = current;
    current = math.remainder((current*617 + 229),1013);
    return retval;
  }
}

class Divisor
{
  public int remainder(int dividend, int divisor){
    int curr;

    int remain;
    int quot;

    curr = 0;
    remain = 0;
    quot = 0;
    if(! (divisor < 0) && ! (0 < divisor))
    {

    }
    else
    {
      curr = dividend;
      while(! (curr < 0)) {
        curr = curr - divisor;
        if(curr < 0){}
        else
          quot = quot + 1;
      }
      remain = curr + divisor;
    }
    return remain;
  }

  /*public int quotient(int dividend, int divisor){
    int curr;

    int remain;
    int quot;

    curr = 0;
    remain = 0;
    quot = 0;
    if(! (divisor < 0) && ! (0 < divisor))
    {

    }
    else
    {
      curr = dividend;
      while(! (curr < 0)) {
        curr = curr - divisor;
        if(curr < 0){}
        else
          quot = quot + 1;
      }
      remain = curr + divisor;
    }
    return quot;
  }*/

}
