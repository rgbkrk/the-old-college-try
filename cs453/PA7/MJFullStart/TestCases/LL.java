class LL {
  public static void main (String[] args) {
    if(new Driver().run() <3){}
    else{}
  }
}

class Driver {
  public int run() {
    //Junk is used to toss off the return values on assignment.
    boolean junk;
    LinkedList ll;
    ll = new LinkedList();
    junk = ll.init();
    junk = ll.add(3);
    junk = ll.add(4);
    junk = ll.add(1);
    junk = ll.add(6);
    junk = ll.add(323);
    junk = ll.add(8008);
    junk = ll.traversal();
    return 0;
  }
}

class LinkedList {
  Node head;
  public boolean init(){
    boolean bjunk;
    head = new Node();
    bjunk = head.setEnd(true);
    return true;
  }

  public boolean add(int i){
    boolean bJunk;
    int iJunk;
    Node nJunk;
    Node nNode;
    nNode = new Node();
    bJunk = nNode.setEnd(false);
    nJunk = nNode.setNext(head);
    iJunk = nNode.setData(i);
    head = nNode;
    return true;
  }

  public boolean traversal(){
    Node curr;
    curr = head;
    while(!curr.getEnd()){
      System.out.println(curr.getData());
      curr = curr.getNext();
    }
    return true;
  }
}

class Node {
  Node next;
  boolean end;
  int data;

  public boolean setEnd(boolean e){
    end = e;
    return end;
  }

  public boolean getEnd(){
    return end;
  }

  public Node getNext(){
    return next;
  }

  public Node setNext(Node n){
    next = n;
    return next;
  }

  public int getData(){
    return data;
  }

  public int setData(int d){
    data = d;
    return data;
  }

}
