class ArrayArgs {
    public static void main(String[] deNada){
        System.out.println( new Inner().testing() );
    }
}
// Testing array syntax for PA7!
// Now I get to use comments. Woohoo!
class Inner {

  public int[] makeArray(int size) {
    return new int[size];
  }

  public int setArrayElt(int[] array, int location, int value) {
    array[location] = value;
    return 1;
  }

    public int testing() {
      int[] a;
      int junk;
      a = this.makeArray(2);
      junk = this.setArrayElt(a,0,0);
      junk = this.setArrayElt(a,1,1);
      System.out.println(a[0]+1);
      System.out.println(a[1]+1);
      return a[1]+a[1]+1;
    }
}
