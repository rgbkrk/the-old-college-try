class Formals {
  public static void main(String[] lalala){{
    System.out.println( new A().driveInt(1,2) );
    System.out.println( new A().driveInt(2,1) );
    System.out.println( new A().driveInt(2,2) );
    System.out.println( new A().driveInt(1,1) );
    System.out.println( new A().driveInt(0-98,342) );
    System.out.println( new A().driveInt(342,0-98) );


    System.out.println( new A().driveBool(true,false));
    System.out.println( new A().driveBool(false,false));
    System.out.println( new A().driveBool(true,true));
    System.out.println( new A().driveBool(false,true));
  }}
}

class A {
  public int driveInt(int a, int b){
    if(new Cmp().lt(a,b)){
      System.out.println(1);
    }
    else
    {
      System.out.println(0);
    }

    if(new Cmp().gt(a,b)){
      System.out.println(1);
    }
    else
    {
      System.out.println(0);
    }
    if(new Cmp().lte(a,b)){
      System.out.println(1);
    }
    else
    {
      System.out.println(0);
    }

    if(new Cmp().gte(a,b)){
      System.out.println(1);
    }
    else
    {
      System.out.println(0);
    }

    if(new Cmp().eq(a,b)){
      System.out.println(1);
    }
    else
    {
      System.out.println(0);
    }

    if(new Cmp().neq(a,b)){
      System.out.println(1);
    }
    else
    {
      System.out.println(0);
    }

    return 171;
  }
  public int driveBool(boolean a, boolean b) {
    if(new BoolOps().and(a,b)){
      System.out.println(1);
    }
    else{
      System.out.println(0);
    }

    if(new BoolOps().or(a,b)){
      System.out.println(1);
    }
    else{
      System.out.println(0);
    }

    if(new BoolOps().xor(a,b)){
      System.out.println(1);
    }
    else{
      System.out.println(0);
    }


    return 8001;
  }
}

class BoolOps {
  public boolean and(boolean left, boolean right) {
    return left && right;
  }

  public boolean or(boolean left, boolean right) {
    return !(! left && ! right);
  }

  public boolean xor(boolean left, boolean right) {
    return (new BoolOps().or(left,right)) && !( left && right );
  }

}

class Cmp {
  public boolean lt(int left, int right) {
    return left < right;
  }

  public boolean gt(int left, int right) {
    return !(left < right) && (new Cmp().neq(left,right));
  }

  public boolean lte(int left, int right) {
    return !(left < right) && (new Cmp().gt(left,right)) && left < right;
  }

  public boolean gte(int left, int right) {
    return !(left < right);
  }

  public boolean eq(int left, int right) {
    boolean retval;
    retval = false;
    if(left < right){
    }
    else{
      retval = (left - right - 1) < 0;
    }
    return retval;
  }

  public boolean neq(int left, int right) {
    return !(new Cmp().eq(left,right));
  }

}

class Empty {
}
