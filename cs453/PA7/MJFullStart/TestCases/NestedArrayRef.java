class NestedArrayRef {
    public static void main(String[] deNada){
        System.out.println( new Inner().testing() );
    }
}
// Testing array syntax for PA7!
// Now I get to use comments. Woohoo!
class Inner {

    public int testing() {
      int[] a;
      int[] b;
      a = new int[2];
      a[0] = 1;
      a[1] = 2;
      b = new int[a[1]];
      System.out.println(a[1]+a[a[1]-1]);
      System.out.println(a.length);
      return a[0];
    }
}
