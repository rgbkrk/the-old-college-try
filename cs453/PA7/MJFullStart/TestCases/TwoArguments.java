class TwoArguments {
  public static void main (String[] args) {
    System.out.println(new Internal().run(1, true));
  }
}
  class Internal {
    public int run(int a, boolean b) {
      return 1;
    }
  }
