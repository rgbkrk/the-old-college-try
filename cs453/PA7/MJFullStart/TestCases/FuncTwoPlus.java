class FuncTwoPlus {
    public static void main(String[] whatever){
        System.out.println( new Foo().testing() );
    }
}

class Foo {

    public int testing() {
    
        int b;
        int a;
        
        a = 56;
        b = 20;
        System.out.println(a + b * 2);
        System.out.println(2 * (6 - 1) + 2);
        System.out.println(this.another(100));
        if (this.lotsaParams(10, 20, 30)) {
            System.out.println(1);    
        } else {
            System.out.println(0);
        }

        return 42;
    }
        
    public int another(int x) {
        return 2 + x;
    }

    public boolean lotsaParams(int p, int q, int r) {
        System.out.println(p-q-r);
        return true;
    }
}