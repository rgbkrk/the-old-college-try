class InClass2 {
    public static void main(String[] whatever){

        System.out.println( new Foo().testing() );
    }
}

class Foo {

  public int testing() {
    int a;
    int b;
    int c;
    int d;
    int e;
    int f;
    a = 1;
    b = 2;
    c = 3;
    d = 4;
    e = 5;
    f = 6;
    
    if (a<b && c<d && e<f) {
        a = 7;
    } else {
        a = 42;
    }
    return 42;
  }
}