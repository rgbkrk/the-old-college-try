class Ack{
  public static void main(String[] args){
    {
      System.out.println(new Ackermann().a(3,4));
      System.out.println(new Ackermann().a(1,4));
      System.out.println(new Ackermann().a(3,6));
    }
  }
}

class Ackermann{
  public int a(int m, int n){
    int retval;
    retval = n+1;
    if(0 < m && 0 < n)
      retval = (new Ackermann().a(m-1,new Ackermann().a(m,n-1)));
    else if(0 < m)
      retval = (new Ackermann().a(m-1,1));
    else
      retval = n + 1;
    return retval;
  }
}
