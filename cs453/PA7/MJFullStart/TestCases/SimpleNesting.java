class SimpleNesting {
  public static void main(String[] args){
    if(((new A()).t(7)).getNum() <3 ){
    }
    else{
    }
  }
}

class A{
  int num;
  public A t(int i){
    A a;
    System.out.println(i*num);
    a = new A();
    a = a.setNum(i*5);
    return a;
  }

  public A setNum(int i){
    System.out.println(i);
    num = i;
    System.out.println(num);
    return this;
  }

  public int getNum(){
    return num;
  }

  public boolean b(int a){
    int i;
    i = a;
    System.out.println(i);
    System.out.println(a);

    return i <3 && a <3 ;
  }

}


