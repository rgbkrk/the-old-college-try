/* This file was generated by SableCC (http://www.sablecc.org/). 
 * Then modified.
 *
 * Modified by DVN for CS453 Spring 2010. PA7.
 */

package ast.node;

import ast.analysis.*;

@SuppressWarnings("nls")
public final class ThisExp extends IExp
{
  // To carry line and position information for this node.
  // Added 4/29/2010 DVN
  private Token _token_;

    public ThisExp(Token _token_)
    {
        // Constructor
      setId(_token_);// Added: DVN
    }

    public Token getId() { // Added: DVN
      return this._token_;
    }

    public void setId(Token node) // Added: DVN based on IdExp
    {
        if(this._token_ != null)
        {
            this._token_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._token_ = node;
    }


    @Override
    public Object clone()
    {
        return new IdExp(
            cloneNode(this._token_));
    }

    public void apply(ISwitch sw)
    {
        ((IAnalysis) sw).caseThisExp(this);
    }

    @Override
    public String toString()
    {
        return "";
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        throw new RuntimeException("Not a child.");
    }
}
