/* This file was generated by SableCC (http://www.sablecc.org/).  
 * Then modified.
 */
package ast.node;

import ast.analysis.*;

@SuppressWarnings("nls")
public final class ArrayExp extends IExp
{
    private IExp _exp_;
    private IExp _index_;

    public ArrayExp()
    {
        // Constructor
    }

    public ArrayExp(
        @SuppressWarnings("hiding") IExp _exp_,
        @SuppressWarnings("hiding") IExp _index_)
    {
        // Constructor
        setExp(_exp_);

        setIndex(_index_);

    }

    @Override
    public Object clone()
    {
        return new ArrayExp(
            cloneNode(this._exp_),
            cloneNode(this._index_));
    }

    public void apply(ISwitch sw)
    {
        ((IAnalysis) sw).caseArrayExp(this);
    }

    public IExp getExp()
    {
        return this._exp_;
    }

    public void setExp(IExp node)
    {
        if(this._exp_ != null)
        {
            this._exp_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._exp_ = node;
    }

    public IExp getIndex()
    {
        return this._index_;
    }

    public void setIndex(IExp node)
    {
        if(this._index_ != null)
        {
            this._index_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._index_ = node;
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._exp_)
            + toString(this._index_);
    }

    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        if(this._exp_ == child)
        {
            this._exp_ = null;
            return;
        }

        if(this._index_ == child)
        {
            this._index_ = null;
            return;
        }

        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        if(this._exp_ == oldChild)
        {
            setExp((IExp) newChild);
            return;
        }

        if(this._index_ == oldChild)
        {
            setIndex((IExp) newChild);
            return;
        }

        throw new RuntimeException("Not a child.");
    }
}
