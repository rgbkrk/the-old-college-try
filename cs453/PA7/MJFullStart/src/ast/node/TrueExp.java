/* This file was generated by SableCC (http://www.sablecc.org/). 
 * Then modified.
 */

package ast.node;

import ast.analysis.*;

@SuppressWarnings("nls")
public final class TrueExp extends IExp
{
    private Token _literal_;

    public TrueExp(
    @SuppressWarnings("hiding") Token _literal_)
    {
        // Constructor
        setLiteral(_literal_);
    }

    @Override
    public Object clone()
    {
        return new TrueExp(_literal_);
    }

    public void apply(ISwitch sw)
    {
        ((IAnalysis) sw).caseTrueExp(this);
    }

    public Token getLiteral()
    {
        return this._literal_;
    }

    public void setLiteral(Token node)
    {
        if(this._literal_ != null)
        {
            this._literal_.parent(null);
        }

        if(node != null)
        {
            if(node.parent() != null)
            {
                node.parent().removeChild(node);
            }

            node.parent(this);
        }

        this._literal_ = node;
    }

    @Override
    public String toString()
    {
        return ""
            + toString(this._literal_);
    }
    @Override
    void removeChild(@SuppressWarnings("unused") Node child)
    {
        // Remove child
        throw new RuntimeException("Not a child.");
    }

    @Override
    void replaceChild(@SuppressWarnings("unused") Node oldChild, @SuppressWarnings("unused") Node newChild)
    {
        // Replace child
        throw new RuntimeException("Not a child.");
    }
}


