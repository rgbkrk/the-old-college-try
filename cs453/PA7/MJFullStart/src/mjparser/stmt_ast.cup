/**
 * CS 453h
 * Kyle Kelley, Dave Newman, Paul Gagliardi
 *
 * NEW AND IMPROVED, NOW WITH MINTY FRESH COMMENTS!
 */
package mjparser;

import java_cup.runtime.*;
import java.util.LinkedList;
import ast.node.*;


parser code {:
    public void unrecovered_syntax_error(Symbol cur_token) {
        report_fatal_error("Fatal syntax error", cur_token);
    }

    public void report_fatal_error(String message, Object info) {
        report_error(message, info);
        done_parsing();
        Symbol token = (Symbol)info;
        mjparser.TokenValue tok = (mjparser.TokenValue)token.value;
        throw new mjparser.ParseException("Fatal parsing error",
                                          tok.line, tok.pos);
    }

    public void report_error(String message, Object info) {
        Symbol token = (Symbol)info;
        if(token.value instanceof Token) {
        Token tok = (Token)token.value;
        System.err.println("[" + tok.getLine() + "," + tok.getPos() + "] "
                               + message + " at " + tok.toString() );
        }
        else if(token.value instanceof mjparser.TokenValue){
        mjparser.TokenValue tok = (mjparser.TokenValue)token.value;
        System.err.println("[" + tok.line + "," + tok.pos + "] "
                               + message + " at " + tok.toString() );
                               }
    }

    public static void main(String args[]) throws Exception {
        new stmt_ast(new Yylex(System.in)).parse();
    }
:}

terminal PLUS, MINUS, TIMES, EQUALS, LESSTHAN;
terminal NOT, AND;
terminal INT, BOOL, STRING, VOID;
terminal CLASS, PUBLIC, STATIC, EXTENDS, NEW;
terminal SYSO, MAIN, LENGTH;
terminal SEMICOLON, DOT, COMMA;
terminal LPAREN, RPAREN, LBRACE, RBRACE, LBRACK, RBRACK;
terminal IF, ELSE, WHILE, RETURN;
terminal Token ID;
terminal TokenValue THIS;
terminal TokenValue NUMBER, TRUE, FALSE;

non terminal Program goal;
non terminal VarDecl var_decl;
non terminal LinkedList<VarDecl> var_decl_list;
non terminal IExp expr;
non terminal IStatement stmt;
non terminal IStatement block_stmt;
non terminal LinkedList<IStatement> stmt_list;
non terminal IType type;
non terminal MainClass mainClass;
non terminal TopClassDecl top_class_decl;
non terminal ChildClassDecl child_class_decl;
non terminal IClassDecl class_decl;
non terminal LinkedList<IClassDecl> class_decl_list;
non terminal MethodDecl method_decl;
non terminal LinkedList<MethodDecl> method_decl_list;
non terminal Formal formal;
non terminal LinkedList<Formal> formal_decl_list;
non terminal LinkedList<Formal> formal_decl_list_prime;
non terminal LinkedList<IExp> expr_list;
non terminal LinkedList<IExp> expr_list_prime;

precedence left AND;
precedence left LESSTHAN;

precedence left PLUS, MINUS;
precedence left TIMES;

precedence right NOT;
precedence right NEW, THIS;
precedence left DOT;
precedence left LENGTH;
precedence left LBRACK;


/* 
 * Remember that the production for the root
 * node must be first in the cup file OR the
 * root must be specified!
 */

goal      ::= mainClass:m class_decl_list:c
              {: RESULT = new Program(m,c); :};


mainClass ::= CLASS ID:className LBRACE PUBLIC STATIC VOID MAIN LPAREN STRING LBRACK RBRACK ID:arg RPAREN LBRACE stmt:s RBRACE RBRACE
              {: RESULT = new MainClass(className, arg, s); :};

top_class_decl ::= CLASS ID:className LBRACE var_decl_list:vl method_decl_list:ml RBRACE
              {: RESULT = new TopClassDecl(className, vl, ml); :};

child_class_decl ::= CLASS ID:className EXTENDS ID:parent LBRACE var_decl_list:vl method_decl_list:ml RBRACE
              {: RESULT = new ChildClassDecl(className, parent, vl, ml); :};

class_decl ::= top_class_decl:tcd
               {: RESULT = tcd; :}

              | child_class_decl:ccd
               {: RESULT = ccd; :};

class_decl_list ::= /* epsilon */
                {: RESULT = new LinkedList<IClassDecl>(); :}

               | class_decl:cd class_decl_list:cdl
                {: cdl.add(0,cd); RESULT = cdl; :}

               ;

method_decl ::= PUBLIC type:t ID:methodName LPAREN formal_decl_list:flist RPAREN LBRACE var_decl_list:vl stmt_list:sl RETURN expr:e SEMICOLON RBRACE
              {: RESULT = new MethodDecl(t,methodName,flist,vl,sl,e); :}
              ;

method_decl_list ::= /* lambda */
                   {: RESULT = new LinkedList<MethodDecl>(); :}

                  | method_decl:md method_decl_list:mdl
                   {: mdl.add(0,md); RESULT = mdl; :}

                  ;

formal    ::= type:t ID:id
            {:  RESULT = new Formal(t,new Token(id.getText(), id.getLine(), id.getPos())); :}
            ;

formal_decl_list ::= /* lambda */
                  {: RESULT = new LinkedList<Formal>(); :}

                  | formal:f formal_decl_list_prime:fdlp
                  {: fdlp.add(0,f); RESULT = fdlp; :}

                  ;

formal_decl_list_prime ::= /* Let epsilon > 0 */
                  {: RESULT = new LinkedList<Formal>(); :}

                  | COMMA formal:f formal_decl_list_prime:fdlp
                  {: fdlp.add(0,f); RESULT = fdlp; :}

                  ;

var_decl  ::= type:ty ID:id SEMICOLON
              {:  RESULT = new VarDecl(ty,new Token(id.getText(),id.getLine(),id.getPos())); :}
              ;

var_decl_list  ::=  /* epsilon */
               {: RESULT = new LinkedList<VarDecl>(); :}

               |  var_decl_list:vdl  var_decl:vd 
               {: vdl.add(vd); RESULT = vdl; :}

               ;

type      ::=  INT
               {: RESULT = new IntType(); :}

            |  BOOL
               {: RESULT = new BoolType(); :}
            
            |  ID:id
               {: Token newToken = new Token(id.getText(),id.getLine(),id.getPos());
                  RESULT = new ClassType(newToken); :}

            |  INT LBRACK RBRACK
               {: RESULT = new ArrayType(); :}

            ;

stmt ::=  ID:i EQUALS expr:e SEMICOLON
          {: RESULT = new AssignStatement(new Token(i.getText(), i.getLine(), i.getPos()), e); :}

       |  ID:ref LBRACK expr:index RBRACK EQUALS expr:val SEMICOLON
          {: RESULT = new ArrayAssignStatement(ref, index, val); :}

       |  SYSO LPAREN expr:e RPAREN SEMICOLON 
          {: RESULT = new PrintStatement(e); :}

       |  block_stmt:b
          {: RESULT = b; :}

       |  IF LPAREN expr:e RPAREN stmt:s1 ELSE stmt:s2 
          {: RESULT = new IfStatement(e, s1, s2); :}

       |  WHILE LPAREN expr:e RPAREN stmt:s 
          {: RESULT = new WhileStatement(e, s); :}

       ;

block_stmt ::= LBRACE stmt_list:sl RBRACE 
               {: RESULT = new BlockStatement(sl); :};

stmt_list  ::=  /*  epsilon  */
               {: RESULT = new LinkedList<IStatement>(); :}

            |  stmt:s stmt_list:sl 
               {: sl.add(0,s); RESULT = sl; :};

expr_list ::=
                  /* lambda */
                  {: RESULT = new LinkedList<IExp>(); :}

                  | expr:e expr_list_prime:edlp
                  {: edlp.add(0,e); RESULT = edlp; :}

                  ;

expr_list_prime ::= /* Let epsilon > 0 */
                  {: RESULT = new LinkedList<IExp>(); :}

                  | COMMA expr:e expr_list_prime:edlp
                  {: edlp.add(0,e); RESULT = edlp; :}

                  ;

expr      ::= 
                NUMBER:n
                {: RESULT = new IntegerExp(new Token(n.text,n.line,n.pos)); :}

            |   TRUE:t
                {: RESULT = new TrueExp(new Token(t.text,t.line, t.pos)); :}

            |   FALSE:f
                {: RESULT = new FalseExp(new Token(f.text,f.line, f.pos)); :}

            |   ID:i 
                {: RESULT = new IdExp(i); :}
        
            |   expr:l PLUS expr:r
                {: RESULT = new PlusExp(l,r); :}
        
            |   expr:l MINUS expr:r
                {: RESULT = new MinusExp(l,r); :}

            |   expr:l TIMES expr:r
                {: RESULT = new MulExp(l,r); :}

            |   LPAREN expr:e RPAREN
                {: RESULT=e; :}

            |   expr:l AND expr:r
                {: RESULT = new AndExp(l,r); :}

            |   expr:l LESSTHAN expr:r
                {: RESULT = new LtExp(l,r); :}

            |   NOT expr:e
                {: RESULT = new NotExp(e); :}

            |   THIS:t
                {: RESULT = new ThisExp(new Token(t.text,t.line,t.pos)); :}

            |   NEW ID:i LPAREN RPAREN
                {: RESULT = new NewExp(i); :}

            |   expr:e DOT ID:i LPAREN expr_list:elist RPAREN
                {: RESULT = new CallExp(e,i,elist); :}

            |   expr:array LBRACK expr:index RBRACK
                {: RESULT = new ArrayExp(array,index); :}

            |   expr:array DOT LENGTH
                {: RESULT = new LengthExp(array); :}

            |   NEW INT LBRACK expr:e RBRACK
                {: RESULT = new NewArrayExp(e); :}

         
            ;



