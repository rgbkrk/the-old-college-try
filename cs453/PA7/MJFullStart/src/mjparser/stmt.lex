/**
 * CS 453h Spring 2010
 * Kyle Kelley, Dave Newman, Paul Gagliardi
 * Updated for PA6
 */
package mjparser;
import ast.node.Token;
import java_cup.runtime.Symbol;
%%
%cup
%char
%line
%public
%eofval{
 return new Symbol(sym.EOF, new TokenValue("EOF", yyline+1, yychar));
%eofval}
EOL=[\n\r]
NOT_EOL=[^\r\n]
NOT_STAR=[^*]
NOT_STAR_OR_SLASH=[^*/]
LINE_COMMENT="//"{NOT_EOL}*{EOL}
C_COMMENT="/*"{NOT_STAR}*("*"({NOT_STAR_OR_SLASH}{NOT_STAR}*)?)*"*/"
%%
"class" { return new Symbol(sym.CLASS, new TokenValue(yytext(), yyline+1, yychar)); }
"public" { return new Symbol(sym.PUBLIC, new TokenValue(yytext(), yyline+1, yychar)); }
"static" { return new Symbol(sym.STATIC, new TokenValue(yytext(), yyline+1, yychar)); }
"void" { return new Symbol(sym.VOID, new TokenValue(yytext(), yyline+1, yychar)); }
"main" { return new Symbol(sym.MAIN, new TokenValue(yytext(), yyline+1, yychar)); }
"String" { return new Symbol(sym.STRING, new TokenValue(yytext(), yyline+1, yychar)); }
"extends" { return new Symbol(sym.EXTENDS, new TokenValue(yytext(), yyline+1, yychar)); }
"new" { return new Symbol(sym.NEW, new TokenValue(yytext(), yyline+1, yychar)); }
"return" { return new Symbol(sym.RETURN, new TokenValue(yytext(), yyline+1, yychar)); }
"this" { return new Symbol(sym.THIS, new TokenValue(yytext(), yyline+1, yychar)); }
"while" { return new Symbol(sym.WHILE, new TokenValue(yytext(), yyline+1, yychar)); }
"int" { return new Symbol(sym.INT, new TokenValue(yytext(), yyline+1, yychar)); }
"boolean" { return new Symbol(sym.BOOL, new TokenValue(yytext(), yyline+1, yychar)); }
"if" { return new Symbol(sym.IF, new TokenValue(yytext(), yyline+1, yychar)); }
"else" { return new Symbol(sym.ELSE, new TokenValue(yytext(), yyline+1, yychar)); }
"false" { return new Symbol(sym.FALSE, new TokenValue(yytext(), yyline+1, yychar)); }
"true" { return new Symbol(sym.TRUE, new TokenValue(yytext(), yyline+1, yychar)); }
"length" { return new Symbol(sym.LENGTH, new TokenValue(yytext(), yyline+1, yychar)); }
"System.out.println" { return new Symbol(sym.SYSO, new TokenValue(yytext(), yyline+1, yychar)); }
"=" { return new Symbol(sym.EQUALS, new TokenValue(yytext(), yyline+1, yychar)); }
";" { return new Symbol(sym.SEMICOLON, new TokenValue(yytext(), yyline+1, yychar)); }
"+" { return new Symbol(sym.PLUS, new TokenValue(yytext(), yyline+1, yychar) ); }
"-" { return new Symbol(sym.MINUS, new TokenValue(yytext(), yyline+1, yychar) ); }
"*" { return new Symbol(sym.TIMES, new TokenValue(yytext(), yyline+1, yychar) ); }
"&&" { return new Symbol(sym.AND, new TokenValue(yytext(), yyline+1, yychar) ); }
"!" { return new Symbol(sym.NOT, new TokenValue(yytext(), yyline+1, yychar) ); }
"<" { return new Symbol(sym.LESSTHAN, new TokenValue(yytext(), yyline+1, yychar) ); }
"{" { return new Symbol(sym.LBRACE, new TokenValue(yytext(), yyline+1, yychar)); }
"}" { return new Symbol(sym.RBRACE, new TokenValue(yytext(), yyline+1, yychar)); }
"(" { return new Symbol(sym.LPAREN, new TokenValue(yytext(), yyline+1, yychar)); }
")" { return new Symbol(sym.RPAREN, new TokenValue(yytext(), yyline+1, yychar)); }
"[" { return new Symbol(sym.LBRACK, new TokenValue(yytext(), yyline+1, yychar)); }
"]" { return new Symbol(sym.RBRACK, new TokenValue(yytext(), yyline+1, yychar)); }
"," { return new Symbol(sym.COMMA, new TokenValue(yytext(), yyline+1, yychar)); }
"." { return new Symbol(sym.DOT, new TokenValue(yytext(), yyline+1, yychar)); }
[0-9]+ { return new Symbol(sym.NUMBER, new TokenValue(yytext(), yyline+1, yychar));}
[a-zA-Z][a-zA-Z0-9_]* { return new Symbol(sym.ID, new Token(yytext(), yyline+1, yychar)); }
[ \t] { /* ignore white space. */ }
{EOL} { /* ignore white space, reset char counter */ yychar = 0; }
{LINE_COMMENT} { /* ignore comments */ yychar = 0; yy_buffer_start = yy_buffer_index-1; /* reset for EOL */ }
{C_COMMENT} { /* ignore comments */  }


. { System.err.println("Illegal character: "+yytext()); }
