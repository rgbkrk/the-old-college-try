package symtable;

import java.io.File;
import java.io.FileWriter;
import java.util.Stack;

public class SymbolTable {

  private Scope mGlobalScope;
  private Stack<Scope> mScopeStack;

  public SymbolTable(){
    mGlobalScope = new Scope(null);
    this.mScopeStack = new Stack<Scope>();
    this.mScopeStack.push(mGlobalScope);
  }

  public SymbolTableEntry lookup(String key) {
    //Lookup the key in our global scope
    Scope scope = mScopeStack.peek();
    return scope.lookup(key);
  }

  public void insert(SymbolTableEntry ste) {
    //Insert into our global scope
    //mGlobalScope.insert(ste);
    Scope scope = mScopeStack.peek();
    scope.insert(ste);
  }
 
 public int size() {
   return mScopeStack.peek().size();
  }

 /**
  * Used in creating the symbol table
  */
  public void insertAndPushScope(NamedScopeSTE namedScopeSTE)
  {
    Scope enclosingScope = this.mScopeStack.peek();
    //Add the named STE to the enclosing Scope
    enclosingScope.insert(namedScopeSTE);

    //Creating the scope for the NamedScopeSTE
    Scope scope = new Scope(enclosingScope);
    namedScopeSTE.setScope(scope);
    //Pushing this new scope onto the stack
    this.mScopeStack.push(scope);
  }

  /**
   * Assumes the symbol table was already built
   */
  public void pushScope(String name)
  {
    SymbolTableEntry ste = this.lookup(name);
    this.pushScope(ste);
  }

  private void pushScope(NamedScopeSTE ste){
    this.mScopeStack.push(ste.getScope());
  }

  private void pushScope(SymbolTableEntry ste){
    if( ste instanceof NamedScopeSTE)
    {
      this.mScopeStack.push(((NamedScopeSTE)ste).getScope());
    }
    //This be an error, which won't happen
  }

  public Scope popScope()
  {
    return this.mScopeStack.pop();
  }

  private static int uniqID = 0;

  public static int nextID()
  {
    return uniqID++;
  }

  public void outputDOT(String filename) throws java.io.IOException {
    uniqID = 0;

    FileWriter fw = new FileWriter(new File(filename));

    fw.write("digraph SymTable {\n");
    fw.write("  graph [rankdir=\"LR\"];\n");
    fw.write("  node [shape=record];\n");
    fw.write(mGlobalScope.genDOT(true));
    fw.write("}\n");
    fw.close();
  }
}
