package symtable;

public abstract class NamedScopeSTE extends SymbolTableEntry {

  protected Scope mScope;

  public Scope getScope() {return mScope;}
  public void setScope(Scope mScope) {this.mScope = mScope;}
}
