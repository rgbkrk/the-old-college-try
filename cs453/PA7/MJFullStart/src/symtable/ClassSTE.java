package symtable;

public class ClassSTE extends NamedScopeSTE {



  public String genDOT() {
      int uid = SymbolTable.nextID();
      String dot = uid + ":<f0>;\n";
      dot += "  " + uid + " ";
      dot += "[label=\" <f0> ClassSTE | <f1> mName = " + name +
             " | <f2> mMain = false "+ 
             " | <f3> mSuperClass = null" + 
             " | <f4> mScope\"];\n";

      dot += "  " + uid + ":<f4> -> " + mScope.genDOT(false);
      return dot;

  }

  public int getNumVariables() {
    return mScope.numVarSTE();
  }

  public String toString()
  {
    return "name " + name;
  }

}

