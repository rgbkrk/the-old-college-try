package symtable;

import ast.node.Token;
import ast.node.BoolType;
import ast.node.IntType;
import ast.node.ArrayType;
import ast.node.ClassType;
import ast.node.IType;

public abstract class SymbolTableEntry {
   protected String name;

   public String getName(){return name;}
   public void setName(String name){this.name = name;}

   public abstract String genDOT();


   public static Type getType(IType it){
     if(it instanceof BoolType)
       return Type.BOOL;
     else if(it instanceof IntType)
       return Type.INT;
     else if(it instanceof ArrayType)
       return Type.ARRAY;
     else if(it instanceof ClassType){
       ClassType ct = (ClassType)it;
       return Type.getClassType(ct.getName().getText());
     }
     return null;
   }
}
