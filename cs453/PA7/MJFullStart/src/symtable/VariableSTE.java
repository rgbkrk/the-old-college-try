package symtable;

public class VariableSTE extends SymbolTableEntry {

   private int offset;
   private String base;
   private Type type;
   private boolean isMember;

   
   public String genDOT() {
      int uid = SymbolTable.nextID();
      String dot = uid + ":<f0>;\n";
      dot += "  " + uid + " ";
      dot += "[label=\" <f0> VarSTE | <f1> mName = " + name +
             " | <f2> mType = " + type +
             " | <f3> mBase = " + base +
             " | <f4> mOffset = " + offset + "\"];\n";
      return dot;
   }

   public String toString()
   {
     return "name " + name + " type " + type + " offset " + offset + " base " + base;
   }

   public Type getType() {return type;}
   public void setType(Type type) {this.type = type;}

   public int getOffset() {return offset;}
   public void setOffset(int offset) {this.offset = offset;}
   public String getBase() {return base;}
   public void setBase(String base) {this.base = base;}
   public boolean isMember(){return isMember;}
   public void setIsMember(boolean isMember){this.isMember = isMember;}
}
