package symtable;

import ast.node.IType;
import ast.node.Formal;
import java.util.List;
import java.util.LinkedList;

public class Signature {
  /* Next time the type may need to be a class */
  private Type returnType;
  private List<Type> formals;

  public Signature(IType type, List<Formal> formals)
  {
    this.returnType = SymbolTableEntry.getType(type);
    this.formals = new LinkedList<Type>();
    for(Formal f: formals)
    {
      this.formals.add(SymbolTableEntry.getType(f.getType()));
    }
  }

  public List<Type> getFormals()
  {
    return this.formals;
  }

  public Type getReturnType(){
    return this.returnType;
  }

  public String genDOT()
  {
    String dot = "mSignature = (";
    for(int i = 0; i < formals.size() - 1; i++)
    {
      Type type = formals.get(i);
      dot += type + ", ";
    }
    if(formals.size() > 0){
      Type type = formals.get(formals.size()-1);
      dot += type;
    }
    dot += ") returns " + returnType;

    return dot;
  }
}
