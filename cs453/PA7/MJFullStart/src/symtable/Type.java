package symtable;

import exceptions.*;

import java.util.*;

public class Type
{
  public static final Type ARRAY = new Type();
  public static final Type BOOL = new Type();
  public static final Type INT = new Type();

  // class type map (key: class name, value: type)
  private static final HashMap<String, Type> classTypes
    = new HashMap<String, Type>();
  
  // If a particular Type instance is a class type
  // then this is the class name
  private final String className;

  private Type()
  {
    className = null;
  }

  public boolean isClassType() {
    return (this != Type.ARRAY && this != Type.BOOL && this != Type.INT);
  }

  private Type(String className)
  {
    if(className == null)
    {
      throw new InternalException("unexpected null argument");
    }

    this.className = className;
  }

  public static  Type getClassType(String className)
  {
    if(className == null)
    {
      throw new InternalException("unexpected null argument");
    }

    Type classType = (Type) classTypes.get(className);
    if(classType == null)
    {
      classType = new Type(className);
      classTypes.put(className, classType);
    }
    return classType;
  }

  // returns true if this type is a class type and false otherwise
  public boolean isReference()
  {
    return className != null;
  }

  public String getClassName()
  {
    if(className == null)
    {
      throw new InternalException("non-reference type");
    }

    return className;
  }

  public String toString()
  {
    if(this == INT)
    {
      return "INT";
    }

    if(this == BOOL)
    {
      return "BOOL";
    }

    if(this == ARRAY)
    {
      return "[INT]";
    }

    return "class_" + className + ";";
  }
}
