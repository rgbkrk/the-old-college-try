/**
 * This code generates MIPS code from an ast tree.
 *
 * 2/23/2010 - Modified from our own EvaVisitor.java
 * 2/23/2010 - Modified from Michelle Strout's DotVisitor.java
 *          Kyle Kelley, Dave Newman, Paul Gagliardi 
 * 6/06 - Modified from Brian Richard's ParserTest.Java.
 *        Michelle Strout
 */
package ast_visitors;

// These imports are all for SableCC-generated packages.
import java.io.PrintWriter;
import java.util.Stack;
import java.util.HashMap;

import ast.analysis.DepthFirstAdapter;
import ast.node.*;
import label.Label;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


import symtable.*;


/**
 * We extend the DepthFirstAdapter.
 * Visitors invoke a defaultCase method on each node they visit.
 * We evaulate the tree and print result
 */
public class MIPSgenVisitor extends DepthFirstAdapter {

  protected SymbolTable symTable;
  protected static final String TRUE = "1";
  protected static final String FALSE = "0";

  private String currentClass;

  protected PrintWriter out; // The stream to which we will write generated code.

  protected static int SIZEOFINT = 4;
  protected HashMap<Node, Type> nodeToClassMap;

  protected boolean willNeed_printint;
  protected boolean willNeed_halloc;

  public MIPSgenVisitor(PrintWriter out, SymbolTable symTable, HashMap<Node,Type> nodeToClassMap) {
    this.symTable = symTable;
    this.willNeed_printint = false;
    this.willNeed_halloc = false;
    this.nodeToClassMap = nodeToClassMap;
    this.currentClass = "";
    this.out = out;
  }

  public void defaultIn(Node node) {
    out.flush();
  }

  public void defaultOut(Node node) {
    out.println();
    out.flush();
  }


   /** Helper routines to push a value onto the stack
    * and pop it off */
  protected void mipsPush(String register, String comment) {
    if (comment.equals(""))
      comment = " \t# mipsPush";
    else
      comment = " \t# " + comment;
    out.println("\taddi $sp, $sp, -" + SIZEOFINT);
    out.println("\tsw   " + register + ", 0($sp)" + comment);
  }

  protected void mipsPush(String register) {
    mipsPush(register, "");
  }

  protected void mipsPop(String register, String comment) {
    if (comment.equals(""))
      comment = " \t# mipsPop";
    else
      comment = " \t# " + comment;
    out.println("\tlw   " + register + ", 0($sp)");
    out.println("\taddi $sp, $sp, " + SIZEOFINT + comment);
  }

  protected void mipsPop(String register) {
    mipsPop(register, "");
  }

  protected void mipsPeek(String register, String comment) {
    if (comment.equals(""))
      comment = " \t# mipsPeek";
    else
      comment = " \t# " + comment;
    out.println("\tlw   " + register + ", 0($sp)" + comment);
  }

  protected void mipsPeek(String register) {
    mipsPeek(register, "");
  }

  protected void genLabel(Label label) {
    out.println(label + ":");
  }

  protected String mangleName(String className, String methodName) {
    return "__" + className + "__" + methodName;
  }

  /** AST visitor methods to generate code for each node. */

  public void outAssignStatement(AssignStatement node){
    out.println("\t## outAssignStatement");
    int offset = 0;
    VariableSTE variable = (VariableSTE)this.symTable.lookup(node.getId().getText());
    offset = variable.getOffset();
    String base = variable.getBase();
    if(variable.isMember()){
      out.println("\tlw   $t1, 16($fp)\t#get implicit \"this\" parameter");
      base = "$t1";
    }
    out.println("\t# Making assignment to variable " + node.getId().getText());
    out.println("\t# from the stack.");
    mipsPop("$t0");
    out.println("\tsw   $t0, " + offset + "(" + base + ")");
    defaultOut(node);
  }

  public void outArrayAssignStatement(ArrayAssignStatement node) {
    out.println("\t## outArrayAssignStatement");

    mipsPop("$t2", "pop value to register");
    mipsPop("$t0", "pop array index to register");

    out.println("\t#Calculating the address of the array first");
    int offset = 0;
    VariableSTE variable = (VariableSTE)this.symTable.lookup(node.getRef().getText());
    offset = variable.getOffset();
    String base = variable.getBase();
    if(variable.isMember()){
      out.println("\tlw   $t1, 16($fp) \t#get implicit \"this\" parameter");
      base = "$t1";
    }
    out.println("\tlw   $t4, " + offset + "(" + base + ")");
    //$t4 is now the base address of the array
    //Now we have the base address
    //We now need to add the index to it
    out.println("\tli   $t3, " + SIZEOFINT);
    out.println("\tmul  $t0, $t0, $t3");
    out.println("\tadd  $t1, $t0, $t4");
    out.println("\taddi $t1, $t1, " + SIZEOFINT + " \t# skip over length item");

    out.println("\tsw $t2, 0($t1)");
    defaultOut(node);
  }

  public void outIdExp(IdExp node){
    out.println("\t## outIdExp");
    int offset = 0;
    VariableSTE variable = (VariableSTE)this.symTable.lookup(node.getId().getText());
    offset = variable.getOffset();
    String base = "";
    if (!variable.isMember())
      // local vars used the base from the symbol table
      base = variable.getBase();
    else {
      // We think we should be able to use getBase(), but member
      // variables don't appear to have the right base.
      out.println("\tlw   $t1, 16($fp) \t# get implicit \"this\" parameter");
      base = "$t1";
    }
    //Put the rvalue into $t0
    out.println("\t# Get the rvalue of variable " + node.getId().getText());
    out.println("\tlw   $t0, " + offset + "(" + base + ")");
    //Push it
    mipsPush("$t0");
    defaultOut(node);
  }

  public void outPrintStatement(PrintStatement node){
    out.println("\t## outPrintStatement");
    willNeed_printint = true;
    //Assumes that the last item on the stack is to be printed
    out.println("\tjal _printint \t\t# System.out.println from the top of the stack");
    mipsPop("$t0"); //Pop the argument put on the stack for our benefit
    defaultOut(node);
  }

  
  public void outPlusExp(PlusExp node){
    out.println("\t## PlusExp");
    opGen("add");
    defaultOut(node);
  }

  public void outMulExp(MulExp node){
    out.println("\t## MulExp");
    opGen("mul");
    defaultOut(node);
  }
  
  public void outMinusExp(MinusExp node){
    out.println("\t## MinusExp");
    opGen("sub");
    defaultOut(node);
  }

  /**
   * Helper method to generate binary operations
   */
  public void opGen(String op) {
    mipsPop("$t1");
    mipsPop("$t0");
    out.println("\t"+op+"  $t0, $t0, $t1");
    mipsPush("$t0");
  }

  /**
   * Control statements.
   */
  public void caseIfStatement(IfStatement node) {
    defaultIn(node);
    out.println("\t## caseIfStatement");
    IStatement elseStatement = node.getElseStatement();
    IStatement thenStatement = node.getThenStatement();
    IExp boolExp = node.getExp();
    if(boolExp!= null) boolExp.apply(this);
    Label elseLabel = new Label();
    Label endLabel = new Label();
    mipsPop("$t0");
    out.println("\tbeq  $t0, $0, " + elseLabel);
    if(thenStatement != null) thenStatement.apply(this);
    out.println("\tj " + endLabel);
    genLabel(elseLabel);
    if(elseStatement != null) elseStatement.apply(this);
    genLabel(endLabel);
    defaultOut(node);
  }

  public void caseWhileStatement(WhileStatement node) {
    defaultIn(node);
    out.println("\t## caseWhileStatement");
    Label startLabel = new Label();
    Label conditionLabel = new Label();
    out.println("\tj " + conditionLabel);
    genLabel(startLabel);
    IStatement body = node.getStatement();
    body.apply(this);
    genLabel(conditionLabel);
    IExp boolExp = node.getExp();
    boolExp.apply(this);
    mipsPop("$t0");
    out.println("\tbne  $t0, $0 " + startLabel);
    defaultOut(node);
  }

   /**
    * Literals: integers and booleans.
    */
  public void outIntegerExp(IntegerExp node){
    out.println("\t## outIntegerExp");
    out.println("\tli   $t0, " + node.getLiteral().getText());
    mipsPush("$t0");
    defaultOut(node);
  }

  public void outFalseExp(FalseExp node) {
    out.println("\t## outFalseExp");
    out.println("\tli   $t0," + FALSE);
    mipsPush("$t0");
    defaultOut(node);
  }

  public void outTrueExp(TrueExp node) {
    out.println("\t## outTrueExp");
    out.println("\tli   $t0, " + TRUE);
    mipsPush("$t0");
    defaultOut(node);
  }

   /**
    * Logical operators
    */
  public void outNotExp(NotExp node) {
    out.println("\t## outNotExp");
    mipsPop("$t0");
    out.println("\txor  $t0, $t0, " + TRUE);
    mipsPush("$t0");
    defaultOut(node);
  }

  public void caseAndExp(AndExp node) {
    defaultIn(node);
    out.println("\t## caseAndExp");
    IExp left = node.getLExp();
    left.apply(this);
    mipsPeek("$t0");
    Label endLabel = new Label();
    out.println("\t# Using register 0 for false!");
    out.println("\tbeq  $t0, $0, " + endLabel + " \t# Was left operand false?"); 
    out.println("\taddi $sp, $sp, 4 \t# Get rid of \"true\" result from left operand (&&)");
    IExp right = node.getRExp();
    right.apply(this);
    genLabel(endLabel);
    defaultOut(node);
  }

  public void outLtExp(LtExp node) {
    out.println("\t## outLtExp");
    mipsPop("$t1");
    mipsPop("$t0");
    out.println("\tslt  $t0, $t0, $t1");
    mipsPush("$t0");
    defaultOut(node);
  }

  /**
   * Classes
   */

   public void inTopClassDecl(TopClassDecl node) {
     inClassDecl(node, node.getName());      
   }

   public void outTopClassDecl(TopClassDecl node) {
     symTable.popScope();   
   }

   public void inChildClassDecl(ChildClassDecl node) {
      inClassDecl(node, node.getName());        
   }

   public void outChildClassDecl(ChildClassDecl node) {
     symTable.popScope();   
   }


   private void inClassDecl(IClassDecl node, Token tok) {
      symTable.pushScope(tok.getText());
      this.currentClass = tok.getText();
   }


  public void outMainClass(MainClass node) {
    //We're all done
    out.println("\t# That's all folks!");
    // Put stack pointer at frame pointer
    out.println("\taddi $sp, $fp, 0 \t# \"clean up\" the stack");
    out.println("\tli   $v0, 10");
    out.println("\tsyscall");
    defaultOut(node);
   }

  public void outProgram(Program node) {
    //Put in code for necessary utility functions
    //if needed
    if(this.willNeed_printint)
      _printint();
    if(this.willNeed_halloc)
      _halloc();
    defaultOut(node);
  }

  public void inMainClass(MainClass node) {
    //main prologue
    out.println("##");
    out.println("## Compiled from MiniJava for Spring 2010 cs453");
    out.println("## Group 453h: Paul Gagliardi, Kyle Kelley, David Newman");
    out.println("##");
    out.println("\t.text");
    out.println("main:");
    // Put frame pointer at stack pointer
    out.println("\taddi $fp, $sp, 0 \t# init frame pointer");
   //main has no local vars
    defaultIn(node);
  }

  /**
   * Methods
   */
  public void inMethodDecl(MethodDecl node) {
    symTable.pushScope(node.getName().getText());
    out.println();
    out.println("\t.text");
    out.println("\t# in method declaration");
    // more name-mangling here.
    out.println(mangleName(currentClass, node.getName().getText()) + ":");
    out.println("\taddi $sp, $sp, -"+ SIZEOFINT + " \t#set up space for return value"); 
    mipsPush("$ra", "Save return address.");
    mipsPush("$fp", "Save old frame pointer.");
    out.println("\taddi $fp, $sp, -4 \t#set up new frame pointer");

    int allocation = node.getVarDecls().size() * SIZEOFINT;
      if(allocation > 0){
        out.println("\taddi  $sp, $sp, -"+ allocation + "\t# allocate space for local variables.");
    }
    defaultIn(node);
  }

  public void outMethodDecl(MethodDecl node) {
    out.println("\t## out method declaration");
    mipsPop("$t0"); // pop return val
    out.println("\tsw   $t0, 12($fp) \t# put return value +12 off fp"); 
    int allocation = node.getVarDecls().size() * SIZEOFINT;
    if(allocation > 0) {
      out.println("\t# Deallocating " + allocation);
      out.println("\taddi $sp, $sp, " + allocation + " \t# deallocate space for local variables"); 
    }
    mipsPop("$fp");
    mipsPop("$ra");
    out.println("\tjr $ra");
    symTable.popScope();
    defaultOut(node);
  }

  public void caseCallExp(CallExp node) {
    defaultIn(node);
    out.println("\t## caseCallExp");
    // evaluate the reciever expression
    node.getExp().apply(this);

    // Now generate code for the actual method call, using the receiver as a parameter.
    int allocationSize = SIZEOFINT * (node.getArgs().size() + 1); // add one for receiver
    mipsPop("$t0", "pop the receiver");
    out.println("\taddi $sp, $sp, -" + allocationSize + " \t# Allocate space for the call");
      // note: here there's a new TYPE() that we need to think about?
    out.println("\tsw   $t0, 0($sp)"); // put "this" into it's proper location as a parameter
    if (allocationSize>1) {
      int currOffset = SIZEOFINT; 
      for (IExp param : node.getArgs()) {
        out.println("\t#Applying parameter \""+param+"\" for call");
        param.apply(this);
        mipsPop("$t0");
        out.println("\tsw   $t0, " + currOffset + "($sp)");
        currOffset += SIZEOFINT;
      }
    }
    // Handle name-mangling
    Type receiverType = nodeToClassMap.get(node.getExp());
    out.println("\tjal " + mangleName(receiverType.getClassName(), node.getId().getText()));
 
    mipsPop("$t0", "pop the return value");
    out.println("\taddi $sp, $sp, " + allocationSize + " \t# deallocate parameters from stack");
    mipsPush("$t0", "push the return value back on");
    defaultOut(node);
  }

  /**
   * Memory allocations.
   */
  public void outNewExp(NewExp node){
    out.println("\t## outNewExp");
    // get size of the class
    Type classType = nodeToClassMap.get(node);
    ClassSTE classSTE = (ClassSTE)symTable.lookup(classType.getClassName());
    int numWords = classSTE.getNumVariables();
    // push (actual) size on to MIPS stack
    out.println("\tli   $t0, " + SIZEOFINT*numWords);
    allocMem("$t0", "$t1");
    defaultOut(node);
  }

  /**
   * 
   */
  public void outThisExp(ThisExp node){
    out.println("\tlw   $t0, 16($fp) \t# outThisExp, getting this");
    mipsPush("$t0");
    defaultOut(node);
}

  public void outNewArrayExp(NewArrayExp node){
    out.println("\t## outNewArrayExp");
    mipsPeek("$t0", "peek at the size for use now and storage later");
    out.println("\taddi $t0, $t0, 1 \t# add 1 for the length component");
    out.println("\tli   $t1, " + SIZEOFINT );
    out.println("\tmul  $t0, $t0, $t1 \t# multiply by " + SIZEOFINT);
    allocMem("$t0", "$t1");
    //At this point, we know the address is on the stack
    mipsPop("$t1", "pop off the address");
    mipsPop("$t0", "pop off the size");
    mipsPush("$t1", "push the address back on");
    out.println("\tsw   $t0, 0($t1) \t# Store the size");

    //Loop through
    Label start = new Label();
    Label end = new Label();
    genLabel(start);
    
    //Check to see if t1 is zero
    out.println("\tbeqz $t0, " + end);
    out.println("\taddi $t0, $t0, -1");
    out.println("\taddi $t1, $t1, " + SIZEOFINT);
    out.println("\tsw   $0,  0($t1) \t# Store zero");
    out.println("\tj " + start);

    genLabel(end);

    defaultOut(node);
  }

  public void outLengthExp(LengthExp node) {
    out.println("\t## outLengthExp");
    mipsPop("$t0", "Getting the address of the array being referenced");
    //t0 has what we need, the spot is at zero, and quite arbitrarily
    //we use t1 for the value.
    dereference("$t0", "$0", "$t1");
    mipsPush("$t1");
    defaultOut(node);
  }

  public void outArrayExp(ArrayExp node) {
    out.println("\t# outArrayExp");
    mipsPop("$t1", "pop array index to register");
    mipsPop("$t0", "pop array address to register");
    out.println("\taddi $t1, $t1, 1 \t## add one to account for length cell in array");
    out.println("\tli   $t2, " + SIZEOFINT );
    out.println("\tmul  $t1, $t1, $t2 \t# multiply by " + SIZEOFINT);
    dereference("$t0", "$t1", "$t0");
    mipsPush("$t0");
    defaultOut(node);
  }


  protected void dereference(String addrReg, String offsetReg, String valueReg){
    out.println("\tadd  " + addrReg + ", " + addrReg + ", " + offsetReg + " \t# compute address offset");
    out.println("\tlw   " + valueReg + ", 0(" + addrReg + ") \t# dereference address");
  }

  protected void reference(String addrReg, String offsetReg, String newAddrReg) {
  }

  protected void allocMem(String sizeRegister, String addressRegister){
    this.willNeed_halloc = true;
    mipsPush(sizeRegister);
    out.println("\tjal _halloc");
    // pop return value (address of object)
    // pop param 
    mipsPop(addressRegister, "pop address of allocated memory");
    mipsPop(sizeRegister, "pop size of allocated memory");
    // push address of object
    mipsPush(addressRegister, "push address of allocated memory");
  }

  /**
   * Run-time library.
   */
  protected void _halloc() {

    out.println();
    out.println("## A function accepting one argument, _halloc allocates");
    out.println("## the number of bytes on the heap required by the arg.");
    out.println("## Returns the address of the first byte allocated.");

    out.println("\t.text:");
    out.println("_halloc:");

    out.println("\t#prologue");
    out.println("\taddi  $sp, $sp, -4 \t# save stack space for return val ");
    out.println("\taddi  $sp, $sp, -4 \t# push return address");
    out.println("\tsw    $ra, 0($sp)");
    out.println("\taddi  $sp, $sp, -4 \t# push frame pointer on stack.");
    out.println("\tsw    $fp, 0($sp)");
    out.println("\taddi  $fp, $sp, -4 \t# set up new frame pointer");

    out.println("\t#do our stuff");
    out.println("\tlw    $a0, 16($fp) \t# load the argument n into a0");
    out.println("\tli    $v0, 9       \t# 9 is sbrk");
    out.println("\tsyscall");

    out.println("\t#epilogue ");
    out.println("\tsw    $v0, 12($fp) \t# save return val on stack ");
    out.println("\t                   \t# <v0 has the return value>");

    out.println("\tlw    $fp, 4($fp)  \t# pop frame pointer");
    out.println("\taddi  $sp, $sp, 4");

    out.println("\tlw    $ra, 0($sp)  \t# pop return address");
    out.println("\taddi  $sp, $sp, 4");

    out.println("\tjr    $ra          \t# jump to return address");

  }

  protected void _printint() {

    out.println();
    out.println("## A function accepting one argument, _printint prints");
    out.println("## the arg using the print system call in MIPS.");
 
    out.println("\t.text");
    out.println("_printint:");

    out.println("\taddi $sp, $sp, -4 \t# push return addr");
    out.println("\tsw   $ra, 0($sp)");

    out.println("\taddi $sp, $sp, -4 \t# push frame pointer");
    out.println("\tsw   $fp, 0($sp)");

    out.println("\taddi $fp, $sp, -4  \t# set up frame pointer");

    out.println("\tlw   $a0, 12($fp)  \t# load the int");
    //out.println("\t  # will it work with just $sp?");

    out.println("\tli   $v0, 1        \t# print the int");
    out.println("\tsyscall");

    out.println("\tla   $a0, lf");
    out.println("\tli   $v0, 4");
    out.println("\tsyscall");

    out.println("\tlw   $fp, 0($sp)   \t# pop frame pointer");
    out.println("\taddi $sp, $sp, 4");

    out.println("\tlw   $ra, 0($sp)   \t# pop return addr");
    out.println("\taddi $sp, $sp, 4");

    out.println("\tjr   $ra           \t# return to caller");
    out.println(".data:");
    out.println("\tlf: .asciiz \"\\n\"");
  }

}
