/**
 * This  visitor checks the type of our AST nodes and reports errors.
 *
 * 4/1/2010  - Modified from our own MIPSgenVisitor.java
 * 2/23/2010 - Modified from our own EvaVisitor.java
 * 2/23/2010 - Modified from Michelle Strout's DotVisitor.java
 * 			   Kyle Kelley, Dave Newman, Paul Gagliardi 
 * 6/06 - Modified from Brian Richard's ParserTest.Java.
 *        Michelle Strout
 */
package ast_visitors;

import java.util.LinkedList;
import java.util.Stack;
import java.util.EmptyStackException;
import java.util.HashMap;

import ast.analysis.DepthFirstAdapter;
import ast.node.*;
import java.util.List;

import symtable.*;

/**
 * We extend the DepthFirstAdapter.  
 * Visitors invoke a defaultCase method on each node they visit.  
 * We evaulate the tree and print result
 */
public class TypeCheckVisitor extends DepthFirstAdapter {

  private class TypeAndToken{
    Type type;
    Token token;
    public TypeAndToken(Type type, Token token){
      this.type = type;
      this.token = token;
    }

    public String toString() {
      return "type: " + type + " token " + token.getText() + " " + token.getLine() + " " + token.getPos();
    }
  }

   public class TypeCheckException extends RuntimeException {}

   private SymbolTable symTable;

   private Stack<TypeAndToken> typeAndTokenStack;
   private HashMap<Node,Type> nodeToClassMap; 
   private String currentClass;

   public TypeCheckVisitor(SymbolTable symTable) {
      this.symTable = symTable;
      this.typeAndTokenStack = new Stack<TypeAndToken>();
      this.nodeToClassMap =  new HashMap<Node,Type>();
   }

   public HashMap<Node, Type> getNodeToClassMap() {
     return nodeToClassMap;
   }

   private void compilationError(Token tok, String message) {
      System.err.println( "[" + tok.getLine() + "," + tok.getPos() + 
                             "] " + message);
      throw new TypeCheckException();
   }

   public void outAssignStatement(AssignStatement node) {


      VariableSTE localVar = (VariableSTE)this.symTable.lookup(node.getId().getText());
      
      if (localVar == null) {
        // Freak out here for undeclared vars.
        compilationError(node.getId(), "Undeclared variable " + node.getId().getText());
      }

      TypeAndToken tat = typeAndTokenStack.pop();

      if ( tat.type != localVar.getType() ){
        // Freak out because the variable and the expression
        // being assigned to it are not of the same type.
        compilationError(tat.token, "Invalid expression type assigned to variable " + node.getId());
      }
   }

  public void outIdExp(IdExp node){
      // push the type onto the typeAndTokenStack
      VariableSTE localVar = (VariableSTE)this.symTable.lookup(node.getId().getText());
      if (localVar != null)
         typeAndTokenStack.push(new TypeAndToken(localVar.getType(),node.getId()));
      else
         compilationError(node.getId(), "Undeclared variable " + node.getId().getText());
      if( localVar.getType().isClassType()) this.nodeToClassMap.put(node, localVar.getType());
   }

  private void opTypeCheck(Type type, String op) {
    TypeAndToken tat = typeAndTokenStack.pop();

    if (tat.type != type)
      compilationError(tat.token, "Invalid operand type for operator " + op);
    //Push the token info back on
    typeAndTokenStack.push(tat);
  }

  private void argTypeCheck(Type type, String methodName) {
    TypeAndToken tat = typeAndTokenStack.pop();

    if (tat.type != type)
      compilationError(tat.token, "Invalid argument type for method " + methodName);
    // No push back, as the print statement is done.
  }

  private void binOpTypeCheck(Type type, String op) throws TypeCheckException {
    TypeAndToken right, left;
    try {
      right = typeAndTokenStack.pop();
      left = typeAndTokenStack.pop();
    }
    catch (EmptyStackException e) {
      System.err.println("WHOA, Empty Stack Exception. Why ME!?");
      throw new TypeCheckException();
    }
    if (left.type != type)
      compilationError(left.token, "Invalid left operand type for operator " + op);
    else if (right.type != type){
      compilationError(right.token, "Invalid right operand type for operator " + op);
    }
    //Push the token info back on
    typeAndTokenStack.push(left);
  }

  public void outIfStatement(IfStatement node)
  {
    //Check for boolean type
    TypeAndToken tat = typeAndTokenStack.pop();
    if(tat.type != Type.BOOL)
      compilationError(tat.token, "Invalid condition type for if statement");
    //Since we got the expression type, it is done. No need to push
  }

  /*
   * For debugging purposes only
   */
  private void printStack() {
    System.out.println("PRINTING STACK");
    for(TypeAndToken t: typeAndTokenStack) {
      System.out.println(t.toString());
    }
    System.out.println("PRINTING STACK END");
  }
   
  public void outWhileStatement(WhileStatement node) {
    //Check for boolean type
    TypeAndToken tat = typeAndTokenStack.pop();
    if(tat.type != Type.BOOL)
      compilationError(tat.token, "Invalid condition type for while statement");
    //Since we got the expression type, it is done. No need to push
  }

   public void outTrueExp(TrueExp node){
     // push the type onto the typeAndTokenStack
      typeAndTokenStack.push(new TypeAndToken(Type.BOOL, node.getLiteral()));
   }
   
   public void outFalseExp(FalseExp node){
     // push the type onto the typeAndTokenStack
      typeAndTokenStack.push(new TypeAndToken(Type.BOOL, node.getLiteral()));
   }
   
   public void outIntegerExp(IntegerExp node){
      typeAndTokenStack.push(new TypeAndToken(Type.INT, node.getLiteral()));
   }

   public void outNotExp(NotExp node){
     opTypeCheck(Type.BOOL, "!");
   }

   public void outPrintStatement(PrintStatement node){
     argTypeCheck(Type.INT, "println");
   }

   public void outAndExp(AndExp node){
     binOpTypeCheck(Type.BOOL, "&&");
   }

   public void outLtExp(LtExp node){
    binOpTypeCheck(Type.INT, "<");
    TypeAndToken left = typeAndTokenStack.pop();
    //Push the token info back on, changing one thing... It's now a boolean!
    TypeAndToken newLeft = new TypeAndToken(Type.BOOL, left.token);
    typeAndTokenStack.push(newLeft);
   }

   public void outPlusExp(PlusExp node){
     binOpTypeCheck(Type.INT, "+");
   }

   public void outMulExp(MulExp node){
     binOpTypeCheck(Type.INT, "*");
   }
   
   public void outMinusExp(MinusExp node){
     binOpTypeCheck(Type.INT, "-");
   }

   /** PA6 Additions */
   /**
    * Need to add the following errors:

      [LINENUM,POSNUM] Invalid type returned from method METHODNAME
      // any method declaration

      [LINENUM,POSNUM] Method METHODNAME does not exist

      [LINENUM,POSNUM] Method METHODNAME requires exactly NUM arguments

      [LINENUM,POSNUM] Invalid argument type for method METHODNAME
        // any call to a method (note that println is a method)
    * 
    */
   public void outCallExp(CallExp node){
     /*
      * Check for these:

      [LINENUM,POSNUM] Method METHODNAME does not exist

      [LINENUM,POSNUM] Method METHODNAME requires exactly NUM arguments

      [LINENUM,POSNUM] Invalid argument type for method METHODNAME
        // any call to a method (note that println is a method)
      */
      Type classType = nodeToClassMap.get(node.getExp());
      if(classType == null) compilationError(node.getId(), "Receiver of method call must be a class type");
      ClassSTE cste = (ClassSTE) symTable.lookup(classType.getClassName());
      Scope localScope = cste.getScope(); //get symbol table for class

      SymbolTableEntry ste = localScope.lookup(node.getId().getText());
      if(ste == null || ! (ste instanceof MethodSTE)) 
        compilationError(node.getId(), "Method " + node.getId().getText() +
		       	" does not exist in class type " + classType.getClassName());
      MethodSTE mste = (MethodSTE) ste;
      Signature sig = mste.getSignature();
      List<Type> formals = sig.getFormals();

      // Testing the number of arguments
      if(formals.size() != node.getArgs().size())
        compilationError(node.getId(), "Method " + node.getId().getText() +
                         " requires exactly " + formals.size() + " arguments");

      // Testing the types of the arguments
      LinkedList<TypeAndToken> l_to_r_formal_list = new LinkedList<TypeAndToken>();
      for(int i = 0; i< formals.size(); i++) {
        l_to_r_formal_list.add(0,typeAndTokenStack.pop());
      }
      for(Type s : formals) {
        TypeAndToken tat = l_to_r_formal_list.remove(0);
        if (tat.type != s) 
        compilationError(tat.token, "Invalid argument type for method " + node.getId().getText());
      }
      typeAndTokenStack.pop(); //popping off "reciever"

      TypeAndToken temp = new TypeAndToken(sig.getReturnType(), node.getId());
      typeAndTokenStack.push(temp);

      // record the type of this method call in the node to class map
      this.nodeToClassMap.put(node, sig.getReturnType());

   }

   public void inTopClassDecl(TopClassDecl node) {
     inClassDecl(node, node.getName());      
   }

   public void outTopClassDecl(TopClassDecl node) {
     symTable.popScope();   
   }

   public void inChildClassDecl(ChildClassDecl node) {
      inClassDecl(node, node.getName());        
   }

   public void outChildClassDecl(ChildClassDecl node) {
     symTable.popScope();   
   }


   private void inClassDecl(IClassDecl node, Token tok) {
      symTable.pushScope(tok.getText());
      this.currentClass = tok.getText();
   }

   public void inMethodDecl(MethodDecl node) {
      symTable.pushScope(node.getName().getText()); 
   }

   public void outMethodDecl(MethodDecl node) {
      //[LINENUM,POSNUM] Invalid type returned from method METHODNAME
      // any method declaration
    //Check for same type
    TypeAndToken tat = typeAndTokenStack.pop();
    if(tat.type != SymbolTableEntry.getType(node.getType()))
      compilationError(tat.token, "Invalid type returned from method " + node.getName().getText());
    //Since we got the expression type, it is done. No need to push
    symTable.popScope();
     //Last expression should be the return statement
   }

   //ThisExp
   public void outThisExp(ThisExp node){
     TypeAndToken tat = new TypeAndToken( Type.getClassType(this.currentClass) , node.getId());
     this.typeAndTokenStack.push(tat);
     this.nodeToClassMap.put(node, Type.getClassType(this.currentClass));
   }

   //ArrayAssignStatement
   public void outArrayAssignStatement(ArrayAssignStatement node){
     TypeAndToken valueTat = typeAndTokenStack.pop();
     TypeAndToken indexTat = typeAndTokenStack.pop();

     VariableSTE variable = (VariableSTE)this.symTable.lookup(node.getRef().getText());
      
      if (variable == null) {
        // Freak out here for undeclared vars.
        compilationError(node.getRef(), "Undeclared variable " + node.getRef());
      }
     
     if(variable.getType() != Type.ARRAY)
       compilationError(node.getRef(), "Array reference to non-array type");

     if(indexTat.type != Type.INT)
       compilationError(indexTat.token, "Invalid index expression type for array reference");

     if(valueTat.type != Type.INT)
       compilationError(valueTat.token, "Invalid expression type assigned into array");
     
   }

   //ArrayExp
   public void outArrayExp(ArrayExp node){
     TypeAndToken indexTat = typeAndTokenStack.pop();
     TypeAndToken arrayRefTat = typeAndTokenStack.pop();

     if(arrayRefTat.type != Type.ARRAY)
       compilationError(arrayRefTat.token, "Array reference to non-array type");

     if(indexTat.type != Type.INT)
       compilationError(indexTat.token, "Invalid index expression type for array reference");
     
     TypeAndToken arrayTat = new TypeAndToken(Type.INT, arrayRefTat.token);
     this.typeAndTokenStack.push(arrayTat);
   }

   public void outNewArrayExp(NewArrayExp node){
     //arraySize on stack
     TypeAndToken arraySizeTat = typeAndTokenStack.pop();
     if(arraySizeTat.type != Type.INT)
       compilationError(arraySizeTat.token, "Invalid operand type for new array operator");

     TypeAndToken arrayTat = new TypeAndToken(Type.ARRAY, arraySizeTat.token);
     this.typeAndTokenStack.push(arrayTat);
   }


   public void outNewExp(NewExp node){
     //Get Type from Symbol table using node.getId().getText()
     String className = node.getId().getText();
     SymbolTableEntry ste = symTable.lookup(className);
     if(ste == null || ! (ste instanceof ClassSTE) ){
       compilationError(node.getId(), "Class " + className + " does not exist");
     }

     TypeAndToken tat = new TypeAndToken( Type.getClassType(className) , node.getId());
     //Pushing class from symbol table as the type
     this.typeAndTokenStack.push(tat);
     this.nodeToClassMap.put(node, Type.getClassType(className));
   }

   public void outFormal(Formal node){
     IType type = node.getType();
     if(type instanceof ClassType){
      String className = ((ClassType)type).getName().getText();
      SymbolTableEntry ste = symTable.lookup(className);
      if(ste == null || ! (ste instanceof ClassSTE) )
         compilationError(((ClassType)type).getName(), "Class " + className + " does not exist");
     }
     //otherwise, we are of type class
   }

   public void outVarDecl(VarDecl node){
     IType type = node.getType();
     if(type instanceof ClassType){
      String className = ((ClassType)type).getName().getText();
      SymbolTableEntry ste = symTable.lookup(className);
      if(ste == null || ! (ste instanceof ClassSTE) )
         compilationError(((ClassType)type).getName(), "Class " + className + " does not exist");
     }
     //otherwise, we are of type class
   }

   //LengthExp
   public void outLengthExp(LengthExp node){
     TypeAndToken tat = this.typeAndTokenStack.pop();

     if( tat.type != Type.ARRAY)
       compilationError(tat.token, "Operator length called on non-array type");

     TypeAndToken lengthTat = new TypeAndToken(Type.INT, tat.token);
     this.typeAndTokenStack.push(lengthTat);
   }


}
