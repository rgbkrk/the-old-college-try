/**
 *
 * 			   Kyle Kelley, Dave Newman, Paul Gagliardi 
 */
package ast_visitors;

// These imports are all for SableCC-generated packages.
import java.io.PrintWriter;
import java.util.Stack;
import java.util.HashMap;

import ast.analysis.DepthFirstAdapter;
import ast.node.*;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


import symtable.*;


/**
 * We extend the DepthFirstAdapter.  
 * Visitors invoke a defaultCase method on each node they visit.  
 * We evaulate the tree and print result
 */
public class SymbolTableCreationVisitor extends DepthFirstAdapter {

   // Instance variables.
   private SymbolTable symTable;
   private boolean compilationFails;
   private static int SIZEOFINT = 4;
   private int localVariableOffset;
   private int memberVariableOffset;
   private int argumentOffset;

   private String currentClass;

   private boolean createLocalVars;

   private static int START_ARGUMENTS = 16;
   private static int START_LOCALS = 0;
   private static int START_MEMBERS = 0;

   // Creator.
   public SymbolTableCreationVisitor() {
      this.createLocalVars = false;
      this.symTable = new SymbolTable();
      this.localVariableOffset = START_LOCALS;
      this.memberVariableOffset = START_MEMBERS;
      this.argumentOffset = START_ARGUMENTS;
      this.compilationFails = false;
   }

   public boolean getCompilationFailed() {
     return compilationFails;
   }

   public SymbolTable getSymbolTable() {
     return this.symTable;
   }

   private void compilationError(Token tok, String message) {
      System.err.println( "[" + tok.getLine() + "," + tok.getPos() + 
                             "] " + message + " " + tok.getText());
      compilationFails = true;
   }

   public void outVarDecl(VarDecl node)
   {
     if(createLocalVars){
       if(addToSymbolTable(node.getType(), node.getName(), localVariableOffset))
          localVariableOffset -= SIZEOFINT; // assumes booleans and ints are of same size.
     }
     else
       if(addToSymbolTable(node.getType(), node.getName(), memberVariableOffset))
          memberVariableOffset += SIZEOFINT; // assumes booleans and ints are of same size.

   }

   public void outFormal(Formal node)
   {
     if(addToSymbolTable(node.getType(), node.getName(), argumentOffset));
       argumentOffset += SIZEOFINT;
   }

   public boolean addToSymbolTable(IType type, Token token, int offset)
   {
     if (this.symTable.lookup(token.getText()) != null) {
        // Freak out, then recover because we're required to continue
        // The var has already been declared so we don't add anything
        // to the symbol table.
        System.err.println( "[" + token.getLine() + "," + token.getPos() + 
                             "] Redeclared symbol " + token.getText());
        compilationFails = true;
        return false;
     }
     
     // We have an undeclared variable, so put it in the symbol table.
     VariableSTE ste = new VariableSTE();
     ste.setOffset(offset);
     ste.setBase("$fp");
     ste.setName(token.getText());
     ste.setType(SymbolTableEntry.getType(type));
     ste.setIsMember(!createLocalVars);
     this.symTable.insert(ste);

     return true;
   }

   public void inChildClassDecl(ChildClassDecl node){
     inClassDecl(node, node.getName());
   }

   public void inTopClassDecl(TopClassDecl node){
     inClassDecl(node, node.getName());
   }

   public ClassSTE inClassDecl(IClassDecl node, Token tok){
     this.currentClass = tok.getText();
     ClassSTE existingClass = (ClassSTE) this.symTable.lookup(this.currentClass);
     if (existingClass != null)
       compilationError(tok, "*** Class Redeclared: ");
     ClassSTE ste = new ClassSTE();
     ste.setName(tok.getText());
     this.symTable.insertAndPushScope(ste);
     //Offsets?
     this.memberVariableOffset = START_MEMBERS;

     this.createLocalVars = false;
     return ste;
   }

   public void outChildClassDecl(ChildClassDecl node){
     this.createLocalVars = false;
     this.symTable.popScope();
   }

   public void outTopClassDecl(TopClassDecl node){
     this.createLocalVars = false;
     this.symTable.popScope();
   }

   public void inMethodDecl(MethodDecl node) {
     //Think about methods with same name error
     // We did not do this before turning it in
     // Can we do this: addToSymbolTable();
     SymbolTableEntry existingMethod = (MethodSTE) this.symTable.lookup(node.getName().getText());
     if (existingMethod != null)
       compilationError(node.getName(), "*** Method Redeclared: ");

     MethodSTE ste = new MethodSTE();
     ste.setName(node.getName().getText());
     Signature signature = new Signature(node.getType(), node.getFormals());
     ste.setSignature(signature);

     //Scope will be set within the SymbolTable
     this.symTable.insertAndPushScope(ste);
     //Need to reset the frame pointer to 0
     this.localVariableOffset = START_LOCALS;
     this.argumentOffset = START_ARGUMENTS;
     this.createLocalVars = true;


     // Adding this to the scope of the method
     VariableSTE thiste = new VariableSTE();
     thiste.setOffset(argumentOffset);
     thiste.setBase("$fp");
     thiste.setName("this");
     thiste.setType(Type.getClassType(this.currentClass));
     this.symTable.insert(thiste);
     argumentOffset += SIZEOFINT;
   }

   public void outMethodDecl(MethodDecl node)
   {
     this.createLocalVars = false;
     this.symTable.popScope();
   }

   
}
