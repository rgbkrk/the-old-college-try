/**
 * This code generates MIPS code from an ast tree.
 *
 * 2/23/2010 - Modified from our own EvaVisitor.java
 * 2/23/2010 - Modified from Michelle Strout's DotVisitor.java
 * 			   Kyle Kelley, Dave Newman, Paul Gagliardi 
 * 6/06 - Modified from Brian Richard's ParserTest.Java.
 *        Michelle Strout
 */
package ast_visitors;

// These imports are all for SableCC-generated packages.
import java.io.PrintWriter;
import java.util.Stack;
import java.util.HashMap;
import java.util.ArrayList;

import ast.analysis.DepthFirstAdapter;
import ast_visitors.MIPSgenVisitor;
import ast.node.*;
import label.Label;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import symtable.*;
import java.util.LinkedList;
import java.util.List;


/**
 * We extend the DepthFirstAdapter.  
 * Visitors invoke a defaultCase method on each node they visit.  
 * We evaulate the tree and print result
 */
public class MIPSgenECVisitor extends MIPSgenVisitor {

  // The following are specific to the register allocation visitor.
  private final static String ON_STACK = "ON_STACK"; 
  private final static String LEFT_REG = "$t0";
  private final static String RIGHT_REG = "$t1";
  private final static String SAVE_FROM_STACK_REG = LEFT_REG;

  private Stack<String> regStack;
  private ArrayList<RegObject> registers;
  private HashMap<String,RegObject> nameToRegObjectMap;

  public MIPSgenECVisitor(PrintWriter out, SymbolTable symTable, HashMap<Node,Type> typeMap) {
    super(out, symTable, typeMap);
    this.regStack = new Stack<String>();
    this.registers = new ArrayList<RegObject>();
    this.nameToRegObjectMap = new HashMap<String,RegObject>();
    String name;
    RegObject reg;
    // Give me an A!
    for (int i = 1; i < 4; i++) {
      name = "$a" + i;
      reg = new RegObject(name, true);
      this.nameToRegObjectMap.put(name, reg);
      registers.add(reg);
    }
    // Give me a T!
    for(int i = 2; i < 10; i++) {
      name = "$t" + i;
      reg = new RegObject(name, true);
      this.nameToRegObjectMap.put(name, reg);
      registers.add(reg);
    } 
    // Give me a V!
    for (int i = 1; i < 2; i++) {
      name = "$v" + i;
      reg = new RegObject(name, true);
      this.nameToRegObjectMap.put(name, reg);
      registers.add(reg);
    }
    // Gimme an S!
     for (int i = 0; i < 8; i++) {
      name = "$s" + i;
      reg = new RegObject(name, true);
      this.nameToRegObjectMap.put(name, reg);
      registers.add(reg);
    }

  }


   public void outMethodDecl(MethodDecl node) {
    out.println("\t# out method declaration");
    String location = getLocation(true);
    // we got the result of the last computation, so we store it to the stack frame.
    out.println("\tsw "+ location + ", 12($fp)\t# put return value +12 off fp"); 
    int allocation = node.getVarDecls().size() * SIZEOFINT;
    if(allocation > 0) {
      out.println("\t# Deallocating " + allocation);
      out.println("\taddi $sp, $sp, " + allocation + "\t# deallocate space for local variables"); 
    }
    mipsPop("$fp");
    mipsPop("$ra");
    out.println("\tjr $ra");
    out.println();
    symTable.popScope();
    defaultOut(node);
  }


  public void saveRegisters(List<String> regList) {
    out.println("\t# saveRegisters");
    for ( int i=0; i<registers.size(); i++) {
      RegObject reg = registers.get(i);
      if (!reg.isFree && regList.contains(reg.regName))
        mipsPush(reg.regName);
    }
  }

  public void saveRegisters() {
    out.println("\t# saveRegisters");
    for ( int i=0; i<registers.size(); i++) {
      RegObject reg = registers.get(i);
      if (!reg.isFree)
        mipsPush(reg.regName);
    }
  }


  public void loadRegisters(List<String> regList) {
    out.println("\t# loadRegisters");
    for ( int i=registers.size()-1; i>=0;  i--) {
      RegObject reg = registers.get(i);
      if (!reg.isFree && regList.contains(reg.regName))
        mipsPop(reg.regName);
    }
  }

  public void loadRegisters() {
    out.println("\t# loadRegisters");
    for ( int i=registers.size()-1; i>=0;  i--) {
      RegObject reg = registers.get(i);
      if (!reg.isFree)
        mipsPop(reg.regName);
    }
  }

  // TODO: Test new modifications
  public void caseCallExp(CallExp node) {
    out.println("\t# case call Exp");
    defaultIn(node);

    node.getExp().apply(this); // evaulate the receiver expression
    String receiverReg = getLocation(true);

    saveRegisters();


    int allocationSize = SIZEOFINT * (node.getArgs().size() + 1);
    out.println("\taddi $sp, $sp, -" + allocationSize + " # Allocate space for the call");
    out.println("\tsw   " + receiverReg + ", 0($sp)"); // put "this" into it's proper location as a parameter
    // note: here there's a new TYPE() that we need to think about?
    int currOffset = SIZEOFINT;
    for (IExp param : node.getArgs()) {
      out.println("\t#Applying parameter \""+param+"\" for call");
      param.apply(this);
      String loc = getLocation(true);
      out.println("\tsw " + loc + ", " + currOffset + "($sp)");
      currOffset += SIZEOFINT;
    }
    Type receiverType = nodeToClassMap.get(node.getExp());
    out.println("\tjal " + mangleName(receiverType.getClassName(), node.getId().getText()));

    // in the end, we save this to a register or to the stack.
    regStack.push(ON_STACK);
    //String saveLocation = getSaveLocation();
    mipsPop(SAVE_FROM_STACK_REG);
    out.println("\t# deallocating space for parameters");
    out.println("\taddi $sp, $sp, " + allocationSize);
    // This actually does the store.
    loadRegisters();
    store(SAVE_FROM_STACK_REG);
    out.println();
    defaultOut(node);
  }

   public void outAssignStatement(AssignStatement node){
    out.println("\t## outAssignStatement");
    int offset = 0;
    VariableSTE variable = (VariableSTE)this.symTable.lookup(node.getId().getText());
    offset = variable.getOffset();
    String base = variable.getBase();
    if(variable.isMember()){
      out.println("\tlw   $t1, 16($fp)\t#get implicit \"this\" parameter");
      base = "$t1";
    }
    out.println("\t# Making assignment to variable " + node.getId().getText());
    String reg = getLocation(true);
    out.println("\tsw   " + reg + ", " + offset + "(" + base + ")");
    defaultOut(node);
  }

  // TODO: test this!
  public void outArrayAssignStatement(ArrayAssignStatement node) {
    out.println("\t## outArrayAssignStatement");

    //Get some free registers

    //mipsPop("$t2", "pop value to register");
    String valReg = getLocation(true);
    //mipsPop("$t0", "pop array index to register");
    String indexReg = getLocation(false);

    markFreedom(valReg, false);
    markFreedom(indexReg, false);

    String tempSizeOfIntReg = getTempRegister();
    String tempAddressReg = getTempRegister();


    out.println("\t#Calculating the address of the array first");
    int offset = 0;
    VariableSTE variable = (VariableSTE)this.symTable.lookup(node.getRef().getText());
    offset = variable.getOffset();
    String base = variable.getBase();
    if(variable.isMember()){
      out.println("\tlw   " +  tempAddressReg + ", 16($fp) \t#get implicit \"this\" parameter");
      base = tempAddressReg;
    }
    out.println("\tlw    "+  tempAddressReg +  ", " + offset + "(" + base + ")");
    //tempAddressReg is now the base address of the array
    //Now we have the base address
    //We now need to add the index to it
    out.println("\tli   " + tempSizeOfIntReg + ", " + SIZEOFINT);
    out.println("\tmul  " + indexReg + ", " + indexReg + ", " +  tempSizeOfIntReg);
    out.println("\tadd  " + tempAddressReg + ", " + indexReg + ", " + tempAddressReg);
    out.println("\taddi " + tempAddressReg + ", " + tempAddressReg + ", " + SIZEOFINT + " \t# skip over length item");
    
    markFreedom(valReg, true);
    markFreedom(indexReg, true);
    markFreedom(tempSizeOfIntReg, true);
    markFreedom(tempAddressReg, true);
    
    out.println("\tsw " + valReg + ", 0(" + tempAddressReg + ")");
    defaultOut(node);
  }


  public void outIdExp(IdExp node){
    out.println("\t## outIdExp");
    int offset = 0;
    VariableSTE variable = (VariableSTE)this.symTable.lookup(node.getId().getText());
    offset = variable.getOffset();
    String base = "";
    if (!variable.isMember())
      // local vars used the base from the symbol table
      base = variable.getBase();
    else {
      // We think we should be able to use getBase(), but member
      // variables don't appear to have the right base.
      out.println("\tlw   $t1, 16($fp) \t# get implicit \"this\" parameter");
      base = "$t1";
    }
    //Put the rvalue into $t0
    out.println("\t# Get the rvalue of variable " + node.getId().getText());
    String reg = getSaveLocation();
    out.println("\tlw   " + reg + ", " + offset + "(" + base + ")");
    //Push it
    store(reg);
    defaultOut(node);
  }


  // Handle the special case of System.out.println
  public void outPrintStatement(PrintStatement node){
    willNeed_printint = true;
    //Assumes that the last item on the stack is to be printed

    if(! ON_STACK.equals(regStack.peek())){
      String reg = getLocation(true);
      mipsPush(reg);
    }
    else {
      // This will never happen unless there are only 2 registers...
      // If it does happen, we assume the result is on the stack
    }
    out.println("\tjal _printint #System.out.println from the top of the stack");

    //Don't need what we put on the stack
    out.println("\taddi $sp, $sp, 4");

    out.println();
    defaultOut(node);
  }

  //
  // Handle all arithmetic operations
  //
  // Handle addition
  public void outPlusExp(PlusExp node){
    out.println("\t# PlusExp");
    opGen("add");
    defaultOut(node);
  }

  // handle multiplication
  public void outMulExp(MulExp node){
    out.println("\t# MulExp");
    opGen("mul");
    defaultOut(node);
  }

  // Handle subtraction
  public void outMinusExp(MinusExp node){
    out.println("\t# MinusExp");
    opGen("sub");
    defaultOut(node);
  }

  // Generalized operator generation.
  public void opGen(String op) {
    String regRight = getLocation(false);
    String regLeft = getLocation(true);
    String storeRegister = getSaveLocation();
    out.println("\t" + op + " " + storeRegister + ", " + regLeft + ", " + regRight);
    store(storeRegister);
    out.println();
  }

  
  //
  // Handle all literals.
  //
  public void outIntegerExp(IntegerExp node){
    out.println("\t# IntegerExp");
    genLoad(node.getLiteral().getText());
    defaultOut(node);
  }

  public void outFalseExp(FalseExp node) {
    out.println("\t# FalseExp");
    genLoad(FALSE);
    defaultOut(node);
  }

  public void outTrueExp(TrueExp node) {
    out.println("\t# TrueExp");
    genLoad(TRUE);
    defaultOut(node);
  }

  // Generalized literal handler.
  public void genLoad(String val) {
    String reg = getSaveLocation();
    out.println("\tli " + reg + ", " + val);
    store(reg);
    out.println();
  }


   //
   // Handle control statements.
   //
   public void caseIfStatement(IfStatement node) {
     IStatement elseStatement = node.getElseStatement();
     IStatement thenStatement = node.getThenStatement();
     IExp boolExp = node.getExp();
     Label elseLabel = new Label();
     Label endLabel = new Label();

     defaultIn(node);
     out.println("\t# IfStatement");
     if(boolExp!= null) boolExp.apply(this);
     String boolLoc = getLocation(true); // assume left for unary operators
     out.println("\t# Using register 0 for false!");
     out.println("\tbeq " + boolLoc + " $0, " + elseLabel);
     if(thenStatement != null) thenStatement.apply(this);
     out.println("\tj " + endLabel);
     genLabel(elseLabel);
     if(elseStatement != null) elseStatement.apply(this);
     genLabel(endLabel);
     defaultOut(node);
   }

   public void caseWhileStatement(WhileStatement node) {
     IStatement body = node.getStatement();
     IExp boolExp = node.getExp();
     Label startLabel =  new Label();
     Label conditionLabel = new Label();

     defaultIn(node);
     out.println("\t# WhileStatement");
     out.println("\t j " + conditionLabel);
     genLabel(startLabel);
     body.apply(this);
     genLabel(conditionLabel);
     boolExp.apply(this);
     String boolLoc = getLocation(true); // assume left for unary operators?
     out.println("\t# Using register 0 for false!");
     out.println("\tbne " + boolLoc + " $0 " + startLabel);
     defaultOut(node);
   }

   /** Logical operators and related expressions. */

   public void outNotExp(NotExp node) {
     out.println("\t# NotExp uses xor with 1 for NOT");
     String arg = getLocation(true);
     String dest = getSaveLocation();
     out.println("\txor " + dest + ", " + arg + " , " + TRUE);
     store(dest);
     out.println();
     defaultOut(node);
   }

   public void caseAndExp(AndExp node) {
      IExp left = node.getLExp();
      IExp right = node.getRExp();
      Label endLabel = new Label();

      defaultIn(node);
      // Create a temporary to hold the result.
      // Note to self: more writeup for final report!
      String destLoc = getSaveLocation();
      out.println("\t# CaseAndExp");
      left.apply(this);
      String leftLoc = getLocation(true);
      out.println("\t# Using register 0 for false!");
      out.println("\tmove "+ destLoc + " " + leftLoc );
      out.println("\tbeq "+ leftLoc + " $0, " + endLabel);
      right.apply(this);
      String rightLoc = getLocation(true);
      out.println("\tmove "+ destLoc + " " + rightLoc );
      genLabel(endLabel);
      store(destLoc);
      out.println();
      defaultOut(node);
   }

   public void outLtExp(LtExp node) {
      out.println("\t# LtExp");
      opGen("slt");
      defaultOut(node);
   }



   
  /**
   * Memory allocations.
   */
   // TODO: Test!
  public void outNewExp(NewExp node){
    out.println("\t## outNewExp");
    // get size of the class
    Type classType = nodeToClassMap.get(node);
    ClassSTE classSTE = (ClassSTE)symTable.lookup(classType.getClassName());
    int numWords = classSTE.getNumVariables();
    // push (actual) size on to MIPS stack
    String tempReg = "$t1";
    out.println("\tli   " + tempReg + ", " + SIZEOFINT*numWords);

    this.willNeed_halloc = true;
    // push argument for halloc
    mipsPush(tempReg);
    out.println("\tjal _halloc");
    // pop return value (address of object)
    // pop param 
    String addressRegister = getSaveLocation();
    mipsPop(addressRegister, "pop address of allocated memory");
    out.println("\taddi $sp, $sp, " + SIZEOFINT + " # pop off the size param");
    // push address of object
    store(addressRegister);

    defaultOut(node);
  }

   // TODO: test!
  public void outThisExp(ThisExp node){
    String thisRegister = getSaveLocation();
    out.println("\tlw   " + thisRegister + ", 16($fp) \t# outThisExp, getting this");
    store(thisRegister);
    defaultOut(node);
}
   // TODO: test me =(
  public void outNewArrayExp(NewArrayExp node){
    out.println("\t## outNewArrayExp");
    String sizeReg = getLocation(true); // "pops" the size
    if(sizeReg == LEFT_REG || sizeReg == RIGHT_REG){
      outOfRegistersOutNewArrayExp(sizeReg);
    }
    else{
      markFreedom(sizeReg, false);
      String tempReg = getTempRegister(); 
      
      out.println("\taddi "+ sizeReg + ", " + sizeReg + ", 1 \t# add 1 for the length component");
      out.println("\tli   " + tempReg + ", " + SIZEOFINT );
      out.println("\tmul  " + tempReg + ", " + sizeReg + ", " + tempReg + "\t# multiply by " + SIZEOFINT);
      
      String addressReg = allocMem(tempReg);
       out.println("\taddi "+ sizeReg + ", " + sizeReg + ", -1 \t# subtract 1 for zero-indexing");
      //At this point, we know the address is in addressReg
      out.println("\tsw   " + sizeReg + ", 0(" + addressReg + ") \t# Store the size in its location");

      out.println("\tmove " + tempReg + ", " + addressReg + " \t#Save address into temporary register");

      //Loop through
      Label start = new Label();
      Label end = new Label();
      genLabel(start);
      
      //Check to see if t1 is zero
      out.println("\tbeqz " + sizeReg + ", " + end);
      out.println("\taddi " + sizeReg + ", " + sizeReg + ", -1");
      out.println("\taddi " + tempReg + ", " + tempReg + ", " + SIZEOFINT);
      out.println("\tsw   $0,  0(" + tempReg + ") \t# Store zero to initialize array contents");
      out.println("\tj " + start);

      genLabel(end);
      markFreedom(tempReg, true);
      markFreedom(sizeReg, true);
    }
    defaultOut(node);
  }

  public void outOfRegistersOutNewArrayExp(String sizeReg){
      //Put size back on stack
      mipsPush(sizeReg);

      String tempReg = "$t1";
      
      out.println("\taddi "+ sizeReg + ", " + sizeReg + ", 1 \t# add 1 for the length component");
      out.println("\tli   " + tempReg + ", " + SIZEOFINT );
      out.println("\tmul  " + tempReg + ", " + sizeReg + ", " + tempReg + "\t# multiply by " + SIZEOFINT);

      this.willNeed_halloc = true;
      // push argument for halloc
      mipsPush(tempReg);
      out.println("\tjal _halloc");
      // pop return value (address of object)
      // pop param 
      mipsPop(tempReg, "pop address of allocated memory");
      out.println("\taddi $sp, $sp, " + SIZEOFINT + " #direct pop of the size (bytes)");

      mipsPop(sizeReg);
      out.println("\taddi "+ sizeReg + ", " + sizeReg + ", -1 \t# subtract 1 for zero-indexing");
      //At this point, we know the address is in addressReg
      out.println("\tsw   " + sizeReg + ", 0(" + tempReg + ") \t# Store the size in its location");

      regStack.push(ON_STACK);
      store(tempReg);

      //Loop through
      Label start = new Label();
      Label end = new Label();
      genLabel(start);
      
      //Check to see if t1 is zero
      //out.println("\tmove " + tempReg + ", " + addressReg + " \t#Save address into temporary register");
      out.println("\tbeqz " + sizeReg + ", " + end);
      out.println("\taddi " + sizeReg + ", " + sizeReg + ", -1");
      out.println("\taddi " + tempReg + ", " + tempReg + ", " + SIZEOFINT);
      out.println("\tsw   $0,  0(" + tempReg + ") \t# Store zero to initialize array contents");
      out.println("\tj " + start);

      genLabel(end);

  }

   // TODO: test this!
  public void outLengthExp(LengthExp node) {
    out.println("\t## outLengthExp");
    String reg = getLocation(true);
    markFreedom(reg, false);
    String storeReg = getSaveLocation();
    dereference(reg, "$0", storeReg);
    store(storeReg);
    markFreedom(reg, true);
    defaultOut(node);
  }
   // TODO: test this!
  public void outArrayExp(ArrayExp node) {
    out.println("\t# outArrayExp");
    String indexReg = getLocation(true);
    markFreedom(indexReg, false);
    String tempReg = getTempRegister();
    out.println("\taddi " + indexReg + ", " +  indexReg + ", 1 \t## add one to account for length cell in array");
    out.println("\tli   " + tempReg + ", " + SIZEOFINT );
    out.println("\tmul  " + indexReg + ", " + indexReg + ", " + tempReg + "\t# multiply by " + SIZEOFINT);
    markFreedom(tempReg, true);
    String addrReg = getLocation(false);
    String valReg = getSaveLocation();
    dereference(addrReg, indexReg, valReg);
    store(valReg);
    markFreedom(indexReg, true);
    defaultOut(node);
  }



  //
  // Utility methods.
  //
  //
  

  // TODO : Trust, but verify.
  protected String allocMem(String sizeRegister){
    String addressRegister = getSaveLocation();
    this.willNeed_halloc = true;
    // push argument for halloc
    mipsPush(sizeRegister);
    out.println("\tjal _halloc");
    // pop return value (address of object)
    // pop param 
    mipsPop(addressRegister, "pop address of allocated memory");
    mipsPop(sizeRegister, "pop size of allocated memory");
    // push address of object
    store(addressRegister);
    return addressRegister;
  }



  private String getLocation(boolean left) {
    // get a location (stack or register) to use for an operation.
    // left tells us whether we're looking at a left argument or not.
    String tempReg = regStack.pop();
    if(tempReg == ON_STACK) {
      if(left) tempReg = LEFT_REG;
      else tempReg = RIGHT_REG;
      mipsPop(tempReg);
    }
    else{
      free(tempReg);
    }
    return tempReg;
  }

  private void markFreedom(String register, boolean freedom) {
    if (!(register.equals("$t0") || register.equals("$t1"))) {
      RegObject registerObject = this.nameToRegObjectMap.get(register);
      registerObject.isFree = freedom;
    }
  }

  private String getTempRegister() {
    for(RegObject temp: registers) {
      if (temp.isFree) {
        temp.isFree = false;
        return temp.regName;
      }	  	
    }
    return RIGHT_REG;
  }

  private String getSaveLocation() {
    // get a location in which we can store something.
    for(RegObject temp: registers) {
      if (temp.isFree) {
        temp.isFree = false;
        regStack.push(temp.regName);
        return temp.regName;
      }	  	
    }
    regStack.push(ON_STACK);
    return SAVE_FROM_STACK_REG;

  }

  private void free(String reg) {
    // note that we're no longer using a location.
    for(RegObject temp: registers) {
      if(temp.regName.equals(reg)){
        temp.isFree = true;
        return;
      }
    }
  }

  private void store(String reg) {
    // generate code to store into a location.
    if(reg.equals(RIGHT_REG) || reg.equals(LEFT_REG))
      mipsPush(reg);
    //else we are done
  }

  private class RegObject {
    // a private class that keeps track of registers allocated.
    public String regName;
    public boolean isFree;
    public RegObject(String regName, boolean isFree) {
      this.regName = regName;	
      this.isFree = isFree;
    }
  }


}
