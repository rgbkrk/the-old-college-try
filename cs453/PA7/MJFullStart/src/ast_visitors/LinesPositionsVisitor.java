/**
 * This  visitor generates line and position data for those nodes that don't
 * already have it (which is to say for non-Token nodes).
 *
 */
package ast_visitors;

// These imports are all for SableCC-generated packages.
import java.util.Stack;

import ast.analysis.ReversedDepthFirstAdapter;
import ast.node.*;

import lines.*;

/**
 * We extend the ReverseDepthFirstAdapter.  
 */
public class LinesPositionsVisitor extends ReversedDepthFirstAdapter {

   private Lines lineData;
   private int currLine;
   private int currPos;

   public LinesPositionsVisitor() {
      super();
      lineData = new Lines();
      // These default values will be odd for degenerate
      // cases like an if-then-else with an empty else
      // clause at the end of a program.
      currLine = 1;
      currPos = 1;
   }

   public Lines getLines() {
      return lineData;
   }

   public void outToken(Token node) {
      currLine = node.getLine();
      currPos = node.getPos();
      lineData.setLine(node, currLine);
      lineData.setPos(node, currPos);
      //defaultOut(node);
   }

   public void defaultOut(Node node) {
      lineData.setLine(node, currLine);
      lineData.setPos(node, currPos);
      // The following would work if every node
      // had a token somewhere in it's subtree.
      //int line = lineData.getLine(node);
      //int pos = lineData.getPos(node);
      //lineData.setLine(node.parent(), line);
      //lineData.setPos(node.parent(), pos);
   }

   public void caseToken(Token node) {
      // For some reason the ReverseDepthFirstVisitor doesn't include this?
      //inToken(node);
      outToken(node);
   }


}
