   public void outCallExp(CallExp node){
      Type classType = nodeToClassMap.get(node.getExp());
      if(classType == null) compilationError(node.getId(), "Receiver of method call must be a class type");
      ClassSTE cste = (ClassSTE) symTable.lookup(classType.getClassName());
      Scope localScope = cste.getScope(); //get symbol table for class

      SymbolTableEntry ste = localScope.lookup(node.getId().getText());
      if(ste == null || ! (ste instanceof MethodSTE)) 
        compilationError(node.getId(), "Method " + node.getId().getText() +
		       	" does not exist in class type " + classType.getClassName());
      MethodSTE mste = (MethodSTE) ste;
      Signature sig = mste.getSignature();
      List<Type> formals = sig.getFormals();

      // Testing the number of arguments
      if(formals.size() != node.getArgs().size())
        compilationError(node.getId(), "Method " + node.getId().getText() +
                         " requires exactly " + formals.size() + " arguments");

      // Testing the types of the arguments
      LinkedList<TypeAndToken> l_to_r_formal_list = new LinkedList<TypeAndToken>();
      for(int i = 0; i< formals.size(); i++) {
        l_to_r_formal_list.add(0,typeAndTokenStack.pop());
      }
      for(Type s : formals) {
        TypeAndToken tat = l_to_r_formal_list.remove(0);
        if (tat.type != s) 
        compilationError(tat.token, "Invalid argument type for method " + node.getId().getText());
      }
      typeAndTokenStack.pop(); //popping off "reciever"

      TypeAndToken temp = new TypeAndToken(sig.getReturnType(), node.getId());
      typeAndTokenStack.push(temp);

      // record the type of this method call in the node to class map
      this.nodeToClassMap.put(node, sig.getReturnType());

   }


