#!/bin/bash

if [ $# -lt 1 ];
then
  echo "Usage: $0 <java_file>"
  exit 1
fi

if [ "$1" == "--help" ];
then
  echo "Attempts to run the currently built MJ.jar on a java file, then runs the corresponding .s file in Mars."
  echo "Usage: $0 <java_file>"
  exit 2
fi

java -jar MJ.jar --two-pass-mips $1
if [ -e "$1.s" ];
then
  echo "Running the compiled MIPS in Mars"
  java -jar Mars.jar "$1.s"
else
  echo "Oh noes! No MIPS code was generated!"
fi


