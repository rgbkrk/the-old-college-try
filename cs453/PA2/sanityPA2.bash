#!/bin/bash
# Wrote to test java files against corresponding mips files

FILES=../TestCases/*.java
for f in $FILES
do
  echo "*** Processing $f "
  BASE=`basename $f`
  echo $BASE
  echo "*** Done with $f"
done
