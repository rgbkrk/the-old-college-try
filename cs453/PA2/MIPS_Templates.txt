     # Templates for MIPS assembly programs
     # First, a template for the main body.
    .data #var declarations follow this
...
    .text #code follows this
    .globl main
main: #first line to execute follows this line
    ...
    # HALT the MARS simulation.
    li      $v0, 10
    syscall



fn_label:               # Make sure to put a label!

    # prologue for a function
    addi	$sp, $sp, -4	# save stack space for return val (optional)
    addi	$sp, $sp, -4	# push return address
    sw		$ra, 0($sp)	#
    addi	$sp, $sp, -4	# push frame pointer # set up new frame pointer
    sw		$fp, 0($sp)	#
    addi	$fp, $sp, -4	# set up new frame pointer 
    addi	$sp, $sp, -4	# One of these for each local variable (or multiply numVars*(-4))

    # do function stuff
    lw 		$t0, 16($fp)	# to load first parameter into $t0 (16 if ret value ... 12 if not)

    #epilogue for a function
    sw		$t0, 12($fp)	# save return val on stack (optional) <assuming t0 has the return value>
    addi	$sp, $sp, 4	# de-allocate (if there are local vars) (or multiply numVars*(-4))

    lw		$fp, 4($fp)	# pop frame pointer
    addi	$sp, $sp, 4	#

    lw		$ra, 0($sp)	# pop return address
    addi	$sp, $sp, 4	# 

    jr		$ra
    
Some sources of information:
http://logos.cs.uic.edu/366/notes/MIPS%20Quick%20Tutorial.htm
http://en.wikibooks.org/wiki/MIPS_Assembly/MIPS_Details

