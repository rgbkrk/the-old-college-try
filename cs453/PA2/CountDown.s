## CountDown.s for Spring 2010 cs453
## Group 453h: David Newman, Kyle Kelley, Paul Gagliardi
## An assembly program to count down from the number 7
## printing each number as we go.

    .text
    .globl main
main:
    addi	$sp, $sp, -4	# push the argument on the stack
    li		$t0, 7		# the specified argument is 7!
    sw		$t0, 0($sp)	#

    jal		_countdown

    addi 	$sp, $sp, 4	# pop the argument off

    li		$v0, 10		# halt!
    syscall

    .text
    .globl  _countdown
_countdown:
    # prologue for a function
    addi	$sp, $sp, -4	# push return address
    sw		$ra, 0($sp)	#
    addi	$sp, $sp, -4	# push frame pointer # set up new frame pointer
    sw		$fp, 0($sp)	#
    addi	$fp, $sp, -4	# set up new frame pointer -- 

    # do function stuff

    lw		$t0, 12($fp)    # load our argument into $t0
    blez	$t0, exit	# if $t0 is 0 jump to exit

    addi	$sp, $sp, -4	# push the argument on the stack
    sw		$t0, 0($sp)	#
    jal		_printint	# jump to print
    addi	$sp, $sp, 4	# pop the argument back off the stack

    lw		$t0, 12($fp)	# load our argument into $t0 again
    addi	$t0, $t0, -1	# subtract one from the argument
    addi    $sp, $sp, -4    # push the new argument on the stack
    sw		$t0, 0($sp)	#
    jal		_countdown	# recursive call
    addi	$sp, $sp, 4	# pop the argument back off the stack

exit:
    #epilogue for a function
    lw		$fp, 4($fp)	# pop frame pointer
    addi	$sp, $sp, 4	#

    lw		$ra, 0($sp)	# pop return address
    addi	$sp, $sp, 4	# 

    jr		$ra


## __printint.s
    .text

_printint:

    addi $sp, $sp, -4	# push return addr
    sw   $ra, 0($sp)

    addi $sp, $sp, -4   # push frame pointer
    sw   $fp, 0($sp)

    addi $fp, $sp, -4	# set up frame pointer

    lw	$a0, 12($fp)	# load the int
			# will it work with just $sp?

    li	$v0, 1		# print the int
    syscall

    la $a0, lf
    li $v0, 4
    syscall

    lw  $fp, 0($sp) 	# pop frame pointer
    addi $sp, $sp, 4

    lw  $ra, 0($sp)	# pop return addr
    addi $sp, $sp, 4

    jr   $ra		# return to caller


.data:
  lf: .asciiz "\n"
