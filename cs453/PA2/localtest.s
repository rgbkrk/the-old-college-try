  .text:
_localtest:

    addi	$sp, $sp, -4	# save stack space for return val 
    addi	$sp, $sp, -4	# push return address
    sw		$ra, 0($sp)	#
    addi	$sp, $sp, -4	# push frame pointer # set up new frame pointer
    sw		$fp, 0($sp)	#
    addi	$fp, $sp, -4	# set up new frame pointer 
    addi	$sp, $sp, -20	# Allocate space for 5 local variables
	#prologue completed
	#starting main body

    lw		$t0, 16($fp)  # load the argument n into t0
    addi	$t0, $t0, -1

    li		$t1, -4
    mul		$t0, $t1, $t0 # Find the complete offset

    add		$t0, $fp, $t0 # Compute the address

    #epilogue for a function
    sw		$t0, 12($fp)	# save return val on stack (optional)
    addi	$sp, $sp, 20	# de-allocate local variables

    lw		$fp, 4($fp)	# pop frame pointer
    addi	$sp, $sp, 4	#

    lw		$ra, 0($sp)	# pop return address
    addi	$sp, $sp, 4	# 

    jr		$ra
