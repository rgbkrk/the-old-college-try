## halloc.s for Spring 2010 cs453
## Group 453h: David Newman, Kyle Kelley, Paul Gagliardi
## A function accepting one argument, _halloc allocates
## the number of bytes on the heap required by the arg.
## Returns the address of the first byte allocated.

    .text:
_halloc:

    #prologue
    addi	$sp, $sp, -4	# save stack space for return val 
    addi	$sp, $sp, -4	# push return address
    sw		$ra, 0($sp)	#
    addi	$sp, $sp, -4	# push frame pointer on stack.
    sw		$fp, 0($sp)
    addi	$fp, $sp, -4	# set up new frame pointer

    #do our stuff
    lw    	$a0, 16($fp)	# load the argument n into a0
    li		$v0, 9		# 9 is sbrk
    syscall

    #epilogue 
    sw		$v0, 12($fp)	# save return val on stack 
                            # <v0 has the return value>

    lw		$fp, 4($fp)	# pop frame pointer
    addi	$sp, $sp, 4	#

    lw		$ra, 0($sp)	# pop return address
    addi	$sp, $sp, 4	# 

    jr		$ra		# jump to return address
