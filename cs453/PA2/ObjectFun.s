## CountDown.s for Spring 2010 cs453
## Group 453h: David Newman, Kyle Kelley, Paul Gagliardi
## An assembly program to demonstrate a "class" representation in mips
## This is the "hard" version, taking the easy one we already did,
## and trying to make it mimic the Java version more closely.

## Some things we could do to mimic it still more closely.
## create setter functions and them to set locals of
## class objects. (ditto getters)

   .text
	.globl main
main:
	addi 	$fp, $sp, 0	#init frame pointer

	##
	## allocate 1st local var as in 1st line of drive()
	##
	addi	$sp, $sp, -4	# save stack space for a local 

	##
	## allocate 2nd local var as in 2nd line of drive()
	##
	addi	$sp, $sp, -4	# save stack space for a local 

	##
	## allocate 3rd local var as in 3rd line of drive()
	##
	addi	$sp, $sp, -4	# save stack space for a local 

	##
	## allocate 1st object as in 4th line of drive()
	##
	addi	$sp, $sp, -4	# push the argument on the stack
	li	$t0, 8		# For 8 bytes
	sw	$t0, 0($sp)	#

	jal	_halloc

	lw 	$v0, 0($sp) #save ret value
	addi 	$sp, $sp, 4 # pop the return value
	addi 	$sp, $sp, 4 # pop the argument

	sw 	$v0, 0($fp) # storing 1st MyClass reference to 1st local

	##
	## allocate the second object as in 5th line of drive()
	##
	addi	$sp, $sp, -4	# push the argument on the stack
	li	$t0, 8		# For 8 bytes
	sw	$t0, 0($sp)	#

	jal	_halloc

	lw 	$v0, 0($sp) #save ret value
	addi 	$sp, $sp, 4 # pop the return value
	addi 	$sp, $sp, 4 # pop the argument

	sw 	$v0, 4($fp) # storing 2nd MyClass reference in 2nd local


	##
	## set int value of 1st object to 42 as in 6th line of drive()
	##
	li 	$t1, 42		#load 42
	lw	$t2, 0($fp)	#load address of 1st object
	addi	$t2, $t2, 4	#we're looking at 2nd local
	sw 	$t1, 0($t2)	#store 42 in second local of 1st object
	sw	$t1, 8($fp)	#store 42 in third local


	##
	## set bool value of 1st object to true as in 7th line of drive()
	##
	li	$t1, 1 		#load true
	lw	$t2, 0($fp)	#load address of 1st object
	addi	$t2, $t2, 0	#we're looking at 1st local
	sw 	$t1, 0($t2)	#store true in that address
	sw	$t1, 8($fp)	#store true in third local


	##
	## set int value of 2nd object to 33 as in 8th line of drive()
	##
	li 	$t1, 33		#load 33
	lw 	$t2, 4($fp)	#load address of 2nd object
	addi	$t2, $t2, 4	#we're looking at 2nd local
	sw	$t1, 0($t2)	#store 33 in second local of 2nd object
	sw	$t1, 8($fp)	#store 33 in third local
	
	##
	## set bool value of 2nd object to false as in 9th line of drive()
	##
	li 	$t1, 0 		#load false
	lw 	$t2, 4($fp)	#load address of 2nd object
	addi	$t2, $t2, 0	#we're looking at 1st local
	sw 	$t1, 0($t2)	#store false in that address
	sw	$t1, 8($fp)	#store false in third local


	##
	## print the boolean in the 1st object as in 12-16 of drive() 
	##
	lw	$t2, 0($fp)	#load address of 1st object
	addi	$t2, $t2, 0	#we're looking at 1st local of 1st object
	lw	$t1, 0($t2)	#get value of 1st local of 1st object
	blez	$t1, else1	#begin if-then-else
	li	$t1, 1		#begin then-part
	addi 	$sp, $sp, -4 	#push arg on stack
	sw 	$t1, 0($sp)
	jal	_printint	#call printint
	addi 	$sp, $sp, 4	#pop the argument
	j	end1
else1:
	li	$t1, 0		#begin else-part
	addi 	$sp, $sp, -4 	#push arg on stack
	sw 	$t1, 0($sp)
	jal	_printint	#call printint
	addi 	$sp, $sp, 4	#pop the argument
end1:

	##
	## print the int in the 1st object as in 17 of drive() 
	##
	lw	$t2, 0($fp)	#load address of 1st object
	addi	$t2, $t2, 4	#we're looking at 2nd local of 1st object
	lw	$t1, 0($t2)	#get value of 2nd local of 1st object
	addi 	$sp, $sp, -4	# push arg on stack
	sw 	$t1, 0($sp)
	jal	_printint	#call printint
	addi 	$sp, $sp, 4	#pop the argument off


	##
	## print the boolean in the 1st object as in 20-24 of drive() 
	##
	lw	$t2, 4($fp)	#load address of second object
	addi	$t2, $t2, 0	#we're looking at 1st local of 2nd object
	lw	$t1, 0($t2)	#get value of 1st local of 2nd object
	blez	$t1, else2	#begin if-then-else
	li	$t1, 1		#begin then-part
	addi 	$sp, $sp, -4 	#push arg on stack
	sw 	$t1, 0($sp)
	jal 	_printint	#call printint
	addi 	$sp, $sp, 4	#pop the argument
	j	end2
else2:
	li	$t1, 0		#begin else-part
	addi 	$sp, $sp, -4 	#push arg on stack
	sw 	$t1, 0($sp)
	jal	_printint	#call printint
	addi 	$sp, $sp, 4	#pop the argument
end2:


	##
	## print the int in the 2nd object as in 25 of drive() 
	##
	lw	$t2, 4($fp)	#load address of second object
	addi	$t2, $t2, 4	#we're dealing with 2nd local of 2nd object
	lw	$t1, 0($t2)	#get value of 2nd local of 2nd object
	addi 	$sp, $sp, -4	# push arg on stack
	sw 	$t1, 0($sp)
	jal 	_printint	#call printint
	addi 	$sp, $sp, 4	#pop the argument off


	##
	## end
	##
	addi 	$sp, $sp, 4	# pop a local variable
	addi 	$sp, $sp, 4	# pop a local variable
	addi 	$sp, $sp, 4	# pop a local variable
	li	$v0, 10		# halt!
	syscall

	.text:
_halloc:

	#prologue
	addi	$sp, $sp, -4	# save stack space for return val
	addi	$sp, $sp, -4	# push return address
	sw	$ra, 0($sp)	#
	addi	$sp, $sp, -4	# push frame pointer on stack.
	sw	$fp, 0($sp)
	addi	$fp, $sp, -4	# set up new frame pointer

	#do our stuff
	lw	$a0, 16($fp)	# load the argument n into a0
	li	$v0, 9		# 9 is sbrk
	syscall

	#epilogue
	sw	$v0, 12($fp)	# save return val on stack
				# <v0 has the return value>

	lw	$fp, 4($fp)	# pop frame pointer
	addi	$sp, $sp, 4	#

	lw	$ra, 0($sp)	# pop return address
	addi	$sp, $sp, 4	#

	jr	$ra		# jump to return address

## __printint.s
	.text

_printint:

	addi 	$sp, $sp, -4	# push return addr
	sw   	$ra, 0($sp)

	addi 	$sp, $sp, -4	# push frame pointer
	sw   	$fp, 0($sp)

	addi 	$fp, $sp, -4	# set up frame pointer

	lw	$a0, 12($fp)	# load the int
			# will it work with just $sp?

	li	$v0, 1		# print the int
	syscall

	la 	$a0, lf
	li 	$v0, 4
	syscall

	lw	$fp, 0($sp) 	# pop frame pointer
	addi 	$sp, $sp, 4

	lw  	$ra, 0($sp)	# pop return addr
	addi	$sp, $sp, 4

	jr	$ra		# return to caller


.data:
  lf: .asciiz "\n"


