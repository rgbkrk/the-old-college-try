## CountDown.s for Spring 2010 cs453
## Group 453h: David Newman, Kyle Kelley, Paul Gagliardi
## An assembly program to demonstrate a "class" representation in mips


   .text
    .globl main
main:
	addi 	$fp, $sp, 0 #init frame pointer

	addi	$sp, $sp, -8	# save stack space for 2xlocals

    addi	$sp, $sp, -4	# push the argument on the stack
    li		$t0, 8			# For 8 bytes
    sw		$t0, 0($sp)		#

    jal		_halloc

	lw 		$v0, 0($sp) #save ret value
	addi 	$sp, $sp, 4 # pop the return value
	addi 	$sp, $sp, 4 # pop the argument

	sw 		$v0, 0($fp) # storing 1st MyClass reference

	# Initialize the first "object"

	li 		$t1, 1 		#set to true
	sw 		$t1, 0($v0)	#load from init var
	li 		$t1, 42		#set to 42
	sw 		$t1, 4($v0)


	lw		$v0, 0($fp)
	addi	$v0, $v0, 0
	lw		$t1, 0($v0) # Load and print the boolean

	addi 	$sp, $sp, -4 #push arg on stack
	sw 		$t1, 0($sp)
	jal _printint
	addi 	$sp, $sp, 4 # pop the argument


	lw		$v0, 0($fp)
	addi	$v0, $v0, 4
	lw		$t1, 0($v0)  # Load and print the int

	addi 	$sp, $sp, -4 # push arg on stack
	sw 		$t1, 0($sp)
	jal _printint
	addi 	$sp, $sp, 4	# pop the argument off


	# Setting up the second object

	addi	$sp, $sp, -4	# push the argument on the stack
    li		$t0, 8			# For 8 bytes
    sw		$t0, 0($sp)		#

    jal		_halloc

	lw 		$v0, 0($sp) #save ret value
	addi 	$sp, $sp, 4 # pop the return value
	addi 	$sp, $sp, 4 # pop the argument

	sw 		$v0, 4($fp) # storing 2nd MyClass reference

	# Initialize the second "object"

	li 		$t1, 0 		#set to false
	sw 		$t1, 0($v0)	#load from init var
	li 		$t1, 33		#set to 33
	sw 		$t1, 4($v0)


	lw		$v0, 4($fp)
	addi	$v0, $v0, 0
	lw		$t1, 0($v0) # Load and print the boolean

	addi 	$sp, $sp, -4 #push arg on stack
	sw 		$t1, 0($sp)
	jal _printint
	addi 	$sp, $sp, 4 # pop the argument


	lw		$v0, 4($fp)
	addi	$v0, $v0, 4
	lw		$t1, 0($v0)  # Load and print the int

	addi 	$sp, $sp, -4 # push arg on stack
	sw 		$t1, 0($sp)
	jal _printint
	addi 	$sp, $sp, 4	# pop the argument off

	# End second object creation


	addi 	$sp, $sp, 8	# pop the local variables

    li		$v0, 10		# halt!
    syscall

    .text:
_halloc:

    #prologue
    addi	$sp, $sp, -4	# save stack space for return val
    addi	$sp, $sp, -4	# push return address
    sw		$ra, 0($sp)	#
    addi	$sp, $sp, -4	# push frame pointer on stack.
    sw		$fp, 0($sp)
    addi	$fp, $sp, -4	# set up new frame pointer

    #do our stuff
    lw    	$a0, 16($fp)	# load the argument n into a0
    li		$v0, 9		# 9 is sbrk
    syscall

    #epilogue
    sw		$v0, 12($fp)	# save return val on stack
                            # <v0 has the return value>

    lw		$fp, 4($fp)	# pop frame pointer
    addi	$sp, $sp, 4	#

    lw		$ra, 0($sp)	# pop return address
    addi	$sp, $sp, 4	#

    jr		$ra		# jump to return address

## __printint.s
    .text

_printint:

    addi $sp, $sp, -4	# push return addr
    sw   $ra, 0($sp)

    addi $sp, $sp, -4   # push frame pointer
    sw   $fp, 0($sp)

    addi $fp, $sp, -4	# set up frame pointer

    lw	$a0, 12($fp)	# load the int
			# will it work with just $sp?

    li	$v0, 1		# print the int
    syscall

    la $a0, lf
    li $v0, 4
    syscall

    lw  $fp, 0($sp) 	# pop frame pointer
    addi $sp, $sp, 4

    lw  $ra, 0($sp)	# pop return addr
    addi $sp, $sp, 4

    jr   $ra		# return to caller


.data:
  lf: .asciiz "\n"
