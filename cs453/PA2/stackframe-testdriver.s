	.text
	.globl main
main:
	addi  $sp, $sp, -4
	addi  $sp, $sp, -4		#PUSH
	sw    $ra, 0($sp)
	addi  $sp, $sp, -4		#PUSH
	sw    $fp, 0($sp)
	addi  $fp, $sp, -4
	addi  $sp, $fp, 0
	la    $t0, .L1
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L1:	.asciiz "Printing 42 to the next line:\n"
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall
	li    $t0, 42
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	jal   _printint
	lw    $t0, 0($sp)	#POP
	addi  $sp, $sp, 4
	la    $t0, .L2
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L2:	.asciiz "\nCalling localtest(2), result should be \n"
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall
	
	# calculate address for 2nd local in any function we call
    # that accepts one parameter and returns one value
    # $sp points just below where we will push param
    # $sp-8 is where return value slot will be
    # $sp-12 points at old $ra
    # $sp-16 points at old $fp
    # $sp-20 points at first local
    # $sp-24 points at second local
    addi  $t0, $sp, -24
    addi  $sp, $sp, -4  #PUSH
    sw    $t0, 0($sp)   
    jal   _printint
	lw    $t0, 0($sp)	#POP
	addi  $sp, $sp, 4
	
	
	li    $t0, 2
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	jal   _localtest
	lw    $v0, 0($sp)	#POP
	addi  $sp, $sp, 4
	lw    $t0, 0($sp)	#POP
	addi  $sp, $sp, 4
	move  $t0, $v0
	sw    $t0, 0($fp)
	la    $t0, .L3
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L3:	.asciiz "Result is "
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall
	lw    $t0, 0($fp)
	move  $a0, $t0
	li    $v0, 1		#WRITE
	syscall
	la    $t0, .L4
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L4:	.asciiz "\n"
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall
	la    $t0, .L5
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L5:	.asciiz "\nCalling localtest(1), result should be \n"
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall

	# calculate address for 1st local in any function we call
    # that accepts one parameter and returns one value
    # $sp points just below where we will push param
    # $sp-8 is where return value slot will be
    # $sp-12 points at old $ra
    # $sp-16 points at old $fp
    # $sp-20 points at first local
    # $sp-24 points at second local
    addi  $t0, $sp, -20
    addi  $sp, $sp, -4  #PUSH
    sw    $t0, 0($sp)   
    jal   _printint
	lw    $t0, 0($sp)	#POP
	addi  $sp, $sp, 4	
	
	li    $t0, 1
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	jal   _localtest
	lw    $v0, 0($sp)	#POP
	addi  $sp, $sp, 4
	lw    $t0, 0($sp)	#POP
	addi  $sp, $sp, 4
	move  $t0, $v0
	sw    $t0, 0($fp)
	la    $t0, .L6
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L6:	.asciiz "Result is "
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall
	lw    $t0, 0($fp)
	move  $a0, $t0
	li    $v0, 1		#WRITE
	syscall
	la    $t0, .L7
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L7:	.asciiz "\n"
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall
	la    $t0, .L8
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L8:	.asciiz "\nCalling localtest(3), result should be \n"
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall
	
	# calculate address for 3rd local in any function we call
    # that accepts one parameter and returns one value
    # $sp points just below where we will push param
    # $sp-8 is where return value slot will be
    # $sp-12 points at old $ra
    # $sp-16 points at old $fp
    # $sp-20 points at first local
    # $sp-24 points at second local
    addi  $t0, $sp, -28
    addi  $sp, $sp, -4  #PUSH
    sw    $t0, 0($sp)   
    jal   _printint
	lw    $t0, 0($sp)	#POP
	addi  $sp, $sp, 4	
	
	
	li    $t0, 3
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	jal   _localtest
	lw    $v0, 0($sp)	#POP
	addi  $sp, $sp, 4
	lw    $t0, 0($sp)	#POP
	addi  $sp, $sp, 4
	move  $t0, $v0
	sw    $t0, 0($fp)
	la    $t0, .L9
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L9:	.asciiz "Result is "
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall
	lw    $t0, 0($fp)
	move  $a0, $t0
	li    $v0, 1		#WRITE
	syscall
	la    $t0, .L10
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L10:	.asciiz "\n"
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall
	la    $t0, .L11
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L11:	.asciiz "\nCalling localtest(4), result should be \n"
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall
	
	# calculate address for 4th local in any function we call
    # that accepts one parameter and returns one value
    # $sp points just below where we will push param
    # $sp-8 is where return value slot will be
    # $sp-12 points at old $ra
    # $sp-16 points at old $fp
    # $sp-20 points at first local
    # $sp-24 points at second local
    addi  $t0, $sp, -32
    addi  $sp, $sp, -4  #PUSH
    sw    $t0, 0($sp)   
    jal   _printint
	lw    $t0, 0($sp)	#POP
	addi  $sp, $sp, 4		
	
	
	li    $t0, 4
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	jal   _localtest
	lw    $v0, 0($sp)	#POP
	addi  $sp, $sp, 4
	lw    $t0, 0($sp)	#POP
	addi  $sp, $sp, 4
	move  $t0, $v0
	sw    $t0, 0($fp)
	la    $t0, .L12
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L12:	.asciiz "Result is "
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall
	lw    $t0, 0($fp)
	move  $a0, $t0
	li    $v0, 1		#WRITE
	syscall
	la    $t0, .L13
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L13:	.asciiz "\n"
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall
	la    $t0, .L14
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L14:	.asciiz "\nUsing halloc to allocate space\n"
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall
	li    $t0, 324
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	jal   _halloc
	lw    $v0, 0($sp)	#POP
	addi  $sp, $sp, 4
	lw    $t0, 0($sp)	#POP
	addi  $sp, $sp, 4
	move  $t0, $v0
	sw    $t0, 0($fp)
	la    $t0, .L15
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L15:	.asciiz "Address to allocated memory is "
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall
	lw    $t0, 0($fp)
	move  $a0, $t0
	li    $v0, 1		#WRITE
	syscall
	la    $t0, .L16
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L16:	.asciiz "\n"
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall
	la    $t0, .L17
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L17:	.asciiz "\nNext allocation should be at "
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall
	lw    $t0, 0($fp)
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	li    $t0, 81
	lw    $t1, 0($sp)	#POP
	addi  $sp, $sp, 4
	sll   $t0, $t0, 2
	add   $t0, $t0, $t1
	move  $a0, $t0
	li    $v0, 1		#WRITE
	syscall
	la    $t0, .L18
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L18:	.asciiz "\nIt is at "
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall
	li    $t0, 4
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	jal   _halloc
	lw    $v0, 0($sp)	#POP
	addi  $sp, $sp, 4
	lw    $t0, 0($sp)	#POP
	addi  $sp, $sp, 4
	move  $t0, $v0
	sw    $t0, 0($fp)
	lw    $t0, 0($fp)
	move  $a0, $t0
	li    $v0, 1		#WRITE
	syscall
	la    $t0, .L19
	addi  $sp, $sp, -4		#PUSH
	sw    $t0, 0($sp)
	.data
.L19:	.asciiz "\n"
	.text
	lw    $a0, 0($sp)	#POP
	addi  $sp, $sp, 4
	li    $v0, 4		#STRING
	syscall
.L0:
	li    $v0, 10
	syscall		#exit

	.text
_sbrk:
	lw    $a0, 0($sp)
	li    $v0, 9
	syscall		#sbrk
	addi  $sp, $sp, -4		#PUSH
	sw    $v0, 0($sp)
	jr    $ra
