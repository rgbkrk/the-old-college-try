// MiniJava for cs453 Spring 2010
// By David Newman, Kyle Kelley, and Paul Gagliardi

class ObjectFun {
  public static void main(String[] args) {
    if((new RealDriver()).drive() < 0){}
    else{}
  }
}

class RealDriver {
  public int drive() {
    MyClass mc;
    MyClass mcf;
    int tempVal;
    mc = new MyClass();
    mcf = new MyClass();
    tempVal = mc.setI(42);
    tempVal = mc.setB(true);
    tempVal = mcf.setI(33);
    tempVal = mcf.setB(false);

    //Can't actually print a boolean value...
    if (mc.getB()) {
	    System.out.println(1);
    } else {
	    System.out.println(0);
    }
    System.out.println(mc.getI());

    if (mcf.getB()) {
	    System.out.println(1);
    } else {
	    System.out.println(0);
    }
    System.out.println(mcf.getI());

    return 31337;

  }
}

class MyClass {
  boolean b;
  int i;
  public int setI(int j)
  {
    i = j;
    return i;
  }
  public int setB(boolean bool)
  {
    b = bool;
    return 1;
  }

  public int getI()
  {
    return i;
  }
  public boolean getB()
  {
    return b;
  }
}

