#!/bin/csh

echo "========================================================="
echo "Regression testing MJ.jar --two-pass-interpret expr1.txt"
java -jar ../MJ.jar --two-pass-interpret expr1.txt > t
diff t expr1.txt.two-pass-interpret.out
echo "DONE with MJ.jar --two-pass-interpret expr1.txt"
echo "========================================================="

echo "========================================================="
echo "Regression testing MJ.jar --two-pass-mips expr1.txt"
java -jar ../MJ.jar --two-pass-mips expr1.txt > t
java -jar Mars.jar expr1.txt.s >> t
diff t expr1.txt.two-pass-mips.out
echo "DONE with MJ.jar --two-pass-mips expr1.txt"
echo "========================================================="

echo "========================================================="
echo "Regression testing MJ.jar --two-pass-interpret assign1.txt"
java -jar ../MJ.jar --two-pass-interpret assign1.txt > t
diff t assign1.txt.two-pass-interpret.out
echo "DONE with MJ.jar --two-pass-interpret assign1.txt"
echo "========================================================="

echo "========================================================="
echo "Regression testing MJ.jar --two-pass-mips assign1.txt"
java -jar ../MJ.jar --two-pass-mips assign1.txt > t
java -jar Mars.jar assign1.txt.s >> t
diff t assign1.txt.two-pass-mips.out
echo "DONE with MJ.jar --two-pass-mips assign1.txt"
echo "========================================================="

echo "========================================================="
echo "Regression testing MJ.jar --two-pass-interpret assign2.txt"
java -jar ../MJ.jar --two-pass-interpret assign2.txt > t
diff t assign2.txt.two-pass-interpret.out
echo "DONE with MJ.jar --two-pass-interpret assign2.txt"
echo "========================================================="

echo "========================================================="
echo "Regression testing MJ.jar --two-pass-mips assign2.txt"
java -jar ../MJ.jar --two-pass-mips assign2.txt > t
java -jar Mars.jar assign2.txt.s >> t
diff t assign2.txt.two-pass-mips.out
echo "DONE with MJ.jar --two-pass-mips assign2.txt"
echo "========================================================="


