	.text
main:

	addi 	$fp, $sp, 0	# init frame pointer
	# allocate space for local variables
	addi	$sp, $sp, -4
	# IntegerExp
	li $t2, 2
	# Making assignment to variable a
	sw $t2, 0($fp)

	# That's all folks!
	addi 	$sp, $fp, 0	# "clean up" the stack
	li $v0, 10
	syscall

