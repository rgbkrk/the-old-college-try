	.text
main:

	addi 	$fp, $sp, 0	# init frame pointer
	# allocate space for local variables
	addi	$sp, $sp, -24
	# IntegerExp
	li $t2, 42
	# Making assignment to variable a
	sw $t2, 0($fp)

	# Get the rvalue of variable a
	lw $t2, 0($fp)

	# IntegerExp
	li $t3, 2
	# MulExp
	mul $t2, $t2, $t3

	# IntegerExp
	li $t3, 12
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 12
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 12
	# MinusExp
	sub $t2, $t2, $t3

	# Making assignment to variable b
	sw $t2, 4($fp)

	# IntegerExp
	li $t2, 2
	# IntegerExp
	li $t3, 4
	# PlusExp
	add $t2, $t2, $t3

	# IntegerExp
	li $t3, 1
	# PlusExp
	add $t2, $t2, $t3

	# IntegerExp
	li $t3, 1
	# PlusExp
	add $t2, $t2, $t3

	# IntegerExp
	li $t3, 1
	# PlusExp
	add $t2, $t2, $t3

	# IntegerExp
	li $t3, 1
	# PlusExp
	add $t2, $t2, $t3

	# IntegerExp
	li $t3, 1
	# PlusExp
	add $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# PlusExp
	add $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# Get the rvalue of variable b
	lw $t2, 4($fp)

	# IntegerExp
	li $t3, 1
	# MinusExp
	sub $t2, $t2, $t3

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# Get the rvalue of variable b
	lw $t2, 4($fp)

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# Get the rvalue of variable b
	lw $t2, 4($fp)

	# IntegerExp
	li $t3, 3
	# MinusExp
	sub $t2, $t2, $t3

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# Get the rvalue of variable b
	lw $t2, 4($fp)

	# IntegerExp
	li $t3, 4
	# MinusExp
	sub $t2, $t2, $t3

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# IntegerExp
	li $t2, 2
	# IntegerExp
	li $t3, 4
	# PlusExp
	add $t2, $t2, $t3

	# IntegerExp
	li $t3, 12
	# PlusExp
	add $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# IntegerExp
	li $t2, 2
	# IntegerExp
	li $t3, 2
	# IntegerExp
	li $t4, 2
	# IntegerExp
	li $t5, 2
	# IntegerExp
	li $t6, 2
	# IntegerExp
	li $t7, 2
	# IntegerExp
	li $t8, 2
	# IntegerExp
	li $t9, 2
	# IntegerExp
	li $a0, 2
	# IntegerExp
	li $a1, 2
	# IntegerExp
	li $a2, 2
	# IntegerExp
	li $a3, 2
	# IntegerExp
	li $v0, 2
	# IntegerExp
	li $v1, 2
	# IntegerExp
	li $t0, 2
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	# IntegerExp
	li $t0, 2
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	# IntegerExp
	li $t0, 2
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	# IntegerExp
	li $t0, 2
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	# IntegerExp
	li $t0, 2
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	# IntegerExp
	li $t0, 2
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	# IntegerExp
	li $t0, 2
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	# MulExp
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	mul $t0, $t0, $t1
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# IntegerExp
	li $t0, 2
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	# PlusExp
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	add $t0, $t0, $t1
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# MulExp
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	mul $t0, $t0, $t1
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# IntegerExp
	li $t0, 2
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	# PlusExp
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	add $t0, $t0, $t1
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# MulExp
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	mul $t0, $t0, $t1
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# IntegerExp
	li $t0, 2
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	# PlusExp
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	add $t0, $t0, $t1
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# MulExp
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	mul $t0, $t0, $t1
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# IntegerExp
	li $t0, 2
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	# PlusExp
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	add $t0, $t0, $t1
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# MulExp
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	mul $t0, $t0, $t1
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# IntegerExp
	li $t0, 2
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	# PlusExp
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	add $t0, $t0, $t1
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# MulExp
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	mul $t0, $t0, $t1
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# IntegerExp
	li $t0, 2
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	# PlusExp
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	lw $t0, 0($sp)
	addi $sp, $sp, 4
	add $t0, $t0, $t1
	addi $sp, $sp, -4
	sw $t0, 0($sp)

	# MulExp
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	mul $v1, $v1, $t1

	# IntegerExp
	li $t0, 2
	addi $sp, $sp, -4
	sw $t0, 0($sp)
	# PlusExp
	lw $t1, 0($sp)
	addi $sp, $sp, 4
	add $v1, $v1, $t1

	# MulExp
	mul $v0, $v0, $v1

	# IntegerExp
	li $v1, 2
	# PlusExp
	add $v0, $v0, $v1

	# MulExp
	mul $a3, $a3, $v0

	# IntegerExp
	li $v0, 2
	# PlusExp
	add $a3, $a3, $v0

	# MulExp
	mul $a2, $a2, $a3

	# IntegerExp
	li $a3, 2
	# PlusExp
	add $a2, $a2, $a3

	# MulExp
	mul $a1, $a1, $a2

	# IntegerExp
	li $a2, 2
	# PlusExp
	add $a1, $a1, $a2

	# MulExp
	mul $a0, $a0, $a1

	# IntegerExp
	li $a1, 2
	# PlusExp
	add $a0, $a0, $a1

	# MulExp
	mul $t9, $t9, $a0

	# IntegerExp
	li $a0, 2
	# PlusExp
	add $t9, $t9, $a0

	# MulExp
	mul $t8, $t8, $t9

	# IntegerExp
	li $t9, 2
	# PlusExp
	add $t8, $t8, $t9

	# MulExp
	mul $t7, $t7, $t8

	# IntegerExp
	li $t8, 2
	# PlusExp
	add $t7, $t7, $t8

	# MulExp
	mul $t6, $t6, $t7

	# IntegerExp
	li $t7, 2
	# PlusExp
	add $t6, $t6, $t7

	# MulExp
	mul $t5, $t5, $t6

	# IntegerExp
	li $t6, 2
	# PlusExp
	add $t5, $t5, $t6

	# MulExp
	mul $t4, $t4, $t5

	# IntegerExp
	li $t5, 2
	# PlusExp
	add $t4, $t4, $t5

	# MulExp
	mul $t3, $t3, $t4

	# IntegerExp
	li $t4, 2
	# PlusExp
	add $t3, $t3, $t4

	# MulExp
	mul $t2, $t2, $t3

	# IntegerExp
	li $t3, 3
	# PlusExp
	add $t2, $t2, $t3

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# IntegerExp
	li $t2, 2
	# Making assignment to variable i
	sw $t2, 8($fp)

	# Get the rvalue of variable b
	lw $t2, 4($fp)

	# IntegerExp
	li $t3, 1
	# MinusExp
	sub $t2, $t2, $t3

	# IntegerExp
	li $t3, 2
	# IntegerExp
	li $t4, 2
	# MulExp
	mul $t3, $t3, $t4

	# IntegerExp
	li $t4, 2
	# MulExp
	mul $t3, $t3, $t4

	# IntegerExp
	li $t4, 2
	# MulExp
	mul $t3, $t3, $t4

	# IntegerExp
	li $t4, 2234
	# MulExp
	mul $t3, $t3, $t4

	# IntegerExp
	li $t4, 1
	# MulExp
	mul $t3, $t3, $t4

	# PlusExp
	add $t2, $t2, $t3

	# IntegerExp
	li $t3, 2132
	# MinusExp
	sub $t2, $t2, $t3

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# Get the rvalue of variable b
	lw $t2, 4($fp)

	# IntegerExp
	li $t3, 2
	# MinusExp
	sub $t2, $t2, $t3

	# Get the rvalue of variable i
	lw $t3, 8($fp)

	# PlusExp
	add $t2, $t2, $t3

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# Get the rvalue of variable b
	lw $t2, 4($fp)

	# IntegerExp
	li $t3, 3
	# MinusExp
	sub $t2, $t2, $t3

	# Get the rvalue of variable i
	lw $t3, 8($fp)

	# MinusExp
	sub $t2, $t2, $t3

	# Get the rvalue of variable i
	lw $t3, 8($fp)

	# Get the rvalue of variable i
	lw $t4, 8($fp)

	# MulExp
	mul $t3, $t3, $t4

	# MinusExp
	sub $t2, $t2, $t3

	# Get the rvalue of variable b
	lw $t3, 4($fp)

	# PlusExp
	add $t2, $t2, $t3

	# Get the rvalue of variable b
	lw $t3, 4($fp)

	# PlusExp
	add $t2, $t2, $t3

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# Get the rvalue of variable b
	lw $t2, 4($fp)

	# IntegerExp
	li $t3, 4
	# MinusExp
	sub $t2, $t2, $t3

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# IntegerExp
	li $t2, 1
	# Making assignment to variable bunnies
	sw $t2, 12($fp)

	# IntegerExp
	li $t2, 1
	# Making assignment to variable oldbunnies
	sw $t2, 16($fp)

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	# Making assignment to variable tempbunnies
	sw $t2, 20($fp)

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	# Get the rvalue of variable oldbunnies
	lw $t3, 16($fp)

	# PlusExp
	add $t2, $t2, $t3

	# Making assignment to variable bunnies
	sw $t2, 12($fp)

	# Get the rvalue of variable tempbunnies
	lw $t2, 20($fp)

	# Making assignment to variable oldbunnies
	sw $t2, 16($fp)

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	# Making assignment to variable tempbunnies
	sw $t2, 20($fp)

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	# Get the rvalue of variable oldbunnies
	lw $t3, 16($fp)

	# PlusExp
	add $t2, $t2, $t3

	# Making assignment to variable bunnies
	sw $t2, 12($fp)

	# Get the rvalue of variable tempbunnies
	lw $t2, 20($fp)

	# Making assignment to variable oldbunnies
	sw $t2, 16($fp)

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	# Making assignment to variable tempbunnies
	sw $t2, 20($fp)

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	# Get the rvalue of variable oldbunnies
	lw $t3, 16($fp)

	# PlusExp
	add $t2, $t2, $t3

	# Making assignment to variable bunnies
	sw $t2, 12($fp)

	# Get the rvalue of variable tempbunnies
	lw $t2, 20($fp)

	# Making assignment to variable oldbunnies
	sw $t2, 16($fp)

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	# Making assignment to variable tempbunnies
	sw $t2, 20($fp)

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	# Get the rvalue of variable oldbunnies
	lw $t3, 16($fp)

	# PlusExp
	add $t2, $t2, $t3

	# Making assignment to variable bunnies
	sw $t2, 12($fp)

	# Get the rvalue of variable tempbunnies
	lw $t2, 20($fp)

	# Making assignment to variable oldbunnies
	sw $t2, 16($fp)

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	# Making assignment to variable tempbunnies
	sw $t2, 20($fp)

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	# Get the rvalue of variable oldbunnies
	lw $t3, 16($fp)

	# PlusExp
	add $t2, $t2, $t3

	# Making assignment to variable bunnies
	sw $t2, 12($fp)

	# Get the rvalue of variable tempbunnies
	lw $t2, 20($fp)

	# Making assignment to variable oldbunnies
	sw $t2, 16($fp)

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	# Making assignment to variable tempbunnies
	sw $t2, 20($fp)

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	# Get the rvalue of variable oldbunnies
	lw $t3, 16($fp)

	# PlusExp
	add $t2, $t2, $t3

	# Making assignment to variable bunnies
	sw $t2, 12($fp)

	# Get the rvalue of variable tempbunnies
	lw $t2, 20($fp)

	# Making assignment to variable oldbunnies
	sw $t2, 16($fp)

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	# Making assignment to variable tempbunnies
	sw $t2, 20($fp)

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	# Get the rvalue of variable oldbunnies
	lw $t3, 16($fp)

	# PlusExp
	add $t2, $t2, $t3

	# Making assignment to variable bunnies
	sw $t2, 12($fp)

	# Get the rvalue of variable tempbunnies
	lw $t2, 20($fp)

	# Making assignment to variable oldbunnies
	sw $t2, 16($fp)

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	# Making assignment to variable tempbunnies
	sw $t2, 20($fp)

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	# Get the rvalue of variable oldbunnies
	lw $t3, 16($fp)

	# PlusExp
	add $t2, $t2, $t3

	# Making assignment to variable bunnies
	sw $t2, 12($fp)

	# Get the rvalue of variable tempbunnies
	lw $t2, 20($fp)

	# Making assignment to variable oldbunnies
	sw $t2, 16($fp)

	# Get the rvalue of variable bunnies
	lw $t2, 12($fp)

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# That's all folks!
	addi 	$sp, $fp, 0	# "clean up" the stack
	li $v0, 10
	syscall

	.text
_printint:
	addi $sp, $sp, -4	# push return addr
	sw   $ra, 0($sp)
	addi $sp, $sp, -4   # push frame pointer
	sw   $fp, 0($sp)
	addi $fp, $sp, -4	# set up frame pointer
	lw	$a0, 12($fp)	# load the int
		# will it work with just $sp?
	li	$v0, 1		# print the int
	syscall
	la $a0, lf
	li $v0, 4
	syscall
	lw  $fp, 0($sp) 	# pop frame pointer
	addi $sp, $sp, 4
	lw  $ra, 0($sp)	# pop return addr
	addi $sp, $sp, 4
	jr   $ra		# return to caller
.data:
	lf: .asciiz "\n"
