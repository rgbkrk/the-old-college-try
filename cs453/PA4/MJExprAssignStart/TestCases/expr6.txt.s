	.text
main:

	addi 	$fp, $sp, 0	# init frame pointer
	# IntegerExp
	li $t2, 1
	# IntegerExp
	li $t3, 1
	# IntegerExp
	li $t4, 1
	# IntegerExp
	li $t5, 1
	# IntegerExp
	li $t6, 1
	# IntegerExp
	li $t7, 1
	# IntegerExp
	li $t8, 1
	# IntegerExp
	li $t9, 1
	# IntegerExp
	li $a0, 1
	# IntegerExp
	li $a1, 1
	# IntegerExp
	li $a2, 1
	# PlusExp
	add $a1, $a1, $a2

	# PlusExp
	add $a0, $a0, $a1

	# PlusExp
	add $t9, $t9, $a0

	# PlusExp
	add $t8, $t8, $t9

	# PlusExp
	add $t7, $t7, $t8

	# PlusExp
	add $t6, $t6, $t7

	# PlusExp
	add $t5, $t5, $t6

	# PlusExp
	add $t4, $t4, $t5

	# PlusExp
	add $t3, $t3, $t4

	# PlusExp
	add $t2, $t2, $t3

	addi $sp, $sp, -4
	sw $t2, 0($sp)
	jal _printint #System.out.println from the top of the stack
	addi $sp, $sp, 4

	# That's all folks!
	addi 	$sp, $fp, 0	# "clean up" the stack
	li $v0, 10
	syscall

	.text
_printint:
	addi $sp, $sp, -4	# push return addr
	sw   $ra, 0($sp)
	addi $sp, $sp, -4   # push frame pointer
	sw   $fp, 0($sp)
	addi $fp, $sp, -4	# set up frame pointer
	lw	$a0, 12($fp)	# load the int
		# will it work with just $sp?
	li	$v0, 1		# print the int
	syscall
	la $a0, lf
	li $v0, 4
	syscall
	lw  $fp, 0($sp) 	# pop frame pointer
	addi $sp, $sp, 4
	lw  $ra, 0($sp)	# pop return addr
	addi $sp, $sp, 4
	jr   $ra		# return to caller
.data:
	lf: .asciiz "\n"
