/**
 * This interprets an ast containing simple miniJava statements. 
 *
 * 3/4/2010 - Modified from our earlier EvalVisitor.java
 * 2/23/2010 - Modified from Michelle Strout's DotVisitor.java
 * 			   Kyle Kelley, Dave Newman, Paul Gagliardi 
 * 6/06 - Modified from Brian Richard's ParserTest.Java.
 *        Michelle Strout
 */
package ast_visitors;

// These imports are all for SableCC-generated packages.
import java.io.PrintStream;
import java.util.Stack;
import java.util.HashMap;
import ast.analysis.DepthFirstAdapter;
import ast.node.*;

/**
 * We extend the DepthFirstAdapter.  
 * Visitors invoke a defaultCase method on each node they visit.  
 * We evaulate the tree and print result
 */
public class InterpretVisitor extends DepthFirstAdapter {
   private int nodeCount = 0;
   private PrintStream out;
   private Stack<Integer> evalStack;
   // We use strings as the key in the hashmap since Tokens don't work.
   private HashMap<String,Integer> IDValues;
   
   private Node stmt_root;

   public InterpretVisitor(Node stmt_root) {
      this.out = System.out;
      this.stmt_root = stmt_root;
      this.evalStack = new Stack<Integer>();
      // We use strings as the key in the hashmap since Tokens don't work.
      this.IDValues = new HashMap<String,Integer>();
   }

   /** Checks to see if node is root, if so, and stack is not 
    * empty, then print an error message.
    */
   public void defaultOut(Node node) {
      if(node.equals(stmt_root)) {
         if(!evalStack.isEmpty()) out.println("Stack not empty in InterpretVisitor! =(");
       }
      out.flush();
   }

   public void outPrintStatement(PrintStatement node){
      int temp = evalStack.pop();
      out.println(temp);
      defaultOut(node);
   }

   public void outAssignStatement(AssignStatement node){
      // We use strings as the key in the hashmap since Tokens don't work.
      this.IDValues.put(node.getId().getText(),evalStack.pop());
      defaultOut(node);
   }
   
   public void outPlusExp(PlusExp node){
      int temp1 = evalStack.pop();
      int temp2 = evalStack.pop();
      evalStack.push(temp1 + temp2);
      defaultOut(node);
   }
   
   public void outIntegerExp(IntegerExp node){
      evalStack.push(Integer.parseInt(node.getLiteral().getText()));
      defaultOut(node);
   }
   
   public void outMulExp(MulExp node){
      int temp1 = evalStack.pop();
      int temp2 = evalStack.pop();
      evalStack.push(temp1 * temp2);
      defaultOut(node);
   }
   
   public void outMinusExp(MinusExp node){
      int temp1 = evalStack.pop();
      int temp2 = evalStack.pop();
      evalStack.push(temp2 - temp1); //second pop minus first pop b/c subtraction is not communtative
      defaultOut(node);
   }

   public void outIdExp(IdExp node){
      // We use strings as keys in the hashmap since Tokens don't work.
      int temp = this.IDValues.get(node.getId().getText());
      evalStack.push(temp);
      defaultOut(node);
   }
   
}
