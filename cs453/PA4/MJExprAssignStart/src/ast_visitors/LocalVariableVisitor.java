/**
 * This interprets an ast containing simple miniJava statements. 
 *
 * 3/4/2010 - Modified from our earlier EvalVisitor.java
 * 2/23/2010 - Modified from Michelle Strout's DotVisitor.java
 * 			   Kyle Kelley, Dave Newman, Paul Gagliardi 
 * 6/06 - Modified from Brian Richard's ParserTest.Java.
 *        Michelle Strout
 */
package ast_visitors;

// These imports are all for SableCC-generated packages.
import java.io.PrintStream;
import java.util.Stack;
import java.util.HashMap;
import ast.analysis.DepthFirstAdapter;
import ast.node.*;

/**
 * We extend the DepthFirstAdapter.  
 * Visitors invoke a defaultCase method on each node they visit.  
 * We evaulate the tree and print result
 */
public class LocalVariableVisitor extends DepthFirstAdapter {
   private int nodeCount = 0;
   // We use strings as the key in the hashmap since Tokens don't work.
   private HashMap<String,Integer> IDValues;
   
   private int numVars;

   public LocalVariableVisitor() {
      // We use strings as the key in the hashmap since Tokens don't work.
      this.IDValues = new HashMap<String,Integer>();
   }

   public void outAssignStatement(AssignStatement node){
      if(!this.IDValues.containsKey(node.getId().getText())){
        numVars++;
        this.IDValues.put(node.getId().getText(),0);
        //System.out.println("Variable assigned " + node.getId().getText());
      }
      defaultOut(node);
   }

   public int getNumVars(){
     return numVars;
   }
   
   /*public void outIdExp(IdExp node){
     // Could let it create...
     //if(!this.IDValues.containsKey(node.getId().getText()){
     //   numVars++;
     //   this.IDValues.put(node.getId().getText(),0);
     // }
     //else{
     // //Probably want to throw an exception
     // }
     
      defaultOut(node);
   }*/
   
}
