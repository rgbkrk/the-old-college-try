/**
 * This code prints ASTs in a post-order traversal.
 *
 * 2/23/2010 - Modified from our own EvaVisitor.java
 * 2/23/2010 - Modified from Michelle Strout's DotVisitor.java
 * 			   Kyle Kelley, Dave Newman, Paul Gagliardi 
 * 6/06 - Modified from Brian Richard's ParserTest.Java.
 *        Michelle Strout
 */
package ast_visitors;

// These imports are all for SableCC-generated packages.
import java.io.PrintStream;
import java.util.Stack;

import ast.analysis.DepthFirstAdapter;
import ast.node.*;

/**
 * We extend the DepthFirstAdapter.  
 * Visitors invoke a defaultCase method on each node they visit.  
 * We evaulate the tree and print result
 */
public class PrintPostVisitor extends DepthFirstAdapter {
   private int nodeCount = 0;
   private PrintStream out;
   private Stack<Integer> evalStack;
   
   private Node ast_root;

   public PrintPostVisitor(Node ast_root) {
      this.out = System.out;
      this.ast_root = ast_root;
      this.evalStack = new Stack<Integer>();
   }

/*   public void defaultOut(Node node) {
       out.println(node.toString());
       out.flush();
   }
  */ 


   public void outPlusExp(PlusExp node){
	out.println("PlusExp");
   }
   
   public void outIntegerExp(IntegerExp node){
	out.println("IntegerExp");
	out.println("Token(" + node.getLiteral().getText() + ")");
   }
   
   public void outMulExp(MulExp node){
	out.println("MulExp");
   }
   
   public void outMinusExp(MinusExp node){
	out.println("MinusExp");
   }
}
