/**
 * This code prints ASTs in a pre-order traversal.
 *
 * 2/23/2010 - Modified from our own EvaVisitor.java
 * 2/23/2010 - Modified from Michelle Strout's DotVisitor.java
 * 			   Kyle Kelley, Dave Newman, Paul Gagliardi 
 * 6/06 - Modified from Brian Richard's ParserTest.Java.
 *        Michelle Strout
 */
package ast_visitors;

// These imports are all for SableCC-generated packages.
import java.io.PrintStream;
import java.util.Stack;

import ast.analysis.DepthFirstAdapter;
import ast.node.*;

/**
 * We extend the DepthFirstAdapter.  
 * Visitors invoke a defaultCase method on each node they visit.  
 */
public class PrintPrefVisitor extends DepthFirstAdapter {
   private int nodeCount = 0;
   private PrintStream out;
   private Stack<Integer> evalStack;
   
   private Node ast_root;

   public PrintPrefVisitor(Node ast_root) {
      this.out = System.out;
      this.ast_root = ast_root;
      this.evalStack = new Stack<Integer>();
   }

/*   public void defaultIn(Node node) {
       out.println(node.toString());
       out.flush();
   }
  */ 
   public void inPlusExp(PlusExp node){
	out.println("PlusExp");
   }
   
   public void inIntegerExp(IntegerExp node){
	out.println("IntegerExp");
	out.println("Token(" + node.getLiteral().getText() + ")");
   }
   
   public void inMulExp(MulExp node){
	out.println("MulExp");
   }
   
   public void inMinusExp(MinusExp node){
	out.println("MinusExp");
   }
}
