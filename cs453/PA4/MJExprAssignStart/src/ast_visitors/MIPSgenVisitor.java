/**
 * This code prints ASTs in a post-order traversal.
 *
 * 2/23/2010 - Modified from our own EvaVisitor.java
 * 2/23/2010 - Modified from Michelle Strout's DotVisitor.java
 * 			   Kyle Kelley, Dave Newman, Paul Gagliardi 
 * 6/06 - Modified from Brian Richard's ParserTest.Java.
 *        Michelle Strout
 */
package ast_visitors;

// These imports are all for SableCC-generated packages.
import java.io.PrintWriter;
import java.util.Stack;
import java.util.HashMap;

import ast.analysis.DepthFirstAdapter;
import ast.node.*;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;


/**
 * We extend the DepthFirstAdapter.  
 * Visitors invoke a defaultCase method on each node they visit.  
 * We evaulate the tree and print result
 */
public class MIPSgenVisitor extends DepthFirstAdapter {
   private HashMap<String,Integer> IDValues;
   private PrintWriter out;
   private Stack<Node> nodeStack;

   private int numVars;

   private static int SIZEOFINT = 4;

   private boolean willNeed_printint;

   private int currentFPOffset;

   public MIPSgenVisitor(PrintWriter out, int localVars) {
      this.willNeed_printint = false;
      this.currentFPOffset = 0;
      this.numVars = localVars;
      this.out = out;
      this.nodeStack = new Stack<Node>();
      this.IDValues = new HashMap<String,Integer>();
   }

   public void defaultIn(Node node) {
     if(nodeStack.empty()) {
       prologue();
     }
    this.nodeStack.push(node);
    out.flush();
   }

   public void defaultOut(Node node) {
       this.nodeStack.pop();
       if(nodeStack.empty())
       {
         epilogue();
       }
       out.flush();
   }

   public void outAssignStatement(AssignStatement node){
      int offset = 0;
      // Put the offset from frame pointer in our HashMap
      if(! this.IDValues.containsKey(node.getId().getText()))
      {
        offset = currentFPOffset;
        this.IDValues.put(node.getId().getText(),currentFPOffset);
        currentFPOffset += SIZEOFINT;
      }
      else
      {
        offset = this.IDValues.get(node.getId().getText());
      }
      out.println("\t# Making assignment to variable " + node.getId().getText());
      out.println("\t# from the stack.");
      mipsPop("$t0");
      out.println("\tsw $t0, " + offset + "($fp)");
      out.println();
      defaultOut(node);
   }

   public void outIdExp(IdExp node){
      //Pull the offset from the hashmap
      int offset = this.IDValues.get(node.getId().getText());
      //Put the rvalue into $t0
      out.println("\t# Get the rvalue of variable " + node.getId().getText());
      out.println("\tlw $t0, " + offset + "($fp)");
      //Push it
      mipsPush("$t0");
      out.println();
      defaultOut(node);
   }

   public void outPrintStatement(PrintStatement node){
     willNeed_printint = true;
     //Assumes that the last item on the stack is to be printed
     out.println("\tjal _printint #System.out.println from the top of the stack");
     mipsPop("$t0"); //Pop the argument that we "put on there"
     out.println();
     defaultOut(node);
   }
   
   public void outPlusExp(PlusExp node){
     out.println("\t# PlusExp");
     mipsPop("$t1");
     mipsPop("$t0");
     out.println("\tadd $t0, $t0, $t1");
     mipsPush("$t0");
     out.println();
     defaultOut(node);
   }

   public void outIntegerExp(IntegerExp node){
     out.println("\t# IntegerExp");
     out.println("\tli $t0, " + node.getLiteral().getText());
     mipsPush("$t0");
     out.println();
     defaultOut(node);
   }
   
   public void outMulExp(MulExp node){
	   out.println("\t# MulExp");
     mipsPop("$t1");
     mipsPop("$t0");
     out.println("\tmul $t0, $t0, $t1");
     mipsPush("$t0");
     out.println();
     defaultOut(node);

   }
   
   public void outMinusExp(MinusExp node){
     out.println("\t# MinusExp");
     mipsPop("$t1");
     mipsPop("$t0");
     out.println("\tsub $t0, $t0, $t1");
     mipsPush("$t0");
     out.println();
     defaultOut(node);
   }


   /** Helper routines to push a value onto the stack
    * and pop it off */
   private void mipsPush(String register)
   {
     out.println("\taddi $sp, $sp, -4");
     out.println("\tsw " + register + ", 0($sp)");
   }

   private void mipsPop(String register)
   {
     out.println("\tlw " + register + ", 0($sp)");
     out.println("\taddi $sp, $sp, 4");
   }

   private void prologue()
   {
     out.println("\t.text");
     out.println("main:");
     out.println();
     // Put frame pointer at stack pointer
     out.println("\taddi 	$fp, $sp, 0	# init frame pointer");

     if(this.numVars > 0){
       // TODO Worry about how big the immediate number will be
       int allocation = this.numVars * SIZEOFINT;
       out.println("\t# allocate space for local variables");
  	   out.println("\taddi	$sp, $sp, -"+ allocation);
     }
   }

   private void epilogue()
   {
         //We're all done
         out.println("\t# That's all folks!");
         // Put stack pointer at frame pointer
         out.println("\taddi 	$sp, $fp, 0	# \"clean up\" the stack");
         out.println("\tli $v0, 10");
         out.println("\tsyscall");
         out.println();

         //Put in code for necessary utility functions
         //if needed
         if(this.willNeed_printint)
           _printint();
   }

   private void _printint()
   {
     //try{
     //  String source = suckSource("src/MIPSutils/printint.s");
     //  out.println(source);
     //}
     //catch(IOException ioe)
     //{
       // Originally this was a backup plan
       // in case of the inability to find the file
       // this turned out well for us, as the jar doesn't look internally

       out.println("\t.text");
       out.println("_printint:");

       out.println("\taddi $sp, $sp, -4	# push return addr");
       out.println("\tsw   $ra, 0($sp)");

       out.println("\taddi $sp, $sp, -4   # push frame pointer");
       out.println("\tsw   $fp, 0($sp)");

       out.println("\taddi $fp, $sp, -4	# set up frame pointer");

       out.println("\tlw	$a0, 12($fp)	# load the int");
		   out.println("\t	# will it work with just $sp?");

       out.println("\tli	$v0, 1		# print the int");
       out.println("\tsyscall");

       out.println("\tla $a0, lf");
       out.println("\tli $v0, 4");
       out.println("\tsyscall");

       out.println("\tlw  $fp, 0($sp) 	# pop frame pointer");
       out.println("\taddi $sp, $sp, 4");

       out.println("\tlw  $ra, 0($sp)	# pop return addr");
       out.println("\taddi $sp, $sp, 4");

       out.println("\tjr   $ra		# return to caller");
       out.println(".data:");
       out.println("\tlf: .asciiz \"\\n\"");
     //}
   }

   /*private String suckSource(String filename) throws IOException
   {
     BufferedReader input =  new BufferedReader(new FileReader(filename));
     String line;
     String source = "";
     String nl = System.getProperty("line.separator");
     while((line = input.readLine())!= null)
     {
       source = source + line + nl;
     }
     input.close();

     return source;

   }*/
   
}
