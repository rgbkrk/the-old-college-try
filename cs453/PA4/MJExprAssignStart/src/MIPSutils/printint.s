## __printint.s
    .text

_printint:

    addi $sp, $sp, -4	# push return addr
    sw   $ra, 0($sp)

    addi $sp, $sp, -4   # push frame pointer
    sw   $fp, 0($sp)

    addi $fp, $sp, -4	# set up frame pointer

    lw	$a0, 12($fp)	# load the int
			# will it work with just $sp?

    li	$v0, 1		# print the int
    syscall

    la $a0, lf
    li $v0, 4
    syscall

    lw  $fp, 0($sp) 	# pop frame pointer
    addi $sp, $sp, 4

    lw  $ra, 0($sp)	# pop return addr
    addi $sp, $sp, 4

    jr   $ra		# return to caller


.data:
  lf: .asciiz "\n"
