/**
 * CS 453h
 * Kyle Kelley, Dave Newman, Paul Gagliardi
 */
package mjparser;

import java_cup.runtime.Symbol;
%%
%cup
%char
%line
%public
%eofval{
 return new Symbol(sym.EOF, new TokenValue("EOF", yyline, yychar));
%eofval}
%%
"System.out.println" { return new Symbol(sym.SYSO); }
"=" { return new Symbol(sym.EQUALS); }
";" { return new Symbol(sym.SEMICOLON); }
"+" { return new Symbol(sym.PLUS); }
"-" { return new Symbol(sym.MINUS); }
"*" { return new Symbol(sym.TIMES); }
"{" { return new Symbol(sym.LBRACE); }
"}" { return new Symbol(sym.RBRACE); }
"(" { return new Symbol(sym.LPAREN); }
")" { return new Symbol(sym.RPAREN); }
[0-9]+ {return new Symbol(sym.NUMBER, new TokenValue(yytext(), yyline, yychar));}
[ \t\r\n\f] { /* ignore white space. */ }
. { System.err.println("Illegal character: "+yytext()); }
