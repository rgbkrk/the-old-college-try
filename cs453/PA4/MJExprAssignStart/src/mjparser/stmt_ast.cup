/**
 * CS 453h
 * Kyle Kelley, Dave Newman, Paul Gagliardi
 */
package mjparser;

import java_cup.runtime.*;
import java.util.LinkedList;
import ast.node.*;

parser code {:
    public void unrecovered_syntax_error(Symbol cur_token) {
        report_fatal_error("Fatal syntax error", cur_token);
    }

    public void report_fatal_error(String message, Object info) {
        report_error(message, info);
        done_parsing();
        Symbol token = (Symbol)info;
        mjparser.TokenValue tok = (mjparser.TokenValue)token.value;
        throw new mjparser.ParseException("Fatal parsing error",
                                          tok.line, tok.pos);
    }

    public void report_error(String message, Object info) {
        Symbol token = (Symbol)info;
        mjparser.TokenValue tok = (mjparser.TokenValue)token.value;
        System.err.println("[" + tok.line + "," + tok.pos + "] "
                               + message + " at " + tok.toString() );
    }

    public static void main(String args[]) throws Exception {
        new stmt_ast(new Yylex(System.in)).parse();
    }
:}

terminal PLUS, MINUS, TIMES, LPAREN, RPAREN, LBRACE, RBRACE, EQUALS, SEMICOLON;
terminal SYSO;
terminal Token ID;
terminal TokenValue NUMBER;

non terminal IExp expr;
non terminal IStatement stmt;
non terminal IStatement block_stmt;
non terminal LinkedList<IStatement> stmt_list;
non terminal Token target;

precedence left PLUS, MINUS;
precedence left TIMES;

/* 
 * Remember that the production for the root
 * node must be first in the cup file!
 */
stmt      ::=  SYSO LPAREN expr:e RPAREN SEMICOLON 
               {: RESULT = new PrintStatement(e); :}

            |  target:t EQUALS expr:e SEMICOLON
               {: RESULT = new AssignStatement(t, e); :}

            | block_stmt:b
               {: RESULT = b; :};

block_stmt ::= LBRACE stmt_list:sl RBRACE 
               {: RESULT = new BlockStatement(sl); :};

stmt_list  ::=  /* epsilon */
               {: RESULT = new LinkedList<IStatement>(); :}

            |  stmt:s stmt_list:sl 
               {: sl.add(0,s); RESULT = sl; :};

target    ::=   ID:i
                {: RESULT = i; :};


expr      ::= 
                NUMBER:n
                {: RESULT = new IntegerExp(new Token(n.text,n.line,n.pos)); :}

            |   ID:i 
                {: RESULT = new IdExp(i); :}
        
            |   expr:l PLUS expr:r
                {: RESULT = new PlusExp(l,r); :}
        
            |   expr:l MINUS expr:r
                {: RESULT = new MinusExp(l,r); :}

            |   expr:l TIMES expr:r
                {: RESULT = new MulExp(l,r); :}

            |   LPAREN expr:e RPAREN
                {: RESULT=e; :}
            
            ;

