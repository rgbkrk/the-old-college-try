/**
 * CS 453h Spring 2010
 * Kyle Kelley, Dave Newman, Paul Gagliardi
 * for PA4
 */
package mjparser;
import ast.node.Token;
import java_cup.runtime.Symbol;
%%
%cup
%char
%line
%public
%eofval{
 return new Symbol(sym.EOF, new TokenValue("EOF", yyline, yychar));
%eofval}
%%
"System.out.println" { return new Symbol(sym.SYSO); }
"=" { return new Symbol(sym.EQUALS); }
";" { return new Symbol(sym.SEMICOLON); }
"+" { return new Symbol(sym.PLUS); }
"-" { return new Symbol(sym.MINUS); }
"*" { return new Symbol(sym.TIMES); }
"{" { return new Symbol(sym.LBRACE); }
"}" { return new Symbol(sym.RBRACE); }
"(" { return new Symbol(sym.LPAREN); }
")" { return new Symbol(sym.RPAREN); }
[0-9]+ { return new Symbol(sym.NUMBER, new TokenValue(yytext(), yyline, yychar));}
[a-zA-Z][a-zA-Z0-9_]* { return new Symbol(sym.ID, new Token(yytext(), yyline, yychar)); }
[ \t\r\n\f] { /* ignore white space. */ }
. { System.err.println("Illegal character: "+yytext()); }
