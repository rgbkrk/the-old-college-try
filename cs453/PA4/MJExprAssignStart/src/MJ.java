/*
 * MJ.java
 * cs 453 Spring 2010 group h
 * David Newman, Kyle Kelley, Paul Gagliardi
 *
 * usage: 
 *   java MJ [--two-pass-interpret | --two-pass-mips ] infile
 *
 * This driver calls one of two possible expression evaluation parsers.
 *
 */
import java.io.FileReader;
import java.io.PrintWriter;

import mjparser.*;
import ast_visitors.*;

public class MJ {

	  private static void usage() {
	      System.err.println(
	        "MJ: Specify input file in program arguments\n" +
	        "java MJ [ --two-pass-interpret | --two-pass-mips ] infile");
	  }
	 
	  public static void main(String args[]) 
	  {
	    
	    if(args.length < 1)
	    {         
	        usage();
	        System.exit(1);
	    }

	    // filename should be the last command line option
	    String filename = args[args.length-1];
	    
	    try {

	      // construct the lexer, 
	      // the lexer will be the same for all of the parsers
	      Yylex lexer = new Yylex(new FileReader(filename));
	      
	      // determine which parser to execute
	        
	      if ( args[0].equals("--two-pass-interpret") ) {

	        // create the AST
	        stmt_ast parser = new stmt_ast(lexer);
	        ast.node.Node ast_root = (ast.node.Node)parser.parse().value;
	            
	        // print ast to file
	        java.io.PrintStream astout =
	          new java.io.PrintStream(
	            new java.io.FileOutputStream(filename + ".ast.dot"));
	        ast_root.apply(new DotVisitor(new PrintWriter(astout)));
	        System.out.println("Printing dot file to " + filename + ".ast.dot");
	      
	        // evaluate the value of expr, also prints value to stdout
	        ast_root.apply(new InterpretVisitor(ast_root));
	      
	      } else if ( args[0].equals("--two-pass-mips") ) {

	        // create the AST
	        stmt_ast parser = new stmt_ast(lexer);
	        ast.node.Node ast_root =         
	            (ast.node.Node)parser.parse().value;
	            
	        // print ast to file
	        java.io.PrintStream astout =
	          new java.io.PrintStream(
	            new java.io.FileOutputStream(filename + ".ast.dot"));
	        ast_root.apply(new DotVisitor(new PrintWriter(astout)));
	        System.out.println("Printing dot file to " + filename + ".ast.dot");

          LocalVariableVisitor localVarVisitor = new LocalVariableVisitor();
          ast_root.apply(localVarVisitor);
	      
	        // generate MIPS code that evaluates the expression
	        java.io.PrintStream mipsout =
	          new java.io.PrintStream(
	            new java.io.FileOutputStream(filename + ".s"));
	        System.out.println("Printing MARS MIPS file to " + filename + ".s");
	        ast_root.apply(new MIPSgenVisitor(new PrintWriter(mipsout),localVarVisitor.getNumVars()));

	      

        } else if (args[0].equals("--two-pass-mips-O") ) {
          System.out.println("Using optimized extra credit compiler");
          System.out.println("Thrusters engaged");
	        stmt_ast parser = new stmt_ast(lexer);
	        ast.node.Node ast_root =         
	            (ast.node.Node)parser.parse().value;
	            
	        // print ast to file
	        java.io.PrintStream astout =
	          new java.io.PrintStream(
	            new java.io.FileOutputStream(filename + ".ast.dot"));
	        ast_root.apply(new DotVisitor(new PrintWriter(astout)));
	        System.out.println("Printing dot file to " + filename + ".ast.dot");

          LocalVariableVisitor localVarVisitor = new LocalVariableVisitor();
          ast_root.apply(localVarVisitor);
	      
	        // generate MIPS code that evaluates the expression
	        java.io.PrintStream mipsout =
	          new java.io.PrintStream(
	            new java.io.FileOutputStream(filename + ".s"));
	        System.out.println("Printing MARS MIPS file to " + filename + ".s");
	        ast_root.apply(new MIPSgenECVisitor(new PrintWriter(mipsout),localVarVisitor.getNumVars()));
          } else {
            System.out.print("Unknown command-line option ");
            System.out.println(args[0]);
          }

	    } catch (Exception e) {
	        e.printStackTrace();
	        System.exit(1);
	    }  
	  }

}
