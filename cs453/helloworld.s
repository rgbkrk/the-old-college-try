## helloworld.s
        .data
hello_world:    .asciiz "Hello World!\n"
    .text
    .globl main
main:
    # There is no need to store $ra and $fp
    # in main for MARS because execution always
    # starts at the top of the input .s file
    # and $ra and $fp always have initial 
    # values of 0.
            
            
    # print Hello World!

    # The number put into $v0 selects MARS syscall.
    # A parameter to a system call should be passed in $a0.
    li      $v0, 4
    la      $a0, hello_world
    syscall



exit_main:
    # HALT the MARS simulation.
    li      $v0, 10
    syscall

