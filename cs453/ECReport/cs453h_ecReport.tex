\documentclass{article}
\usepackage{fullpage}
\usepackage{listings}
\usepackage{color}

\definecolor{kaiBlue}{rgb}{0.1,0,0.55} 
\definecolor{kaiGrey}{rgb}{.99,.99,.99} 
\definecolor{kaiGreen}{rgb}{0.15,0.55,0.1}

\lstset{language=Java, numbers=left, frame=none,
	keywordstyle=\color{kaiBlue},
	commentstyle=\color{red},
   	stringstyle=\color{kaiGreen},
   	numbers=left,
   	numberstyle=\tiny{\color{yellow}},
   	stepnumber=1,
   	numbersep=10pt,
   	backgroundcolor=\color{kaiGrey},
   	tabsize=4,
   	showspaces=false,
   	showstringspaces=false
}

\title{Register Allocation Optimization in MiniJava Compiler\\
CS 453h Extra Credit Report}
\author{P. Gagliardi \and K. Kelley \and D. Newman}
\pdfpagewidth 8.5in
\pdfpageheight 11in

\begin{document}
\maketitle

\section{Abstract}
For Extra Credit in CS453, Introduction to Compilers, we implemented a register allocation
scheme as part of our miniJava compiler. The algorithm, optimization, and
performance will be discussed in the following sections.

\section{Design}

There are three principal methods, \texttt{getSaveLocation},
\texttt{getLocation}, and \texttt{store}.  The \texttt{getSaveLocation} method
is called before generation of MIPS operation code, and \texttt{store} is
called after generation of MIPS operation code. They work together to
generate MIPS stack code and manage registers when necessary. The method
\texttt{getLocation} is also called before generation of MIPS operation code to
manage registers for operands used in MIPS operations.

The \texttt{getSaveLocation} method gets a register location for temporary
storage of an intermediate result. If there is a free register, it is marked as
in use, placed in an internal stack of memory locations and returned. If there
is no free register, a special token, called \texttt{ON\_STACK}, indicating that the
intermediate result is to be stored on the MIPS stack, is placed on the
internal stack of memory locations. In this case, the method
\texttt{getSaveLocation} returns \texttt{\$t0} because the calling method will
store the intermediate result in \texttt{\$t0} and then push that value on the
MIPS stack in order to leave \texttt{\$t0} free for further computation. If
\texttt{getSaveLocation} has indicated that the result of the current MIPS
operation should be placed on the MIPS stack, then the \texttt{store} method
generates MIPS code to do that.

The \texttt{getLocation} method gets a register location from the internal
stack. If the \texttt{ON\_STACK} token was at the top of the stack, then
\texttt{getLocation} generates code to pop the MIPS stack into \texttt{\$t0} or
\texttt{\$t1} and returns the name of the register used. Otherwise,
\texttt{getLocation} returns the name of the register that was at the top of
the internal stack.

Difficulties in the design were faced in \textit{newArrayExp}, where, in extreme situations,
no registers are available for the intilization of the array and heap allocation. A specific test case
does exist to crash our current optimized implementation. To avoid syncronization issues, all registers in 
use are saved before a method call in \textit{caseCallExp} and then restored post method call. 


%See Listing \ref{meatRegAlloc} on page \pageref{meatRegAlloc} for the major meat of our code!

%
%
% We may want to include a specific write-ups on ANDexp and caseCall
%
%

\section{Performance}
The performance gains from the optimized register allocation scheme were quite significant. In Table
\ref{tab:mips} (pg. \pageref{tab:mips}), total executed MIPS instructions on a variety of test cases are compared, and the optimized
code would indicate a speed and performance increase. Specifically in \textit{MM.java}, a matrix
multiplication test case, the total executed instructions are cut nearly by a factor of 4. Not only are the number
of instructions decreased, but the number of \textit{expensive} memory related MIPS instructions are decreased, 
seen in Table \ref{tab:memory} (pg. \pageref{tab:memory}). Usually decreased by a factor of 2 to 5, eliminating unnecessary memory reads is a
very significant performance increase. Not surprisingly, due to the elimination of most stack manipulation arithmetic,
ALU-related MIPS instructions decreased as well displayed in Table \ref{tab:alu} (pg. \pageref{tab:alu}). Jump and Branch related instructions 
were always exactly the same in compared test cases.

The test cases used for comparision consisted of \textit{LotsOfRegisters.java}, a series of 45 nested additions 
of constants and method calls. \textit{MM.java} is a very large matrix manipulation example with an entire
matrix multiplication framework complete with a pseudo-random number generator. \textit{Nesting.java} and
\textit{PaulPaul.java} are rather basic class and method test cases, while \textit{Recursion.java} demonstrates
10 levels of recursive calls.

\begin{table}[t]
\begin{center}
  \begin{tabular} {|c|c|c|}
    \hline
     \texttt{Test Case} & \texttt{Unoptimized} & \texttt{Optimized} \\
    \hline
    LotsOfRegisters.java & 512 & 259 \\
    MM.java & 1,368,177 & 348,618 \\
    Nesting.java & 556 & 451 \\
    PaulPaul.java & 385 & 218 \\
    Recursion.java & 1040 & 633\\
    \hline
    \end{tabular}
    \caption{Comparison of executed MIPS instructions} 
    \label{tab:mips}
\end{center}
\end{table}

\begin{table}[t]
\begin{center}
  \begin{tabular} {|c|c|c|}
    \hline
     \texttt{Test Case} & \texttt{Unoptimized} & \texttt{Optimized} \\
    \hline
    LotsOfRegisters.java & 203 & 76 \\
    MM.java & 655,120 & 145,202 \\
    Nesting.java & 235 & 177 \\
    PaulPaul.java & 165 & 80 \\
    Recursion.java & 440 & 231\\
    \hline
    \end{tabular}
    \caption{Comparison of executed Memory-related MIPS instructions} 
    \label{tab:memory}
\end{center}
\end{table}

\begin{table}[t]
\begin{center}
  \begin{tabular} {|c|c|c|}
    \hline
     \texttt{Test Case} & \texttt{Unoptimized} & \texttt{Optimized} \\
    \hline
    LotsOfRegisters.java & 298 & 172 \\
    MM.java & 633,357 & 123,510 \\
    Nesting.java & 258 & 210 \\
    PaulPaul.java & 185 & 103 \\
    Recursion.java & 511 & 313\\
    \hline
    \end{tabular}
    \caption{Comparison of executed ALU-related MIPS instructions} 
    \label{tab:alu}
\end{center}
\end{table}

The results were achieved using 14 registers; if more registers were
available, optimization would be expected to increase. Under some
circumstances, these optimizations can potentially eliminate up to 6
instructions moving data between memory and registers.  If all 14 registers are
in use within a single expression, some of the code generated is the same as
the unoptimized version, but as the evaluation proceeds optimization will
occur.

\section{Conclusions}

Using the register allocation scheme, significant performance increases were observed. 
Eliminating unnecessary stack manipulation decreased memory reads and arithmetic, while 
decreasing the actual size of generated code. The challenges faced in this extra credit project 
were not trivial and lent insight into the complexity of compiler optimization.
\end{document}
