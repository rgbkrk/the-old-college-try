===== Terminals =====
[0]EOF [1]error [2]CARET [3]A [4]AT 


===== Non terminals =====
[0]S 

===== Productions =====
[0] S ::= S S CARET 
[1] $START ::= S EOF 
[2] S ::= S S AT 
[3] S ::= A 

===== Viable Prefix Recognizer =====
START lalr_state [0]: {
  [S ::= (*) S S AT , {EOF A }]
  [$START ::= (*) S EOF , {EOF }]
  [S ::= (*) A , {EOF A }]
  [S ::= (*) S S CARET , {EOF A }]
}
transition on S to state [2]
transition on A to state [1]

-------------------
lalr_state [1]: {
  [S ::= A (*) , {EOF CARET A AT }]
}

-------------------
lalr_state [2]: {
  [S ::= S (*) S AT , {EOF A }]
  [S ::= (*) S S AT , {CARET A AT }]
  [$START ::= S (*) EOF , {EOF }]
  [S ::= (*) A , {CARET A AT }]
  [S ::= S (*) S CARET , {EOF A }]
  [S ::= (*) S S CARET , {CARET A AT }]
}
transition on EOF to state [4]
transition on S to state [3]
transition on A to state [1]

-------------------
lalr_state [3]: {
  [S ::= S S (*) AT , {EOF CARET A AT }]
  [S ::= S (*) S AT , {CARET A AT }]
  [S ::= (*) S S AT , {CARET A AT }]
  [S ::= S S (*) CARET , {EOF CARET A AT }]
  [S ::= (*) A , {CARET A AT }]
  [S ::= S (*) S CARET , {CARET A AT }]
  [S ::= (*) S S CARET , {CARET A AT }]
}
transition on CARET to state [6]
transition on S to state [3]
transition on AT to state [5]
transition on A to state [1]

-------------------
lalr_state [4]: {
  [$START ::= S EOF (*) , {EOF }]
}

-------------------
lalr_state [5]: {
  [S ::= S S AT (*) , {EOF CARET A AT }]
}

-------------------
lalr_state [6]: {
  [S ::= S S CARET (*) , {EOF CARET A AT }]
}

-------------------
-------- ACTION_TABLE --------
From state #0
 [term 3:SHIFT(to state 1)]
From state #1
 [term 0:REDUCE(with prod 3)] [term 2:REDUCE(with prod 3)]
 [term 3:REDUCE(with prod 3)] [term 4:REDUCE(with prod 3)]
From state #2
 [term 0:SHIFT(to state 4)] [term 3:SHIFT(to state 1)]
From state #3
 [term 2:SHIFT(to state 6)] [term 3:SHIFT(to state 1)]
 [term 4:SHIFT(to state 5)]
From state #4
 [term 0:REDUCE(with prod 1)]
From state #5
 [term 0:REDUCE(with prod 2)] [term 2:REDUCE(with prod 2)]
 [term 3:REDUCE(with prod 2)] [term 4:REDUCE(with prod 2)]
From state #6
 [term 0:REDUCE(with prod 0)] [term 2:REDUCE(with prod 0)]
 [term 3:REDUCE(with prod 0)] [term 4:REDUCE(with prod 0)]
------------------------------
-------- REDUCE_TABLE --------
From state #0
 [non term 0->state 2]
From state #1
From state #2
 [non term 0->state 3]
From state #3
 [non term 0->state 3]
From state #4
From state #5
From state #6
-----------------------------
------- CUP v0.11a beta 20060608 Parser Generation Summary -------
  0 errors and 0 warnings
  5 terminals, 1 non-terminal, and 4 productions declared, 
  producing 7 unique parse states.
  0 terminals declared but not used.
  0 non-terminals declared but not used.
  0 productions never reduced.
  0 conflicts detected (0 expected).
  Code written to "parser.java", and "sym.java".
---------------------------------------------------- (v0.11a beta 20060608)
