# Factorial.s
# Recursive version of factorial program for cs453 lecture
# Chris Wilcox
# Colorado State University
# 1/28/2010

#########    main program

    .text
main:

    # CALL FACTORIAL
    li  $t0, 7          # load constant
    addi $sp, $sp, -4   # push parameter
    sw   $t0, 0($sp)    # store parameter
    jal  factorial      # call factorial
    lw   $t1, 0($sp)    # load return value
    addi $sp, $sp, 4    # pop return value
    addi $sp, $sp, 4    # pop parameter
    
    # DISPLAY RESULT
    la   $a0, output    # output label
    li   $v0, 4         # syscall: print string
    syscall             #
    move $a0, $t1       # output value
    li   $v0, 1         # syscall: print integer
    syscall             #
    la   $a0, lf        # line feed
    li   $v0, 4         # syscall: print string
    syscall             #

    # EXIT SIMULATOR
    li   $v0, 10        # syscall: exit
    syscall             #
        
#########    factorial function

# int factorial (int number) 
# {
#     int value=1;
#     if (number > 1)
#         value = number * factorial(number-1);
#    return value;
# }

factorial:

    # PROLOGUE
    addi $sp, $sp, -4   # make space for return value
    addi $sp, $sp, -4   # push return address
    sw   $ra, 0($sp)    # store return address
    addi $sp, $sp, -4   # push frame pointer
    sw   $fp, 0($sp)    # store frame pointer
    addi $fp, $sp, -4   # set up new frame pointer

    # FUNCTION
    li     $t0, 1       # $t0 = value = 1
    lw    $t1, 16($fp)  # $t1 = number
    blez $t1, exit      # if number <= 0 exit

recursive:
    sub  $t2, $t1, $t0      # number -= 1
    addi $sp, $sp, -4       # push parameter
    sw   $t2, 0($sp)        # store parameter
    jal  factorial          # call factorial
    lw   $t0, 0($sp)        # load return value
    addi $sp, $sp, 4        # pop return value
    addi $sp, $sp, 4        # pop parameter
    lw    $t1, 16($fp)      # $t1 = number
    mul  $t0, $t1, $t0      # value = number * factorial(number-1)

exit:

    # EPILOGUE
    sw   $t0, 12($fp)   # return value
    lw   $fp, 0($sp)    # restore frame pointer
    addi $sp, $sp, 4    # pop frame pointer
    lw   $ra, 0($sp)    # restore return address
    addi $sp, $sp, 4    # pop return address

    jr   $ra            # function return
        
#########    global data

    .data
    .align     0             
output:     .asciiz    "Factorial value: "
lf:         .asciiz    "\n"

