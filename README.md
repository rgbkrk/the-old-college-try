The Old College Try
--------------------

In order to appease my packrat sensibilities, I'm uploading some projects from my time at Colorado State University.

My plan is to only release code for classes where the current projects are quite different.

CS453: Compilers
==================

Originally, this project came from the [Introduction to Compilers](http://www.cs.colostate.edu/~cs453/) course at Colorado State University. They now appear to be using some neat little devices called the [meggy-jr](http://www.evilmadscientist.com/2008/meggy-jr-rgb/). I'm jealous.


